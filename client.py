"""
.. Copyright (c) 2014 Institute of Psychology of the Polish Academy of Sciences.
   All rights reserved. For full license text (in Polish) please see LICENSE.polish
  
   Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)

   This file is a part of the nminds-x4 program, used to conduct psychological
   experiments on group decision-making. For more information please see README file.

This module contains the client program, represented by an instance of `Client`,
which connects to the server and awaits commands - such as launching an experiment.
Single call can start multiple client processes with different names, sharing
machine resources.

USAGE: client.py [-h] [-a ADDR] [-p PORT] [-i] [-c CONFIG_FILE] [-l LOG_LEVEL]
                 [--hold]
                 names [names ...]

positional arguments:
  names           Names of the clients.

optional arguments:
  -h, --help      show this help message and exit
  -a ADDR         Server address.
  -p PORT         Server port.
  -i              Monitoring client.
  -c CONFIG_FILE  Config file to use.
  -l LOG_LEVEL    Log level of console output.
  --hold          Keeps the console open after termination.

"""

import argparse
import sys
import logging
from time import sleep
from exprunner import common
from exprunner.client import Client
from exprunner.msgsocket import addr_to_readable_str
from logging.config import dictConfig
from multiprocessing import Process, Pipe

#: Logger for use by Client instances (and related objects).
log = logging.getLogger('client')

#: Key used to request a connection attempt in case of a failed first attempt.
_KEY_CONNECT = 'c'

#: Key used to quit the program in case the connection fails.
_KEY_QUIT = 'q'

#: Delays (in seconds) between consecutive automatic connection attempts.
_CONNECT_DELAYS = [1.0] * 2


def run_connection_sequence(client):
    """
    Attempts to connect the client autamatically a few times (specified by _CONNECT_DELAYS),
    before switching to manual confiramtion mode, in which the user has to either order
    a connection attempt or quit the procedure.

    :param client: the client to connect

    """
    addr = client.config.get('connection', 'server_ip')
    port = client.config.getint('connection', 'server_port')
    reuse_addr = client.config.get('connection', 'reuse_addr')
    dictConfig(common.get_default_logging(client.config.get('paths', 'log'),
                                          file_level='DEBUG',
                                          console_level=client.config.get('misc', 'log_level')))
    for delay in _CONNECT_DELAYS:
        if client.try_connect(addr, port, reuse_addr):
            print 'Client %s connected to %s:%d' % (client.name, addr, port)
            break
        sleep(delay)
    while not client.is_connected():
        print '%s: Unable to connect to %s:%d, type \'%s\' to try again or \'%s\' to quit.' % (client.name, addr, port, _KEY_CONNECT, _KEY_QUIT)
        s = sys.stdin.readline()
        if len(s) == 0 or s[0] == _KEY_QUIT:
            break
        if s[0] == _KEY_CONNECT:
            if client.try_connect(addr, port, reuse_addr):
                break
    if client.is_connected():
        print client.name + ': Connection established with ' + addr_to_readable_str(client.get_remote_addr()) + '.'


def create_client(name, args, pipes):
    """
    Creates a new Client instance.

    :param name: Name of the new client.
    :param args: Dictionary containing relevant createion arguments.
    :param pipes: Pipes connecting multiple instances on same machine.\
    Tuple containning input and outut pipes for the client.
    :returns: The created client object.

    """
    config = common.Config({'name': name})
    config_file = args.config_file if args.config_file is not None else common.CONFIG_FILE
    loaded_configs = config.read(config_file)
    if config_file not in loaded_configs:
        raise Exception('Unable to load client config file: ' + config_file)
    for lc in loaded_configs:
        log.info('Config loaded: %s', lc)
    if args.addr is not None:
        config.set('connection', 'server_ip', args.addr)
    if args.port is not None:
        config.set('connection', 'server_port', args.port)
    if args.log_level is not None:
        config.set('misc', 'log_level', args.log_level)
    client = Client(name, args.interactive, config, pipes)
    return client


def run_client(name, args, pipes):
    """
    Creates, connects and runs a Client.

    :param args: Dictionary containing relevant createion arguments.
    :param pipes: Tuple containning input and outut pipes for the client.

    """
    client = create_client(name, args, pipes)
    run_connection_sequence(client)
    if client.is_connected():
        client.loop()
        print 'Client {} disconnected.'.format(name)
    else:
        print 'Unable to connect ' + name


def main():
    """
    Runnable client script.

    """
    parser = argparse.ArgumentParser(description='exprunner client.')
    parser.add_argument('names', help='Names of the clients.', type=common.valid_name, nargs='+')
    parser.add_argument('-a', dest='addr', type=common.valid_addr, help='Server address.')
    parser.add_argument('-p', dest='port', type=common.valid_port, help='Server port.')
    parser.add_argument('-i', dest='interactive', action='store_true', help='Monitoring client.')
    parser.add_argument('-c', dest='config_file', help='Config file to use.')
    parser.add_argument('-l', dest='log_level', type=common.valid_log_level, help='Log level of console output.')
    parser.add_argument('--hold', action='store_true', help='Keeps the console open after termination.')
    args = parser.parse_args()
    hold = args.hold
    to_spawn = [(pn, Pipe(duplex=False)) for pn in args.names[1:]]
    processes = []
    for name, pipe in to_spawn:
        pipe_in, pipe_out = pipe
        p = Process(target=run_client, args=(name, args, ([pipe_in], [])))
        processes.append((p, pipe_out))
        p.start()
    run_client(args.names[0], args, ([], [pipe_out for p, pipe_out in processes]))
    for p, pipe_out in processes:
        p.join()
        pipe_out.close()
    if hold:
        print 'Client has exited. Press ENTER to continue.'
        raw_input()
    else:
        print 'Client has exited.'

if __name__ == '__main__':
    main()
