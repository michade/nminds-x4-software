"""
.. Copyright (c) 2014 Institute of Psychology of the Polish Academy of Sciences.
   All rights reserved. For full license text (in Polish) please see LICENSE.polish

   Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)

   This file is a part of the nminds-x4 program, used to conduct psychological
   experiments on group decision-making. For more information please see README file.

The interacting minds experiment. Up to 4 participants can be connected.
See supplementary documents for experiment details.

The experiment takes no command line arguments.

Additional required files:
stimuli.xml - Contains the text of varius messages displayed.
config.txt - The experiment's configuration.
config_test.txt - Test configuration (overrides config in test mode)

"""

import math
import os
import random
from traceback import format_exc
import pygame
import pyglet
import pyglet.image
import pyglet.image.atlas
import pyglet.sprite
import numpy as np
import exprunner.exputil as exputil
import exprunner.experiment as experiment
from exprunner.trials import Trials


def make_gabor_patch(size, sd, contrast, freq, ori, phase):
    """
    Create a Gabor patch image in the form of a numpy array.

    :param size: Width and height of the patch in pixels.
    :param sd: Standard deviation of the gaussian envelope (in pixels).
    :param contrast: Contrast multiplier (0.0-1.0).
    :param freq: Spatial frequency of the sine wave (in cycles per pixel).
    :param ori: Orientation of the wave, in radians (0.0 means vertical).
    :param phase: Pahse of the sine wave (in radians).

    """
    X = (np.linspace(0, size, size) / size) - .5
    Xm, Ym = np.meshgrid(X, X)
    Xw = Xm * np.cos(ori)
    Yw = Ym * np.sin(ori)
    grating = np.sin(((Xw + Yw) * freq * 2 * np.pi) + phase) * contrast
    gauss = np.exp(-((Xm ** 2) + (Ym ** 2)) / (2 * sd ** 2))
    gabor = grating * gauss
    gabor = ((gabor + 1.0) / 2.0) * 255.0
    gabor[gabor < 0] = 0.0
    gabor[gabor > 255] = 255.0
    return gabor


def make_gabor_surface(size, resolution, sd, contrast, freq, ori, phase):
    """
    Create a pyglet surface containing a Gabor patch, with parameters
    specified in arbitrary units, tied to pixels py the resolution parameter.

    :param size: Width and height of the patch in arbitrary units.
    :param resolution: Size of the texture in pixels.
    :param sd: Standard deviation of the gaussian envelope (in arbitrary units).
    :param contrast: Contrast multiplier (0.0-1.0).
    :param freq: Spatial frequency of the sine wave (in cycles per unit).
    :param ori: Orientation of the wave, in radians (0.0 means vertical).
    :param phase: Pahse of the sine wave (in radians).

    """
    px_per_unit = float(resolution) / float(size)
    gabor = make_gabor_patch(size, sd * px_per_unit, contrast, freq / px_per_unit, math.radians(ori), math.radians(phase)).astype(int)
    gabor = gabor.astype('uint8')
    return pyglet.image.ImageData(size, size, 'L', gabor.data.__str__())


class Rect(object):
    """
    A 2D, colored rectangle.

    """

    def __init__(self, x0, y0, x1, y1, color):
        """
        Initializes the object.

        :param x0: x coordinate of the lower left corner.
        :param y0: y coordinate of the lower left corner.
        :param x1: x coordinate of the upper right corner.
        :param y1: y coordinate of the upper right corner.

        """
        self._verts = pyglet.graphics.vertex_list(4, ('v2f', (x0, y0, x1, y0, x1, y1, x0, y1)))
        self._color = color if len(color) > 3 else color + (255,)

    def draw(self):
        """
        Draw the rectangle.

        """
        col = [c / 255.0 for c in self._color]
        pyglet.gl.glColor4f(*col)
        self._verts.draw(pyglet.gl.GL_QUADS)


class GaborCircle(object):
    """
    A set of Gabor patches, arranged circulary around the center of thescreen.
    All patches are identical, possibly with the exception of one, `target` patch.

    """

    def __init__(self, gabors, nPatches, origin, radius, phase, target_idx=None, target_pos=None, fixation=None):
        """
        Initializes the object.

        :param gabors: A list of gabor patch objects. The first is always the non-target patch object.
        :param nPatches: Number of patches in  the circle.
        :param radius: Radius of the circle (distance from the center of the screen to the
                 center of each patch) in visual degrees.
        :param phase: Clockwise shift in radians of the first patch.
        :param target_idx: The target patch's Index in the `gabors` list. Should be > 0.
                     None indicates no target patch in the set.
        :param target_pos: The position of the target patch in the set. 0 is topmost, follows clockwise.
        :param fixation: Additional stimuli (fixation cross) to be presented with the gabor patches.

        """
        self.gabors = gabors
        self.fixation = fixation
        self.nPathes = nPatches
        self.radius = radius
        self.phase = phase
        self.target_idx = target_idx
        self.target_pos = target_pos
        self.origin = origin

    def draw(self):
        """
        Sends the stimulus to the renderer to be drawn. It will appear in the next frame.

        """
        if self.fixation is not None:
            self.fixation.draw()
        x0, y0 = self.origin
        for i in range(self.nPathes):
            g = self.gabors[self.target_idx] if i == self.target_pos else self.gabors[0]
            alpha = self.phase + (2.0 * math.pi * i) / self.nPathes
            g.set_position(math.cos(alpha) * self.radius + x0 - g.width / 2, math.sin(alpha) * self.radius + y0 - g.height / 2)
            g.draw()


class CameraClap(object):
    """
    A `camera clap`, allowing for synchronization between the experiment and a recording camera.
    Shows a countdown with numbers and a `pie timer`. Plays a short note as each second passes
    and longer note after the countdown has finished.

    """

    def __init__(self, window, nsecs, make_text, radius=0.4, note='Dsh'):
        """
        Initializes the object.

        :param window: The psychopy window providing the rendering context.
        :param nsec: Number of seconds to count down from.
        :param make_text: A function(text, position, color, font_size) creating a graphical text object.
        :param radius: The radius of the `pie timer`.
        :param note: A musical note to be played as each second passes.

        """
        self._circle_radius = radius * window.height
        self._clock = None
        self._window = window
        self._nsecs = nsecs
        self._sound_sec = exputil.Note(note, 0.25)
        self._sound_end = exputil.Note(note, 0.5)
        self._sound_sec.load()
        self._sound_end.load()
        x0 = window.width / 2
        y0 = window.height / 2
        self._numbers = [make_text(str(i), pos=(x0, y0), color=(0, 0, 0, 255), text_size=15.0)
                         for i in range(nsecs, 0, -1)]
        self._n_edges = 100
        phase = math.pi * 0.5
        cv = [(x0 + self._circle_radius * math.cos(a), y0 + self._circle_radius * math.sin(a))
              for a in [phase + 2.0 * math.pi * x / self._n_edges
                        for x in range(self._n_edges)]]
        cv = [(x0, y0)] + cv + [cv[0]]
        self._circle_vertices = []
        for x, y in cv:
            self._circle_vertices.append(x)
            self._circle_vertices.append(y)
        self._prev = -1

    def draw(self):
        """
        Draws the stimulus to the back buffer, and plays sounds at appropriate moments.

        """
        if self._clock is None:
            self._clock = exputil.Clock()
        t = self._clock.getTime()
        sec = int(math.floor(t))
        if sec < self._nsecs:
            if sec > self._prev:
                self._prev = sec
                self._sound_sec.play()
            wedge_prop = min(1.0, max(t - sec, 0.0))
            wedge_n = int(round((self._n_edges + 1) * wedge_prop))
            pyglet.gl.glColor3f(0.5, 0.5, 0.5)
            pyglet.graphics.draw_indexed(self._n_edges + 2, pyglet.gl.GL_TRIANGLE_FAN,
                                         range(0, self._n_edges + 2),
                                         ('v2f', self._circle_vertices[:(2 * (self._n_edges + 2))]))
            pyglet.gl.glColor3f(1.0, 1.0, 1.0)
            pyglet.graphics.draw_indexed(wedge_n + 1, pyglet.gl.GL_TRIANGLE_FAN,
                                         range(0, wedge_n + 1),
                                         ('v2f', self._circle_vertices[:(2 * (wedge_n + 1))]))
            self._numbers[sec].draw()
        elif sec > self._prev:
            self._prev = sec
            self._sound_end.play()


def _all_same(lst):
    """
    Are all elements of the list equal ('==') to ech other?.

    :param lst: the list to check

    """
    if len(lst) == 0:
        return True
    for i in range(1, len(lst)):
        if lst[i] != lst[0]:
            return False
    return True


def read_color(s):
    """
    Parse a color description.
    In the input string, color has three components - red, green and blue, each an integer in range from 0 to 255.

    :param s: The string to parse

    :returns: A color tuple (r, g, b)

    """
    c = s.split()
    r = int(c[0].strip())
    g = int(c[1].strip())
    b = int(c[2].strip())
    return (r, g, b)


def read_contrast(s):
    """
    Parse contract values from string.
    Currently implemented as floats.

    """
    return float(s.strip())


def _rotated(lst, times=1):
    """
    Returns the list `lst` rotated `times` times right.

    e.g.:
    >>> _rotated([1, 2, 3, 4, 5], times=2)
    [4, 5, 1, 2, 3]

    """
    times = times % len(lst)
    tmp = lst[-times:]
    tmp.extend(lst[:-times])
    return tmp


def visdegrees_to_px(x, monitor):
    """
    Converts visual degrees to pixel size on a given monitor.

    """
    return x * ((0.5 * monitor.resolution[1]) / math.degrees(math.atan2(0.5 * monitor.height, monitor.distance)))


def calc_dpi(window, monitor):
    """
    Calculates a dpi (dots per inch) parameter of a monitor

    """
    return window.height / (monitor.height * 0.3937)  # 0.3937 cm per inch


def visdegrees_to_pt(x, monitor, dpi):
    """
    Conerts visual degrees to font points on a given monitor.

    """
    return 72.0 * (visdegrees_to_px(x, monitor) / dpi)  # about 72 point


#: The name for the experiment config file.
CONFIG_FILE = 'config.txt'

#: The name of the file containing text stimuli.
STIMULI_FILE = 'stimuli.xml'


class Main(experiment.Experiment):
    """
    The interacting minds experiment class.

    """

    def __init__(self, client, info, rpc, config, pipes, logger=None):
        """
        Initializes the Experiment object.
        Warning: this does NOT call Experiment.prepare.

        :param clien: Name of the client running this instance.
        :param info: ExperimentInfo object, containing additional experiment info.
        :param rpc: Interface for rpc calls to server.
        :param config: Local configuration information.
        :param pipes: Pipes used for ipc with other instances on same physical machine.

        """
        super(Main, self).__init__(client, info, rpc, config, pipes, logger)

    def prepare(self):
        """
        Preapre the experiment. Creates graphical resources and media.

        :param client: Name of the client running this instance.
        :param info: ExperimentInfo object, containing additional experiment info.
        :param rpc: Interface for rpc calls to server.
        :param config: Local configuration information.
        :param pipes: Pipes used for ipc with other instances on same physical machine.

        """
        self._master_clock = exputil.Clock()
        self._load_config()
        self._contrasts = [read_contrast(self.config.get('stimulus', 'base_contrast'))] + [read_contrast(s) for s in self.config.get('stimulus', 'target_contrasts').split(',')]
        self._color_codes = [read_color(s) for s in self.config.get('setting', 'color_codes').split(',')]
        self._results_dir = exputil.ResultsDirectory(self.info.name, self.config.get('paths', 'data'), create=True)
        self.log.info('Results directory: %s', self._results_dir)
        self._shuffle_feedback_texts = self.config.getboolean('stimulus', 'shuffle_feedback_texts')
        exp_dir = os.path.join(self.config.get('paths', 'experiments'), self.info.name)
        if self.pipes is not None:
            self._evhub = exputil.EventHub(len(self.pipes[0]) == 0, self.pipes[0], self.pipes[1])
        else:
            self._evhub = None
        stimuli_file = os.path.join(exp_dir, STIMULI_FILE)
        self._stimuli = exputil.DomSerializer(stimuli_file)
        self.log.info('Stimuli data loaded: %s', stimuli_file)
        if self.client != 'server' and self.client != 'master':
            self._init_monitor()

    def _load_config(self):
        """
        Loads configuration files - experiment-specific, then local.

        """
        exp_conf_dir = os.path.join(self.config.get('paths', 'experiment_configs'), self.info.name)
        exp_conf_file = os.path.join(exp_conf_dir, CONFIG_FILE)
        local_conf_file = os.path.join(self.config.get('paths', 'local_config'))
        loaded_configs = self.config.read([exp_conf_file, local_conf_file])
        if exp_conf_file not in loaded_configs:
            raise Exception('Unable to load experiment config file: ' + exp_conf_file)
        if local_conf_file not in loaded_configs:
            raise Exception('Unable to load local config file: ' + local_conf_file)
        for lc in loaded_configs:
            self.log.info('Config loaded: %s', lc)

    def _init_monitor(self):
        """
        Inits the pyglet monitor.

        """
        self._monitor = exputil.Monitor(self.config.getfloat('monitor', 'width'),
                                        self.config.getfloat('monitor', 'height'),
                                        self.config.getfloat('monitor', 'distance'),
                                        (self.config.getint('window', 'resolution_x'), self.config.getint('window', 'resolution_y')),
                                        self.config.getfloat('monitor', 'frame_length'))

    def _init_window(self):
        """
        Creates the psychopy window.

        """
        fullscr = self.config.getboolean('window', 'fullscreen')
        display = pyglet.window.get_platform().get_default_display()
        screen_no = self.config.getint('window', 'screen')
        screens = display.get_screens()
        if screen_no < len(screens):
            screen = screens[screen_no]
        else:
            self.log.warning('Requested screen no {} but only {} screens are found. Using screen 0 instead'.format(screen_no, len(screens)))
            screen = screens[0]
        template = pyglet.gl.Config(double_buffer=True)
        gl_config = screen.get_best_config(template)
        self._window = pyglet.window.Window(vsync=True,
                                            caption='Interacting Minds Experiment',
                                            width=self.config.getint('window', 'resolution_x') if not fullscr else None,
                                            height=self.config.getint('window', 'resolution_y') if not fullscr else None,
                                            fullscreen=fullscr,
                                            config=gl_config,
                                            screen=screen)
        if fullscr:
            self._window.set_exclusive_mouse(True)
        self.evhub.after_window_created(self._window)
        self._set_background_color((0.5, 0.5, 0.5))  # floats here, because of glClearColor.
        self.log.debug('Pyglet window created.')

    def _set_background_color(self, color):
        """
        Sets the background color for the experiment.

        """
        if len(color) < 4:
            color = color + (0.0,)
        pyglet.gl.glClearColor(*color)

    def _init_stimuli(self, stim):
        """
        Creates the stimuli.

        :param stim: Stimuli text, loaded from `stimuli.xml`.

        """
        conf = self.config
        w = self._window.width
        h = self._window.height
        self._text_size = conf.getfloat('stimulus', 'text_size')
        self._text_font = conf.get('stimulus', 'text_font')
        px_per_vdeg = visdegrees_to_px(1.0, self._monitor)
        texres = conf.getint('stimulus', 'texture_resolution')  # actually this is texture size in px
        texres = int(visdegrees_to_px(4.0, self._monitor))
        # conf.getfloat('stimulus', 'gabor_radius') - co z tym?
        gsurfs = [make_gabor_surface(texres,
                                     px_per_vdeg,
                                     conf.getfloat('stimulus', 'gabor_sd'),
                                     contrast,
                                     conf.getfloat('stimulus', 'gabor_sf'),
                                     conf.getfloat('stimulus', 'gabor_orientation'),
                                     conf.getfloat('stimulus', 'gabor_phase'))
                  for contrast in self._contrasts]
        atlas = pyglet.image.atlas.TextureAtlas(width=len(gsurfs) * texres, height=texres)
        imgs = [atlas.add(g) for g in gsurfs]
        gabors = [pyglet.sprite.Sprite(imgs[i], blend_src=pyglet.gl.GL_SRC_COLOR, blend_dest=pyglet.gl.GL_DST_COLOR) for i in range(0, len(imgs))]
        circle_radius = visdegrees_to_px(conf.getfloat('stimulus', 'circle_radius'), self._monitor)
        circle_phase = conf.getfloat('stimulus', 'circle_phase')
        n_patches = conf.getint('stimulus', 'patches_in_circle')
        fixation = self._make_text_stim(stim.fixation, text_size=1.5, pos=(w / 2, h / 2), color=(0, 0, 0, 255))
        origin = (self._window.width * 0.5, self._window.height * 0.5)
        self._gabor_circles = [GaborCircle(gabors, n_patches, origin, circle_radius, circle_phase, fixation=fixation),
                               GaborCircle(gabors, n_patches, origin, circle_radius, circle_phase, fixation=fixation)]
        self._keys = {self._get_key_code('key_first'): 0, self._get_key_code('key_second'): 1}
        allow_abort = self.config.getboolean('misc', 'allow_user_abort')
        self._abort_key = self._get_key_code('key_abort') if allow_abort else None
        self._decision_texts = [(self._make_text_stim(stim.decision % 1, col),
                                 self._make_text_stim(stim.decision % 2, col))
                                for col in self._color_codes]
        self._feedback_texts = [(self._make_text_stim(stim.correct_feedback, col),
                                 self._make_text_stim(stim.incorrect_feedback, col))
                                for col in self._color_codes]
        blank = exputil.BlankScreen()
        self._block_wait_blank = self._make_screen(blank, meeting='block_wait')
        self._subblock_wait_blank = self._make_screen(blank, meeting='subblock_wait')
        self._trial_wait_blank = self._make_screen(blank, meeting='trial_wait')
        self._indiv_dec_input = exputil.InputChecker(self._keys.keys(), self.evhub)
        self._indiv_dec_prompt = self._make_screen(self._make_text_stim(stim.indiv_dec_prompt), name='indiv_dec_prompt', other=[self._indiv_dec_input])
        self._indiv_dec_meet = exputil.MeetingChecker(self.control, self.client, 'indiv_dec_wait', auto_reset=False)
        self._indiv_dec_wait_1 = self._make_screen(blank, duration=0.25, other=[self._indiv_dec_meet])
        self._indiv_dec_wait_2 = self._make_screen(self._make_text_stim(stim.indiv_dec_wait), other=[self._indiv_dec_meet])
        self._group_dec_input = exputil.InputChecker(self._keys.keys(), self.evhub)
        group_dec_beep = exputil.Note(self.config.get('stimulus', 'beep_note'), self.config.getfloat('stimulus', 'beep_duration'))
        group_dec_beep.load()
        self._group_dec_beep_blank = self._make_screen(blank, duration=0.3, on_show_first=lambda: group_dec_beep.play())
        self._group_dec_prompt = self._make_screen(self._make_text_stim(stim.group_dec_prompt), name='group_dec_prompt', other=[self._group_dec_input])
        self._group_dec_wait = self._make_screen(self._make_text_stim(stim.group_dec_wait), meeting='group_dec_wait')
        #self._continue_keys = [self._get_key_code('key_continue')] #not used in this version
        self._continue_keys = [self._get_key_code('key_first')]
        self._instructions = self._make_screen(exputil.StimGroup(Rect(0, 0.8 * h, w, h,
                                                                      self._color_codes[self._clients.index(self.client) + 1]),
                                                                 self._make_text_stim(stim.instructions)),
                                               keys=self._continue_keys)
        self._before_prac = self._make_screen(self._make_text_stim(stim.before_prac), keys=self._continue_keys)
        self._before_exp = self._make_screen(self._make_text_stim(stim.before_exp), keys=self._continue_keys)
        self._end_screen = self._make_screen(self._make_text_stim(stim.end_screen), keys=[self._get_key_code('key_finish')])
        self._switch_places_text = self._make_screen(self._make_text_stim(stim.switch_places_text), keys=self._continue_keys)
        self._switch_places_wait = self._make_screen(self._make_text_stim(stim.switch_places_wait), meeting='switch_places_wait')
        self._camera_clap_wait = self._make_screen(blank, meeting='camera_clap_wait')
        self._camera_clap = self._make_screen(CameraClap(self._window, 3, self._make_text_stim), duration=4.0, is_static=False)
        self._presenter = exputil.Presenter(self._window, self._master_clock)
        stim_length = conf.getfloat('stimulus', 'duration')
        self._trial_proc = [self._make_screen(blank, name='trial_wait', meeting='stim_wait'),
                            self._make_screen(fixation, name='fixation', duration=conf.getfloat('stimulus', 'fixation_duration')),
                            self._make_screen(self._gabor_circles[0], name='stim1', duration=stim_length),
                            self._make_screen(blank, name='blank_between', duration=conf.getfloat('stimulus', 'blank_interval_duration')),
                            self._make_screen(self._gabor_circles[1], name='stim2', duration=stim_length),
                            self._indiv_dec_prompt]
        self.log.debug('Stimuli created')

    def _get_key_code(self, name):
        """
        Gets a specified key code from the config file.

        :param name: - Key name.

        """
        s = self.config.get('input', name).strip()
        if exputil.is_joystick_key_code(s):
            return exputil.add_joystick_id(s, int(self.config.get('input', 'joystick_id').strip()))
        else:
            return s

    def run(self):
        """
        Executes the experiment.

        """
        self.log.info('Experiment %s started', __name__)
        trials = None
        data_file = None
        self._clients = sorted([c[0] for c in self.info.clients if c[1] == experiment.ClientType.NORMAL])
        participants = range(len(self._clients))
        try:
            if self.client == 'server':
                self._init_meetings()
                session_no = self._results_dir.get_new_session_id('exp')
                self.log.info('Session no.: %d', session_no)
                self.control.put('session_no', session_no)
                data_file = self._results_dir.make_new_file('exp', session_no)
                n_practice_blocks = 1
                n_experimental_blocks = self.config.getint('procedure', 'n_blocks')
                if n_experimental_blocks != len(participants) and self.config.getboolean('procedure', 'switch_places'):
                    self.log.warning('"Swicth places" is on, but #blocks=%d and #participants=%d', n_experimental_blocks, len(participants))
                trials = self._make_trials(n_practice_blocks, n_experimental_blocks, participants)
                self._run_exp_server(trials)
            else:
                pygame.init()
                self._init_window()
                self._init_stimuli(self._stimuli)
                session_no = self.control.get('session_no')
                self.log.info('Session no.: %d', session_no)
                self._run_exp()
        finally:
            if self.client == 'server':
                if data_file is not None and trials is not None:
                    self._save_data(data_file, trials)
            else:
                try:
                    if self._window is not None:
                        self._window.close()
                        self.log.debug('Window closed.')
                except:
                    self.log.error(format_exc())
                pygame.quit()
            self.log.info('Experiment %s finished.', __name__)

    def _save_data(self, data_file, trials):
        """
        Write data obtained from the experiment to a file.

        :param data_file: The path to the outout file. It will be truncated.
        :param trials: The a list of lists of TrialHandlers, containing the data, from each block and subblock respectively.

        """
        try:
            with open(data_file, 'w') as f:
                f.write(trials.format(header=True, sep='\t', rowsep='\n'))
            self.log.info('Saved %d trials', trials.nrow)
        except:
            self.log.error(format_exc())

    def _run_exp(self):
        """
        Run the client part of the experimental procedure.

        """
        switch_places = self.config.getboolean('procedure', 'switch_places')
        self._presenter.show(self._instructions,
                             self._camera_clap_wait)
        if self.config.getboolean('procedure', 'camera_clap'):
            self._presenter.show(self._camera_clap)
        self._master_clock.reset()
        self._presenter.show(self._before_prac)
        prev_practice = True
        while True:
            self._presenter.show(self._block_wait_blank)
            block = self.control.get('block')
            if block is None:
                break
            if not block['practice'] and prev_practice:
                self._presenter.show(self._before_exp)
                prev_practice = False
            self.log.info('Entering %s block #%d', block['block_type'], block['block_idx'])
            self._run_block(block)
            if switch_places and not block['practice'] and block['block_idx'] < block['no_blocks'] - 1:
                self._presenter.show(self._switch_places_text,
                                     self._switch_places_wait)
            self.log.info('Finished %s block #%d', block['block_type'], block['block_idx'])
        self._presenter.show(self._end_screen)

    def _run_exp_server(self, trials):
        """
        Run the server part of the experimental procedure.

        :param trials: A Trials object contatining trial data.

        """
        self.control.meet('camera_clap_wait', self.client)
        self._master_clock.reset()
        practice = trials.child('practice')
        for block in practice.children():
            self.control.put('block', block.data())
            self.control.meet('block_wait', self.client)
            self.log.info('Entering practice block #%d', block.index)
            self._run_block_server(block)
            self.log.info('Finished practice block #%d', block.index)
        switch_places = self.config.getboolean('procedure', 'switch_places')
        experimental = trials.child('experimental')
        for block in experimental.children():
            self.control.put('block', block.data())
            self.control.meet('block_wait', self.client)
            self.log.info('Entering experimental block #%d', block.index)
            self._run_block_server(block)
            if switch_places and block.index < len(experimental) - 1:
                self.control.meet('switch_places_wait', self.client)
            self.log.info('Finished experimental block #%d', block.index)
        self.control.put('block', None)
        self.control.meet('block_wait', self.client)

    def _run_block(self, block):
        """
        Run an experimental block - client part.

        :param block: The block to run (a Trials object).

        """
        while True:
            self._presenter.show(self._subblock_wait_blank)
            subblock = self.control.get('subblock')
            if subblock is None:
                break
            self.log.info('Entering subblock #%d', subblock['subblock_idx'])
            if subblock['no_subblocks'] > 1:
                progress = self._make_screen(self._make_text_stim(self._stimuli.progress_info % (
                    subblock['block_idx'] * subblock['no_blocks'] + subblock['subblock_idx'] + 1, subblock['no_subblocks'] * subblock['no_blocks'])), duration=1.0)
                self._presenter.show(progress)
            self._run_subblock(subblock)
            self.log.info('Finished subblock #%d', subblock['subblock_idx'])

    def _run_block_server(self, block):
        """
        Run an experimental block - server part.

        :param block: The block to run (a Trials object).

        """
        self._prev_gr_dec_maker = block['participants'][0]
        for subblock in block.children():
            self.control.put('subblock', subblock.data())
            self.control.meet('subblock_wait', self.client)
            self.log.info('Entering subblock #%d', subblock.index)
            self._run_subblock_server(subblock)
            self.log.info('Finished subblock #%d', subblock.index)
        self.control.put('subblock', None)
        self.control.meet('subblock_wait', self.client)

    def _run_subblock(self, subblock):
        """
        Run an experimental subblock - client part.

        :param subblock: The subblock to run (A Trials object).

        """
        client_idx = self._clients.index(self.client)
        participants = subblock['participants']
        my_participant = participants[client_idx]
        while True:
            self._presenter.show(self._trial_wait_blank)
            trial = self.control.get('trial')
            if trial is None:
                break
            self._presenter.reset_data()
            self.log.debug('Starting trial #%02d', trial['trial_idx'])
            i_tgt = trial['target_screen_nr']
            i_non = (trial['target_screen_nr'] + 1) % 2
            self._gabor_circles[i_tgt].target_pos = trial['target_pos']
            self._gabor_circles[i_tgt].target_idx = trial['contrast_idx']
            self._gabor_circles[i_non].target_pos = None
            self._gabor_circles[i_non].target_idx = None
            self._presenter.show(*self._trial_proc)
            indiv_dec_key, indiv_dec_rt = self._indiv_dec_input.pressed[0]
            indiv_dec = self._keys[indiv_dec_key]
            indiv_dec_time = self._indiv_dec_prompt.onset + indiv_dec_rt
            self.control.put('p' + str(my_participant) + '_indiv_dec', (indiv_dec, indiv_dec_rt, indiv_dec_time, self.client))
            self._indiv_dec_meet.manual_reset()
            self._presenter.show(self._indiv_dec_wait_1)
            if not self._indiv_dec_meet.check():
                self._presenter.show(self._indiv_dec_wait_2)
            decs = []
            for p in participants:
                dec, _, _, _ = self.control.get('p' + str(p) + '_indiv_dec')
                decs.append(dec)
            self.control.meet('group_dec_maker_wait', self.client)  # should be v.fast -> presenter can be skipped
            gr_dec_maker = self.control.get('gr_dec_maker')
            feedback_texts_order = self.control.get('feedback_texts_order')
            if gr_dec_maker is not None:
                indiv_dec_table = self._make_screen(self._make_decisions_table(decs, 0.8, feedback_texts_order),
                                                    duration=self.config.getfloat('setting', 'decisions_info_duration'))
                self._presenter.show(self._group_dec_beep_blank, indiv_dec_table)
                if gr_dec_maker == my_participant:
                    self._presenter.show(self._group_dec_prompt)
                    group_dec_key, group_dec_rt = self._group_dec_input.pressed[0]
                    group_dec = self._keys[group_dec_key]
                    group_dec_time = self._group_dec_prompt.onset + group_dec_rt
                    self.control.put('group_dec', (group_dec, group_dec_rt, group_dec_time, self.client))
                    self.control.meet('group_dec_wait', self.client)
                else:
                    self._presenter.show(self._group_dec_wait)
                    group_dec, _, _, _ = self.control.get('group_dec')
            else:
                group_dec = indiv_dec
            feedback = self._make_screen(self._make_feedback_table(decs, group_dec, trial['target_screen_nr'], 0.8, feedback_texts_order),
                                         duration=self.config.getfloat('setting', 'feedback_duration'))
            self._presenter.show(feedback)
            self.log.info('Trial #%d: participant=%d correct=%d answer=%d group=%d contrast_idx=%d pos=%d',
                          trial['trial_idx'], my_participant, trial['target_screen_nr'],
                          indiv_dec, group_dec, trial['contrast_idx'], trial['target_pos'])
            self.control.put('p' + str(my_participant) + '_presenter_data', self._presenter.data())
            self.control.meet('after_trial_wait', self.client)
            self.log.debug('Finished trial #%02d', trial['trial_idx'])

    def _run_subblock_server(self, subblock):
        """
        Run an experimental subblock - server part.

        :param subblock: The subblock to run (A Trials object).

        """
        participants = subblock['participants']
        for trial in subblock.children():
            self.control.put('trial', trial.data())
            self.control.meet('trial_wait', self.client)
            self.control.meet('stim_wait', self.client)
            trial['trial_start'] = self._master_clock.getTime()
            self.control.meet('indiv_dec_wait', self.client)
            decs = []
            for i in range(len(participants)):
                indiv_dec, indiv_rt, indiv_time, indiv_client = self.control.get('p' + str(participants[i]) + '_indiv_dec')
                decs.append(indiv_dec)
                trial['p' + str(participants[i]) + '.indiv_dec'] = int(indiv_dec)
                trial['p' + str(participants[i]) + '.indiv_dec_rt'] = indiv_rt
                trial['p' + str(participants[i]) + '.indiv_dec_time'] = indiv_time
                trial['p' + str(participants[i]) + '.client'] = indiv_client
            # version with randomly selected group responder:
            #gr_dec_maker = random.choice(participants) if not _all_same(decs) else -1
            if not _all_same(decs):
                gr_dec_maker = self._prev_gr_dec_maker
                # version with 'rotating' responder:
                self._prev_gr_dec_maker = (gr_dec_maker + 1) % len(participants)
            else:
                gr_dec_maker = None
            feedback_texts_order = range(len(participants))
            if self._shuffle_feedback_texts:
                random.shuffle(feedback_texts_order)
            trial['group_dec_maker'] = gr_dec_maker
            trial['feedback_texts_order'] = str(feedback_texts_order)
            self.control.put('gr_dec_maker', gr_dec_maker)
            self.control.put('feedback_texts_order', feedback_texts_order)
            self.control.meet('group_dec_maker_wait', self.client)
            if gr_dec_maker > -1:
                self.control.meet('group_dec_wait', self.client)
                group_dec, group_rt, group_time, group_client = self.control.get('group_dec')
                trial['group_dec'] = int(group_dec)
                trial['group_dec_rt'] = group_rt
                trial['group_dec_time'] = group_time
                trial['group_dec_client'] = group_client
            else:
                trial['group_dec'] = int(decs[0])
                trial['group_dec_rt'] = None
                trial['group_dec_time'] = None
                trial['group_dec_client'] = None
            self.control.meet('after_trial_wait', self.client)
            trial['trial_finish'] = self._master_clock.getTime()
            for i in range(len(participants)):
                pd = self.control.get('p' + str(participants[i]) + '_presenter_data')
                for k, v in pd:
                    trial['p' + str(participants[i]) + '.' + k] = v
            self.log.info('Trial #%02d: correct=%d answers=%s group_ans=%d gr_dec_maker=%s contrast=%.3f',
                          trial['trial_idx'], trial['target_screen_nr'],
                          decs, trial['group_dec'], gr_dec_maker if gr_dec_maker is not None else '-', trial['contrast'])
            self.log.debug('Finished trial #%02d', trial.index)
        self.control.put('trial', None)
        self.control.meet('trial_wait', self.client)

    def _make_screen(self, stim, name=None, duration=None, keys=None, meeting=None, on_show_first=lambda: None, other=[], is_static=True):
        """
        Helper for creating a screen.

        :param stim: The visual stimulus to show.
        :param name: The name of the screen, for recording purposes.
        :param duration:The duration, for which the screen is supposed to be shown.
        :param keys: Keys that terminate the screen's presentation.
        :param meeting: A meeting, that should terminate the screen's presentation.
        :param on_show_first: A callable to call on screen onset.
        :param other: Other Checkers for the screen.
        :param is_static: Does the screen contain only animated elements (for optimization purposes).

        :returns: The created screen.

        """
        checkers = list(other)
        if duration is not None:
            checkers.append(exputil.TimeChecker(duration, self._monitor.frame_length))
        if keys is not None:
            checkers.append(exputil.InputChecker(keys, self.evhub))
        if self._abort_key is not None:
            checkers.append(exputil.AbortKeyChecker([self._abort_key], self.evhub, experiment.ExperimentAbortedException))
        if meeting is not None:
            checkers.append(exputil.MeetingChecker(self.control, self.client, meeting))
        if len(checkers) == 0:
            c = exputil.Never()
        elif len(checkers) == 1:
            c = checkers[0]
        else:
            c = exputil.ComplexChecker('or', checkers)
        return exputil.Screen(stim, name, c, on_show_first, is_static)

    #: Names of meeting to create
    _MEETING_NAMES = ['camera_clap_wait',
                      'switch_places_wait',
                      'block_wait',
                      'subblock_wait',
                      'trial_wait',
                      'stim_wait',
                      'indiv_dec_wait',
                      'group_dec_maker_wait',
                      'group_dec_wait',
                      'after_trial_wait',
                      'foobar']

    def _init_meetings(self):
        """
        Create the Meeting objects the experiment uses.

        """
        all_client_names = [c[0] for c in self.info.clients]
        for name in Main._MEETING_NAMES:
            self.control.ensure_meeting(name, all_client_names)

    def abort(self):
        """
        In case of an abort, interrupt all blocking mechanisms.

        """
        if self.client == 'server':
            for name in Main._MEETING_NAMES:
                self.control.abort_meeting(name)

    def _make_decisions_table(self, decisions, size=1.0, text_order=None):
        """
        Create a stimulus representing a table of participants' answers.

        :param decisions: A list of individual decisions.
        :param size: Size of the table, in screen height units.
        :param text_order: A vertical permutation of the texts.
        :returns: The stimuli showing the decisions table.

        """
        w = float(self._window.width)
        h = float(self._window.height)
        n = len(decisions)
        texts = [None] * n
        if text_order is None:
            text_order = range(n)
        for i in range(n):
            txt = self._decision_texts[i + 1][decisions[i]]
            txt.x = w / 2
            txt.y = size * (h * (((n - 1) - i) + 1)) / (n + 3) + (1.0 - size) * h / 2
            texts[text_order[i]] = txt
        return exputil.StimGroup(*texts)

    def _make_feedback_table(self, decisions, group_decision, correct_decision, size=1.0, text_order=None):
        """
        Create a stimulus representing a table of feedback to participants' decisions.

        :param decisions: A list of individual decisions.
        :param group_decision: The group decision.
        :param correct_decision: The correct answer.
        :param size: Size of the table, in screen height units.
        :param text_order: A vertical permutation of the texts.
        :returns: The stimuli showing the feedback table.

        """
        w = float(self._window.width)
        h = float(self._window.height)
        n = len(decisions)
        texts = [None] * n
        if text_order is None:
            text_order = range(n)
        for i in range(n):
            txt = self._feedback_texts[i + 1][0 if decisions[i] == correct_decision else 1]
            txt.x = w / 2
            txt.y = size * (h * (((n - 1) - i) + 1)) / (n + 2) + (1.0 - size) * h / 2
            texts[text_order[i]] = txt
        txt = self._feedback_texts[0][0 if group_decision == correct_decision else 1]
        txt.x = w / 2
        txt.y = size * (h * (n + 1)) / (n + 2) + (1.0 - size) * h / 2
        texts.append(txt)
        return exputil.StimGroup(*texts)

    def _make_text_stim(self, text, color=(255, 255, 255, 255), pos=None, text_size=None):
        """
        Helper for creates a text stimulus.

        text - The text to appear.
        color - The color of the text.
        pos - The position of the text, in default units.
        text_size - Size of the text (duh...) in visual degrees.

        The created stimulus object.

        """
        if len(color) == 3:
            color = color + (255,)
        if not pos:
            pos = (self._window.width / 2, self._window.height / 2)
        text_size = text_size if text_size else self._text_size
        dpi = calc_dpi(self._window, self._monitor)
        dpi = int(dpi)
        font_size_pt = visdegrees_to_pt(text_size, self._monitor, dpi)
        label = pyglet.text.Label(text=text,
                                  x=pos[0], y=pos[1],
                                  font_name=self._text_font,
                                  font_size=font_size_pt,
                                  color=color,
                                  anchor_x='center', anchor_y='center',
                                  dpi=dpi,
                                  #halign='center',  # cannot set halign here due to a bug in pyglet
                                  multiline=True, width=self._window.width)
        label.set_style('align', 'center')  # workaround halign bug above
        return label

    def _make_trials(self, n_blocks_prac, n_blocks_exper, participants):
        """
        Crate trials for entire experiment.

        :param n_block_prac: Number of practice blocks to create.
        :param n_blocks_exper: Number of experimental blocks to create.
        :param participants: A list of participant ID's.
        :returns: A Trials object containing trial data for the entire experiment.

        """
        participants = list(participants)  # local copy
        switch_places = self.config.getboolean('procedure', 'switch_places')
        trials = Trials(indexer=None, index_name='block_type')
        n_patches = self.config.getint('stimulus', 'patches_in_circle')
        n_subblocks_prac = self.config.getint('procedure_practice', 'n_subblocks')
        prac = trials.add_child({'practice': True, 'no_blocks': n_blocks_prac}, index='practice', index_name='block_idx')
        prac_reps = [int(s.strip()) for s in self.config.get('procedure_practice', 'n_contrast_repeats').split(',')]
        for _ in range(n_blocks_prac):
            block = prac.add_child({'no_subblocks': n_subblocks_prac, 'participants': participants}, index_name='subblock_idx')
            for _ in range(n_subblocks_prac):
                subblock = block.add_child({}, index_name='trial_idx')
                self._make_subblock(subblock, prac_reps, n_patches)
        n_subblocks_exper = self.config.getint('procedure', 'n_subblocks')
        exper = trials.add_child({'practice': False, 'no_blocks': n_blocks_exper}, index='experimental', index_name='block_idx')
        exper_reps = [int(s.strip()) for s in self.config.get('procedure', 'n_contrast_repeats').split(',')]
        for _ in range(n_blocks_exper):
            block = exper.add_child({'no_subblocks': n_subblocks_exper, 'participants': participants}, index_name='subblock_idx')
            for _ in range(n_subblocks_exper):
                subblock = block.add_child({}, index_name='trial_idx')
                self._make_subblock(subblock, exper_reps, n_patches)
            if switch_places and block['block_idx'] < block['no_blocks'] - 1:
                participants = _rotated(participants, 1)
        return trials

    def _make_subblock(self, subblock, repeats, n_patches):
        """
        Create variable values for a subblock.

        :param subblock: The Trials object to add data to.
        :param repeats: Number of repeats of each condition combinations.
        :param n_patches: Number of Ganor patches in a single set.

        """
        items = []
        for ci in range(1, len(self._contrasts)):
            for _ in range(repeats[ci - 1]):
                items.append({'target_screen_nr': 0,
                              'target_pos': random.randint(0, n_patches - 1),
                              'contrast_idx': ci,
                              'contrast': self._contrasts[ci]})
                items.append({'target_screen_nr': 1,
                              'target_pos': random.randint(0, n_patches - 1),
                              'contrast_idx': ci,
                              'contrast': self._contrasts[ci]})
        random.shuffle(items)
        for itm in items:
            subblock.add_child(itm)

    @property
    def evhub(self):
        """
        Event hub used for IO event checking.

        """
        return self._evhub
