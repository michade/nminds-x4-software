"""
.. Copyright (c) 2014 Institute of Psychology of the Polish Academy of Sciences.
   All rights reserved. For full license text (in Polish) please see LICENSE.polish
  
   Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)

   This file is a part of the nminds-x4 program, used to conduct psychological
   experiments on group decision-making. For more information please see README file.

This module contains the server program, represented by an instance of `Server`,
which is an intermediary and coordinator between clients. Also accepts
command line input regarding e.g. starting an experiment.

USAGE: server.py [-h] [-a ADDR] [-p PORT] [-c CONFIG_FILE] [-x EXECUTE] [-n]
                 [-l LOG_LEVEL] [--hold]

optional arguments:
  -h, --help      show this help message and exit
  -a ADDR         Bind address.
  -p PORT         Port to listen on.
  -c CONFIG_FILE  Config file to use.
  -x EXECUTE      Execute COMMAND when N clients connect.
  -n              Do not create an input console. Quit after an experiment has
                  finished
  -l LOG_LEVEL    Log level of console output, one of: [d]ebug, [i]nfo
                  (default), [w]arning, [e]rror, [c]ritical.
  --hold          Keeps the console open after termination.

"""

import argparse
import logging
from logging.config import dictConfig
import exprunner.common as common
from exprunner.server import Server

#: Logger for use by Client instances (and related objects).
log = logging.getLogger('server')


def main():
    """
    Runnable server script.

    """
    parser = argparse.ArgumentParser(description='exprunner server.')
    parser.add_argument('-a', dest='addr', type=common.valid_addr, help='Bind address.')
    parser.add_argument('-p', dest='port', type=common.valid_port, help='Port to listen on.')
    parser.add_argument('-c', dest='config_file', help='Config file to use.')
    parser.add_argument('-x', dest='execute', help='Execute COMMAND when N clients connect.')
    parser.add_argument('-n', dest='no_console', action='store_true', help='Do not create an input console. Quit after an experiment has finished')
    parser.add_argument('-l', dest='log_level', type=common.valid_log_level, help='Log level of console output, one of: [d]ebug, [i]nfo (default), [w]arning, [e]rror, [c]ritical.')
    parser.add_argument('--hold', action='store_true', help='Keeps the console open after termination.')
    args = parser.parse_args()
    hold = args.hold
    config = common.Config({'name': 'server'})
    config_file = args.config_file if args.config_file is not None else common.CONFIG_FILE
    loaded_configs = config.read(config_file)
    if config_file not in loaded_configs:
        raise Exception('Unable to load client config file: ' + config_file)
    for lc in loaded_configs:
        log.info('Config loaded: %s', lc)
    if args.addr is not None:
        config.set('connection', 'server_ip', args.addr)
    if args.port is not None:
        config.set('connection', 'server_port', args.port)
    if args.no_console is not None:
        config.set('misc', 'console', str(not args.no_console))
    if args.log_level is not None:
        config.set('misc', 'log_level', args.log_level)
    dictConfig(common.get_default_logging(config.get('paths', 'log'),
                                          file_level='DEBUG',
                                          console_level=config.get('misc', 'log_level')))
    log.debug('Creating server.')
    server = Server(config)
    log.debug('Server created.')
    if server.bind():
        print 'Server bound to %s:%d' % server.get_bind_info()
        if args.execute is not None:
            s = args.execute.split(None, 1)
            server.new_client_hook = (int(s[0]), s[1])
        if not config.getboolean('misc', 'console'):
            server.experiment_done_hook = 'q'
        server.loop()
        if hold:
            print 'Server has exited. Press ENTER to continue.'
            raw_input()
        else:
            print 'Server has exited.'
    else:
        print 'Unable to bind.'


if __name__ == '__main__':
    main()
