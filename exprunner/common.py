"""
.. Copyright (c) 2014 Institute of Psychology of the Polish Academy of Sciences.
   All rights reserved. For full license text (in Polish) please see LICENSE.polish
  
   Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)

   This file is a part of the nminds-x4 program, used to conduct psychological
   experiments on group decision-making. For more information please see README file.

Utility functions used by both client and server.

"""

import argparse
import re
import ConfigParser
import os

#: The default name of the config files.
CONFIG_FILE = 'config.txt'


def get_default_logging(logfile, file_level='DEBUG', console_level='INFO'):
    """
    Returns the default logging configuration to be passed to logging.config.dictConfig.
    Contains a console and file logger.

    Arguments:
    :param logfile: Name of the log file.
    :param file_level: Logging level for the log file.
    :param console_level: Logging level for the conssole output.

    """
    return {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'default': {
                'format': '%(threadName)s\t%(levelname)s:%(name)s:\t%(message)s'
            }
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'level': console_level,
                'formatter': 'default'},
            'file': {
                'class': 'logging.FileHandler',
                'level': file_level,
                'formatter': 'default',
                'filename': logfile,
                'mode': 'w'},
            'null': {'class': 'logging.NullHandler'}
        },
        'loggers': {
            'msgsocket': {'handlers': ['null'], 'propagate': False}
        },
        'root': {
            'level': 'DEBUG',
            'handlers': ['console', 'file']
        }
    }


def valid_name(s):
    """
    If string `s` is a valid name for a client, returns it.
    Raises ArgumentTypeError otherwise. For use in argparse.

    """
    res = re.match("\A[\w\-]{1,16}\Z", s)
    if res is None:
        msg = "\"%s\" is not a valid client _name." % s
        raise argparse.ArgumentTypeError(msg)
    return res.group(0)


def valid_addr(s):
    """
    If string `s` is a valid addr part of socket connection, returns it.
    Raises ArgumentTypeError otherwise. For use in argparse.

    """
    res = re.match('\A([\w\.\:]+)\Z', s)
    if res is not None:
        return res.group(1)
    else:
        msg = '\"%s\" is not a valid address.' % s
        raise argparse.ArgumentTypeError(msg)


def valid_port(s):
    """
    If string `s` is a valid port number for a client, returns it (as an int).
    Raises ArgumentTypeError otherwise. For use in argparse.

    """
    port = int(s)
    if port >= 256 and port <= 65535:
        return port
    else:
        msg = '%d is not a valid, usable port number.' % port
        raise argparse.ArgumentTypeError(msg)

#: Log level codes.
_LOG_LEVELS = {
    'd': 'debug',
    'i': 'info',
    'w': 'warning',
    'e': 'error',
    'c': 'critical',
    'debug': 'debug',
    'info': 'info',
    'warning': 'warning',
    'error': 'error',
    'critical': 'critical'}


def valid_log_level(s):
    """
    Checks if a string represents a valid log level name.

    """
    if s.lower() in _LOG_LEVELS:
        return _LOG_LEVELS[s].upper()
    else:
        raise argparse.ArgumentTypeError('"{}" is not a valid log level name'.format(s))


class Config(object):
    """
    A wrapper around ConfigParser.ConfigParser, adding the capability of loading
    alternate test configuration if _TEST_MODE_OPTION is specified as True.

    For more details see ConfigParser.ConfigParser documentation.

    """

    def __init__(self, defaults):
        """
        Initializes the object.

        :param defaults: The default values to the ConfigParser initializer.

        """
        self._config = ConfigParser.ConfigParser(defaults)
        self.test_mode = False

    def read(self, files):
        """
        Reads in the data from `files`, and from alternate (test)
        files (with names same as originals, but with '_test' appended).

        :param files: list of config file names.
        :returns: list of names of loaded files.

        """
        res = self._config.read(files)
        if self.test_mode:
            res.extend(self._config.read([self._test_name(f) for f in files]))
        return res

    def get(self, section, key):
        """
        Get an entry `key` from `section` as a string.

        """
        return self._config.get(section, key)

    def getint(self, section, key):
        """
        Get an entry `key` from `section` as an int.

        """
        return self._config.getint(section, key)

    def getfloat(self, section, key):
        """
        Get an entry `key` from `section` as a float.

        """
        return self._config.getfloat(section, key)

    def getboolean(self, section, key):
        """
        Get an entry `key` from `section` as a boolean.

        """
        return self._config.getboolean(section, key)

    def set(self, section, key, value):
        """
        Sets a config variable.

        :param section: The section of the variable to set.
        :param key: The name of the config varaible to set.
        :param value: The value to assign to the variable.

        """
        self._config.set(section, key, value)

    #: The key for the test mode flag. Specifying whether to load test files.
    _TEST_MODE_OPTION = 'test_mode'

    #: Section to place the _TEST_MODE_OPTION
    _TEST_MODE_SECTION = 'test_mode'

    @property
    def test_mode(self):
        """
        I the test mode active?

        """
        return self._config.getboolean(Config._TEST_MODE_SECTION, Config._TEST_MODE_OPTION)

    @test_mode.setter
    def test_mode(self, value):
        """
        Turns on/off the test mode. Changes only how the files are loaded.

        """
        if not self._config.has_section(Config._TEST_MODE_SECTION):
            self._config.add_section(Config._TEST_MODE_SECTION)
        self._config.set(Config._TEST_MODE_SECTION, Config._TEST_MODE_OPTION, str(value))

    def _test_name(self, name):
        """
        Returns the test version of `name`.

        """
        base, ext = os.path.splitext(name)
        return base + '_test' + ext

    def clone(self):
        """
        Clones this instance.

        """
        clone = Config(self._config.defaults())
        for sect in self._config.sections():
            if not clone._config.has_section(sect):
                clone._config.add_section(sect)
            for name, value in self._config.items(sect, raw=True):
                clone._config.set(sect, name, value)
        return clone


def make_log_message_id(n):
    """
    Formats a key for a log message with a given id.

    :param n: id(number) of a message.
    :returns: key, under which this message is stored in the rpc system  (in `logs` directory).

    """
    return 'm{:d}'.format(n)
