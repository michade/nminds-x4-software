"""
.. Copyright (c) 2014 Institute of Psychology of the Polish Academy of Sciences.
   All rights reserved. For full license text (in Polish) please see LICENSE.polish
  
   Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)

   This file is a part of the nminds-x4 program, used to conduct psychological
   experiments on group decision-making. For more information please see README file.

An object managing input/output events events, according to the reactor pattern.

"""

import socket
import logging
import threading
import Queue
from select import select
import exprunner.msgsocket as msgsocket
from exprunner.msgsocket import addr_to_readable_str

#: Logger used by objects in this module.
log = logging.getLogger(__name__)


class Multiplexer(object):
    """
    An object managing input/output events events, according to the reactor pattern.
    Uses subclasses of RawHandler to handle the events, and Worker instances to manage threads.

    Should actually be renamed `Reactor`.

    """

    def __init__(self):
        """
        Initializes the Multiplexer object.

        """
        self._interrupter = _InterruptHandler(self)
        self._loopback_send_lock = threading.Lock()
        self._handlers = [self._interrupter]
        self._shutdown = threading.Event()
        self._main_worker = None
        self._workers = []

    def _do_loop(self):
        """
        The inner logic of the  main reactor loop.
        Uses `select` to respond to input/output events.

        """
        while self._continue_loop():
            lists = self._create_select_lists(self._handlers)
            rready, wready, xready = select(lists[0], lists[1], lists[2])
            hlist = self._mark_ready_handlers(rready, wready, xready)
            for h in hlist:
                h.handle_raw()
                h.clear_ready()

    def _continue_loop(self):
        """
        Should the main loop continue?

        """
        if not self._shutdown.is_set():
            log.debug('Multiplexer loop continues: shutdown flag not set.')
            return True
        #log.debug(self._get_handlers_info())
        for w in self._workers:
            if not w.is_done():
                log.debug('Multiplexer loop continues: worker %s still active.', w.name)
                return True
        for h in self._handlers:
            if not h.is_done():
                log.debug('Multiplexer loop continues: active handler remaining.')
                return True
        return False

    def _get_handlers_info(self, brief=False):
        """
        Returns a string conatining info on handlers and workers present,
        for debugging purposes.

        :param brief: Return a shorter version (only counts)?

        """
        cnt = 'Active: {}/{} handlers, {}/{} workers\n'.format(
            len([h for h in self._handlers if not h.is_done()]), len(self._handlers),
            len([w for w in self._workers if not w.is_done()]), len(self._workers))
        if brief:
            return cnt
        s = '\n=================\n'
        s += cnt
        for h in self._handlers:
            s += ('-' if h.is_done() else '+') + ' ' + str(h) + '\n'
        for w in self._workers:
            s += ('-' if w.is_done() else '+') + ' ' + str(w) + '\n'
        s += '================='
        return s

    def loop(self):
        """
        Starts the reactor (i.e. its event handling loop).

        """
        for h in list(self._handlers):
            h.start()
        if self._main_worker is None:
            self._do_loop()
        else:
            loop_worker = self.get_worker(self._do_loop, "MultiplexerLoop")
            loop_worker.set_done()
            loop_worker.start()
            self._main_worker.run()
            loop_worker.join()
            self._workers.remove(loop_worker)
        self.on_shutdown()
        while len(self._handlers) > 0:
            h = self._handlers.pop()
            h.close()
            log.debug('Handler closed')
        while len(self._workers) > 0:
            w = self._workers.pop()
            w.join()
            log.debug('Worker joined: %s', w.name)

    def _create_select_lists(self, handler_list):
        """
        Sorts handlers into three lists, based on the IO operations
        a handler is subscribed to, for use in the select function.
        One handler can be present in multiple output lists.

        :param handler_list: The list of handlers to sort.

        :returns: A tuple contatining three lits of handlers subscribing to read, write and special IO events.

        """
        rlist, wlist, xlist = [], [], []
        for h in handler_list:
            if h.request_r:
                rlist.append(h)
            if h.request_w:
                wlist.append(h)
            if h.request_x:
                xlist.append(h)
        return rlist, wlist, xlist

    def _mark_ready_handlers(self, rready, wready, xready):
        """
        For handlers, for which IO events appear, marks ready flags.

        :param rready: List of handlers for which a read event is available.
        :param wready: List of handlers for which a write event is available.
        :param xready: List of handlers for which a special event is available.

        :returns: A list of all handlers, for which any events are available.
        Each handler appearrs only once.

        """
        hlist = []
        for h in rready:
            h.ready_r = True
            hlist.append(h)
        for h in wready:
            h.ready_w = True
            if not h.ready_r:
                hlist.append(h)
        for h in xready:
            h.ready_x = True
            if not (h.ready_r or h.ready_w):
                hlist.append(h)
        return hlist

    def add_handler(self, handler):
        """
        Adds a handler to the reactor.

        :param handler: The handler instance to add.

        """
        self._handlers.append(handler)

    def remove_handler(self, handler):
        """
        Removes a handler from the reactor.

        :param handler: The handler instance to remove.

        """
        self._handlers.remove(handler)

    def shutdown(self):
        """
        Notifies the workers about the reactor shutting down.

        """
        self._shutdown.set()
        for h in self._handlers:
            h.shutdown()
        for w in self._workers:
            w.shutdown()
        self.interrupt()

    def is_shutting_down(self):
        """
        Is the reactor about to exit its loop?

        """
        return self._shutdown.is_set()

    def on_shutdown(self):
        """
        Action to be taken after to shutdown. To be overriden in subclasses.

        """
        pass

    def interrupt(self):
        """
        Interrupts the IO wait (`selsect`).

        An interrupt channel is a loopback connection,
        that wakes up the reactor from a call to `select`, so
        that it can handle events not comming from sockets.
        Basically it is a workaraoud the limitations of `select`.

        """
        with self._loopback_send_lock:
            self._interrupter.interrupt()

    def get_worker(self, target, name=None):
        """
        Creates a new Worker.

        :param target: Function or bound method to run in another thread.
        :param name: Name of the new thread.

        :returns: The new Worker.

        """
        worker = Worker(self, target, name)
        self._workers.append(worker)
        return worker

    def get_main_worker(self, target):
        """
        Creates a new Worker, that uses the application's main thread of control.
        Certain tasks (e.g. graphics) can only be performed in the main thread.

        :param target: Function or bound method to run in the main thread.

        :returns: The new Worker.

        """
        if self._main_worker is not None:
            raise Exception("Attempt to create more than one MainThreadWorker")
        self._main_worker = MainThreadWorker(self, target)
        self._workers.append(self._main_worker)
        return self._main_worker


class MainThreadWorker(object):
    """
    A wrapper around thread logic. This particular class uses the thread
    in which it was creted, instead of a new one.
    Intended to be created in the main thread, as certain tasks (e.g. graphics)
    can only be performed in the main thread.
    After creation call the `run` method to give control to the worker object.

    """

    def __init__(self, owner, target):
        """
        Initializes the object.

        :param owner: Owning Multiplexer instance.
        :param target: A callable object, that will be executed in the main thread. (in the `run` method).

        """
        self._owner = owner
        self._target = target
        self._started = threading.Event()
        self._is_done = threading.Event()
        self._name = "MainThreadWorker"
        log.debug('Main thread worker created.')

    def start(self):
        """
        Starts the worker.

        """
        self._started.set()

    def run(self):
        """
        Executes the `target` in the main thread.

        """
        self._started.wait()
        log.debug('MainThreadWorker started.')
        self._target()
        log.debug('MainThreadWorker finished.')
        self.set_done()
        self.owner.interrupt()

    def join(self):
        """
        Actually does nothing, exists for compatibility with the Worker class.

        """
        if not self._started.is_set():
            raise Exception("Attempt to join a thread that has not been started.")

    def shutdown(self):
        """
        Notifies the worker about reactor shutdown.

        """
        self.on_shutdown()

    def on_shutdown(self):
        """
        Action to be taken on reactor shutdown.

        """
        pass

    def set_done(self):
        """
        Mark the worker as done.

        """
        self._is_done.set()

    def is_done(self):
        """
        Has the worker completed its task?

        """
        return self._is_done.is_set()

    @property
    def name(self):
        """
        The name of the underlying thread.

        """
        return self._name

    @name.setter
    def name(self, value):
        """
        Silently fails.

        """
        pass  # fail silently

    @property
    def owner(self):
        return self._owner


class Worker(threading.Thread):
    """
    A wrapper around thread logic. Specify the code to be executed
    in the `target` parameter of __init__.

    """

    def __init__(self, owner, target, name=None):
        """
        Initializes the object.

        :param owner: Owning Multiplexer instance.
        :param target: A callable object, that will be executed in a separate thread.
        :param name: Name for the new thread.

        """
        self._is_done = threading.Event()
        threading.Thread.__init__(self, target=target, name=name)
        self._owner = owner
        log.debug('Thread \'%s\' created.', self.name)

    def run(self):
        """
        Executes the `target` in the woker's private thread.

        """
        log.debug('Thread \'%s\' started.', self.name)
        super(Worker, self).run()
        log.debug('Thread \'%s\' finished.', self.name)
        self.set_done()

    def shutdown(self):
        """
        Notifies the worker about reactor shutdown.

        """
        self.on_shutdown()

    def on_shutdown(self):
        """
        Action to be taken on reactor shutdown.

        """
        pass

    def set_done(self):
        """
        Mark the worker as done.

        """
        self._is_done.set()

    def is_done(self):
        """
        Has the worker completed its task?

        """
        return self._is_done.is_set()


class RawHandler(object):
    """
    Base class for handlers handling raw, socket-level IO events.

    Override `handle_raw` to provide custom behaviour.

    """

    def __init__(self, socket_, owner):
        """
        Initializes the object.

        :param socket: The connected socket, which this handler tracks.
        :param owner: The owning Multiplexer instance.

        """
        self._socket = socket_
        self._owner = owner
        self._ready_r = False
        self._ready_w = False
        self._ready_x = False
        self._request_r = False
        self._request_w = False
        self._request_x = False
        self._is_done = threading.Event()
        self._is_closed = threading.Event()

    def handle_raw(self):
        """
        Called to handle raw socket IO event.

        Subclasses override this method with their own code.

        """
        pass

    def fileno(self):
        """
        Gets the fileno of the underlying socket.

        """
        return self._socket.fileno()

    def clear_ready(self):
        """
        Clears alle flags, which indicate that some events have occured.

        """
        self._ready_r = False
        self._ready_w = False
        self._ready_x = False

    def clear_requests(self):
        """
        Clears all flags indicating requests for socket events.

        """
        self._request_r = False
        self._request_w = False
        self._request_x = False

    def close(self):
        """
        Closes the underlying socket.

        """
        self._is_closed.set()
        self.clear_requests()
        self.clear_ready()
        self.set_done()  # just in case
        if self._socket is not None:
            self._socket.close()
        self.owner.interrupt()

    def start(self):
        """
        Called to when the handler starts operating.

        Subclasses override this method with their own code.

        """
        pass

    def set_done(self):
        """
        Mark the worker as done.

        """
        self._is_done.set()

    def is_done(self):
        """
        Has the worker completed its task?

        """
        return self._is_done.is_set()

    def is_closed(self):
        return self._is_closed.is_set()

    def shutdown(self):
        """
        Notifies the worker about reactor shutdown.

        """
        self.on_shutdown()

    def on_shutdown(self):
        """
        Action to be taken on reactor shutdown.

        """
        pass

    @property
    def owner(self):
        """
        The owning Multiplexer instance.

        """
        return self._owner

    @property
    def request_r(self):
        """
        Request a read operation?

        """
        return self._request_r

    @request_r.setter
    def request_r(self, value):
        """
        Request a read operation?

        """
        if not self.is_closed():
            self._request_r = value

    @property
    def request_w(self):
        """
        Request a write operation?

        """
        return self._request_w

    @request_w.setter
    def request_w(self, value):
        """
        Request a write operation?

        """
        if not self.is_closed():
            self._request_w = value

    @property
    def request_x(self):
        """
        Request a special operation?

        """
        return self._request_x

    @request_x.setter
    def request_x(self, value):
        """
        Request a special operation?

        """
        if not self.is_closed():
            self._request_x = value

    @property
    def ready_r(self):
        """
        Is a read operation ready for this handler?

        """
        return self._ready_r

    @ready_r.setter
    def ready_r(self, value):
        """
        Sets the read operation flag.

        """
        self._ready_r = value

    @property
    def ready_w(self):
        """
        Is a write operation ready for this handler?

        """
        return self._ready_w

    @ready_w.setter
    def ready_w(self, value):
        """
        Sets the write operation flag.

        """
        self._ready_w = value

    @property
    def ready_x(self):
        """
        Is a special operation ready for this handler?

        """
        return self._ready_x

    @ready_x.setter
    def ready_x(self, value):
        """
        Sets the special operation flag.

        """
        self._ready_x = value


class MessageHandler(RawHandler):
    """
    Base class for handlers handling message passing events.

    Override `handle_message`, `handle_introduction`,
    and `handle_disconnect` to provide custom behaviour.

    Call `send` to send a message.

    """

    def __init__(self, socket, addr, owner, is_introduced=True):
        """
        Initializes the object.

        :param socket: The connected socket, which this handler tracks.
        :param addr: Address and port of the remote endpoint.
        :param owner: The owning Multiplexer instance.
        :param is_introduced: Have the introductory messages already benn passed?

        """
        super(MessageHandler, self).__init__(socket, owner)
        self._is_introduced = is_introduced
        if isinstance(socket, msgsocket.MsgSocket):
            self._mu = socket
        else:
            self._mu = msgsocket.MsgSocket((socket, addr))

    def handle_read(self):
        """
        Reads availible data from the undelrying socket.

        """
        lst = self._mu.recv()
        if lst is None:
            self.clear_requests()
            self.handle_disconnect()
            return
        for msg in lst:
            if self._is_introduced:
                self.handle_message(msg)
            elif self.handle_introduction(msg):
                self._is_introduced = True
            else:
                log.error('Error introducing %s - closing.', self.get_remote_addr())
                self.close()

    def handle_write(self):
        """
        Writes enqueued data to the undelrying socket.

        """
        more_to_send = self._mu.send()
        if more_to_send is None:
            self.clear_requests()
            self.handle_disconnect()
            return
        self.request_w = more_to_send

    def handle_x(self):
        """
        Handles special socket events. Unused.

        """
        log.debug("Special socket event has occurred.")

    def handle_disconnect(self):
        """
        Called to handle disconnection.

        Subclasses override this method with their own code.

        """
        pass

    def handle_raw(self):
        """
        Handles raw input by calling appropriate helper methods.

        """
        if self._ready_r:
            self.handle_read()
        if self._ready_w:
            self.handle_write()
        if self._ready_x:
            self.handle_x()

    def handle_message(self, msg):
        """
        Called to handle an incomming message.

        Subclasses override this method with their own code.

        :param msg: The message to handle.

        """
        pass

    def handle_introduction(self, msg):
        """
        Called to handle an introducotry message.

        Subclasses override this method with their own code.

        :param msg: The intrudoctory message to handle.

        :returns: True, if introduction was successful.

        """
        return True

    def send(self, msg):
        """
        Send a message trough the connection.

        Intended to be used by subclass code.

        """
        msg.sender = self.owner.name
        self._mu.push(msg)
        self.request_w = True

    def close(self):
        """
        Close the connection and unregister the handler.

        """
        self.clear_requests()
        self._mu.close()
        super(MessageHandler, self).close()

    def get_remote_addr(self):
        """
        Gets the address and port of the remote endpoint.

        """
        return self._mu.remote_addr

    def is_closed(self):
        return self._is_closed.is_set()


class ThreadedMessageHandler(MessageHandler):
    """
    Base class for handlers handling message passing events.
    The messages are handled in a separate thread.

    Override `thread_handled_message`, `handle_introduction`,
    and `handle_disconnect` to provide custom behaviour.

    Call `send` to send a message.

    """

    #: A special object to pass to the queue to indicate shutting down.
    _MESSAGE_END = object()

    def __init__(self, socket, addr, owner, threadname=None, run_as_main=False, is_introduced=True):
        """
        Initializes the object.

        :param socket: The connected socket, which this handler tracks.
        :param addr: Address and port of the remote endpoint.
        :param owner: The owning Multiplexer instance.
        :param threadname: The name for the handler's message-handling thread.
        :param run_as_main: Handle the messages in the application's main thread?
        :param is_introduced: Have the introductory messages already benn passed?

        """
        super(ThreadedMessageHandler, self).__init__(socket, addr, owner, is_introduced)
        if run_as_main:
            self._worker = owner.get_main_worker(self.run_thread)
        else:
            if threadname is None:
                threadname = msgsocket.addr_to_readable_str(addr)
            self._worker = owner.get_worker(self.run_thread, threadname)
        self._inqueue = Queue.Queue()
        self._send_lock = threading.Lock()

    def handle_message(self, msg):
        """
        Enqueues the message to handle it in a separate thread.

        """
        self._inqueue.put(msg)

    def thread_handle_message(self, msg):
        """
        Called to handle an incomming message.
        Runs in a handler's private thread.

        Subclasses override this method with their own code.

        :param msg: The message to handle.

        """
        pass

    def run_thread(self):
        """
        A method running in the message-handling thread.

        """
        while True:
            msg = self.get_message()
            if msg is None:
                break
            self.thread_handle_message(msg)

    def get_message(self):
        """
        Block until a message is available in the queue, and
        retreive it.

        :returns: The retreived message, or None if the handler is to be terminated.

        """
        msg = self._inqueue.get()
        if msg is ThreadedMessageHandler._MESSAGE_END:
            return None
        return msg

    def send(self, msg):
        """
        Send a message trough the connection.
        Can be safely called from the message-handling thread.

        Intended to be used by subclass code.

        """
        with self._send_lock:
            super(ThreadedMessageHandler, self).send(msg)
        self.owner.interrupt()

    def handle_write(self):
        """
        Writes enqueued data to the undelrying socket.

        """
        with self._send_lock:
            return super(ThreadedMessageHandler, self).handle_write()

    def start(self):
        """
        Called when the handler starts operating.

        """
        self.request_r = True
        self._worker.start()

    def close(self):
        """
        Close the connection.

        """
        self._inqueue.put(ThreadedMessageHandler._MESSAGE_END)
        super(ThreadedMessageHandler, self).close()

    def get_name(self):
        """
        Gets the name of the message-handling thread.

        """
        return self._worker.name

    def set_name(self, name):
        """
        Sets the name of the message-handling thread.

        """
        self._worker.name = name


class TcpListener(RawHandler):
    """
    A handler listening for new TCP connections.

    """

    def __init__(self, owner, make_handler=None):
        """
        Initializes the object.

        :param owner: The owning Multiplexer instance.
        :param make_handler: A callable that accepts a newly connected socket and an
        address of the remote endpoint; Returns a handler to handler
        the new connection.

        """
        RawHandler.__init__(self, None, owner)
        self._make_handler = make_handler
        self.bind_info = None

    def handle_raw(self):
        """
        Accepts a new connection.

        """
        sock, addr = self._socket.accept()
        self.handle_accept(sock, addr)

    def handle_accept(self, sock, addr):
        """
        Uses `make_handler` to create a handler for a new connection,
        then registers and starts the new handler.

        """
        if self._make_handler:
            h = self._make_handler(sock, addr)
            self._owner.add_handler(h)
            h.start()

    def set_accepting(self, val):
        """
        Set the flag controlling whether this handler accepts new connections.

        """
        self.request_r = val

    def is_accepting(self):
        """
        A flag controlling whether this handler accepts new connections.

        """
        return self.request_r

    def bind(self, host, port, reuse_addr, backlog=5):
        """
        Binds the socket used for receiving new connections.

        :param host: Local bind address.
        :param port: Local port used for bind call.
        :param reuse_addr: Reuse recently used address for binding?
        :param backlog: maximum number of connection requests kept by the OS waiting to
                        be accepted. Further requests are automatically rejected (by OS),
                        until the waiting connetion requests are handled.

        """
        infos = socket.getaddrinfo(host, port, socket.AF_UNSPEC, socket.SOCK_STREAM, 0, socket.AI_PASSIVE)
        for ifo in infos:
            try:
                self.bind_to(ifo, reuse_addr, backlog=backlog)
            except socket.error as err:
                log.info('Unable to bind to %s: %s', ifo, err)
                continue
            self.bind_info = ifo[4]
            return True
        return False

    def bind_to(self, info, reuse_addr, backlog=5):
        """
        Bind to a specific addrinfo. See socket.getaddrinfo docs for more details.

        """
        family, socktype, proto, _, sockaddr = info
        try:
            s = socket.socket(family, socktype, proto)
            if reuse_addr:
                s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            s.bind(sockaddr)
            s.listen(backlog)
            self._socket = s
            log.info('Listening socket bound to %s', addr_to_readable_str(sockaddr))
        except socket.error as err:
            if s:
                s.close()
            raise err

    def on_shutdown(self):
        self.set_accepting(False)
        self.set_done()


class LoopbackConnection(object):
    """
    A pair of sockets on the same python process, connected with each other.
    Used to overcome the limitations of python's `select` implementation.

    """

    #: The ip address to be used for loopback connections.
    _LOOPBACK_IP = '127.0.0.1'

    #: The port to be used for loopback connections.
    _LOOPBACK_PORT = 12345

    def __init__(self, reuse_addr):
        """
        Initializes the object.

        :param reuse_addr: Reuse the discarded binding addresses? (see socket.socket docs).

        """
        self._listen_sock = None
        self._sockets = [None, None]
        try:
            self._listen_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            if reuse_addr:
                self._listen_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self._listen_sock.bind((LoopbackConnection._LOOPBACK_IP, 0))
            #self._listen_sock.bind((LoopbackConnection._LOOPBACK_IP, LoopbackConnection._LOOPBACK_PORT))
            self._listen_sock.listen(1)
        except socket.error as err:
            log.error("Error creating passive loopback socket: %s", err)
            if self._listen_sock:
                self._listen_sock.close()
            raise err
        try:
            connect_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            if reuse_addr:
                connect_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self._sockets[0] = connect_sock
        except socket.error as err:
            log.error('Error creating active loopback socket: %s', err)
            if connect_sock:
                connect_sock.close()
            raise err
        partner = threading.Thread(name='TMP_FOR_LOOPBACK', target=self._do_accept)
        partner.start()
        try:
            addr = self._listen_sock.getsockname()
            self._sockets[0].connect(addr)
        except socket.error as err:
            log.critical("Unable to connect to loopback socket %s:%d, because: %s", addr[0], addr[1], err)
            self.close()
            raise err
        finally:
            partner.join()

    def _do_accept(self):
        """
        Accepts the loopback connection.

        """
        self._sockets[1], _ = self._listen_sock.accept()
        self._listen_sock.close()
        self._listen_sock = None

    def close(self):
        """
        Closes the underlying sockets.

        """
        if self.is_alive():
            sock = self._sockets
            self._sockets = None
            sock[0].close()
            sock[1].close()

    def get_socket(self, i):
        """
        Gets the i-th socket (there are two).

        """
        return self._sockets[i]

    def is_alive(self):
        """
        Is the connection alive?

        """
        return self._sockets is not None


class _InterruptHandler(RawHandler):
    """
    Handles the Multiplexers interrupt channel - a loopback
    connection, that wakes up the reactor from a call to `select`, so
    that it can handle events not comming from sockets.
    Basically it is a workaraoud the limitations of `select` (lack of
    cross-OS portability for signals).

    """

    def __init__(self, owner, reuse_addr=True):
        """
        Initializes the object.

        :param socket: The socket, which this handler tracks.
        :param reuse_addr: Reuse the discarded binding addresses? (see socket.socket docs).

        """
        self._interrupter = LoopbackConnection(reuse_addr)
        super(_InterruptHandler, self).__init__(self._interrupter.get_socket(1), owner)
        self.request_r = True
        self.set_done()

    def handle_raw(self):
        """
        Handles data incomming trough the interrupt channel.

        """
        if self._interrupter.is_alive():
            s = self._interrupter.get_socket(1).recv(32)
            if len(s) == 0:
                log.warning("Loopback socket unexpectedly closed.")

    def interrupt(self):
        """
        Initiate the interrupt.

        An interrupt channel is a loopback connection,
        that wakes up the reactor from a call to `select`, so
        that it can handle events not comming from sockets.
        Basically it is a workaraoud the limitations of Python's `select`
        (i.e. lack of signals and portablility issues with passing files to select).

        """
        if self._interrupter.is_alive():
            self._interrupter.get_socket(0).send('\0')
        else:
            log.debug('Interrupter not alive')

    def close(self):
        """
        Closes and unregisters the handler.

        """
        log.debug('Closing interrupt channel.')
        self._interrupter.close()
        super(_InterruptHandler, self).close()


class FileInputHandler(RawHandler):
    """
    A handler for file input. Has separate threads
    for handling lines and reading from file.

    Override `handle_lines` to provide custom behaviour.

    """

    #: A special object to pass to the queue to indicate shutting down.
    _MESSAGE_END = object()

    def __init__(self, owner, file_, threadname=None):
        """
        Initializes the object.

        :param owner: The owning Multiplexer instance.
        :param file_: An open file descriptior.
        :param threadname: A name for the thread doing the reading.

        """
        super(FileInputHandler, self).__init__(None, owner)
        self._file = file_
        self._thread = self.owner.get_worker(self.run_thread, 'FileIn-' + threadname if threadname else None)
        self._worker = self.owner.get_worker(self.run_worker, 'Handler-' + threadname if threadname else None)
        self._queue = Queue.Queue()
        if self._file is not None:
            self.set_done()

    def run_thread(self):
        """
        Waits for new lines to appear in the file, and enqueue them for handling.
        Runs in a separate thread.

        """
        while self._file is not None:
            s = self._file.readline().strip()
            self._queue.put(s)
            if self.check_quit(s):
                break
        self._queue.put(FileInputHandler._MESSAGE_END)
        #self._file.close() - nie stdin!

    def run_worker(self):
        """
        Waits for new lines to appear in the queue,
        retrieve them, and handle them.
        Runs in the message-handling thread.

        """
        while True:
            line = self._queue.get()
            if line is FileInputHandler._MESSAGE_END:
                break
            self.handle_line(line)

    def handle_line(self, line):
        """
        Override this method to provide code, that handles an input line.

        """
        pass

    def check_quit(self, s):
        """
        Check if a text line indicates, that the handler should shut down.

        :param s: The text line.

        :returns: True, if the handler should shut down. False otherwise.

        """
        return False

    def start(self):
        """
        Starts the handler and its private threads.

        """
        self._worker.start()
        self._thread.start()
        super(FileInputHandler, self).start()

    def close(self):
        """
        Closes and unregisters the handler.

        """
        self._queue.put(FileInputHandler._MESSAGE_END)
        super(FileInputHandler, self).close()


class FileOutputHandler(RawHandler):
    """
    A handler for file output.

    Use `writelines` to write lines to file.

    """

    #: A special object to pass to the queue to indicate shutting down.
    _MESSAGE_END = object()

    def __init__(self, owner, file_, threadname=None):
        """
        Initializes the object.

        :param owner: The owning Multiplexer instance.
        :param file_: An open file descriptior.
        :param threadname: A name for the thread doing the writing.

        """
        super(FileOutputHandler, self).__init__(None, owner)
        self._file = file_
        self._queue = Queue.Queue()
        self._thread = self.owner.get_worker(self.run_thread, 'FileOut-' + threadname if threadname else None)
        self.set_done()
        self._thread.set_done()

    def writelines(self, lines):
        """
        Enqueue lines to write to the file.

        :param lines: A list of strings to be written to the file. Line terminators are added automatically.

        """
        self._queue.put(lines)

    def run_thread(self):
        """
        Waits for new lines to appear in the queue,
        retrieve them, and write to file.
        Runs in a separate thread.

        """
        while True:
            lines = self._queue.get()
            if lines is FileOutputHandler._MESSAGE_END:
                break
            for line in lines:
                self._file.write(str(line) + '\n')
        #self._file.close() - nie stdout!

    def start(self):
        """
        Starts the handler and its private thread.

        """
        self._thread.start()

    def close(self):
        """
        Closes and unregisters the handler.

        """
        self._queue.put(FileOutputHandler._MESSAGE_END)
        super(FileOutputHandler, self).close()
