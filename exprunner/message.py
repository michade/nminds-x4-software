"""
.. Copyright (c) 2014 Institute of Psychology of the Polish Academy of Sciences.
   All rights reserved. For full license text (in Polish) please see LICENSE.polish
  
   Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)

   This file is a part of the nminds-x4 program, used to conduct psychological
   experiments on group decision-making. For more information please see README file.

Application-layer message classes.

"""


class Message(object):
    """
    Base class for all messages used by the program.

    """

    def __init__(self):
        """
        Initializes the object with default values.

        """
        self.sender = "UNKNOWN"
        self.timesent = -1.0

    def _repr_fields(self):
        """
        Provideas a human-readable representation of message's fields.
        To be overriden in subclsses.

        """
        return ''

    def __repr__(self):
        """
        Provides a human-readable representation of the message's contetns.

        """
        return '<%s: sender=%s %s>' % (str(self.__class__), self.sender, self._repr_fields())


class Introduction(Message):
    """
    A introductory message, specifying only the client type.

    """
    def __init__(self, client_type):
        """
        Initializes the object.
       
        :param client_type: A client.ClientType value representing the type of the sender.

        """
        self.client_type = client_type


class Rpc(Message):
    """
    A message containig a remote call.

    """

    def __init__(self, call_):
        """
        Initializes the object.

        :param call_: A rpc.Call object.

        """
        Message.__init__(self)
        self.call = call_

    def _repr_fields(self):
        """
        Provideas a human-readable representation of message's fields.

        """
        return 'call=' + repr(self.call)


class RpcResult(Message):
    """
    A message containig the result of a remote call.

    """

    def __init__(self, result):
        """
        Initializes the object.

        :param result: A rpc.Result object.

        """
        Message.__init__(self)
        self.result = result

    def _repr_fields(self):
        """
        Provides a human-readable representation of message's fields.

        """
        return 'res=' + repr(self.result)
