"""
.. Copyright (c) 2014 Institute of Psychology of the Polish Academy of Sciences.
   All rights reserved. For full license text (in Polish) please see LICENSE.polish
  
   Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)

   This file is a part of the nminds-x4 program, used to conduct psychological
   experiments on group decision-making. For more information please see README file.

Utilities for defining client and server commands - containing info,
keys etc. Based on the command pattern. Module also contains handlers for
the multiplexer.Multiplexer, allowing easy handling of command input and execution.

"""

import logging
import types
import threading
import exprunner.multiplexer as multiplexer
import exprunner.rpc as rpc

#: logger for this module's classes
log = logging.getLogger(__name__)


class CommandsHandler(multiplexer.FileInputHandler):
    """
    Handler (used by the multiplexer) for command input.

    """

    def __init__(self, owner, file_, name):
        """
        Initializes the object.

        :param owner: Owning multiplexer.
        :param file_: File object for line input.
        :param name: Name assigned to this handler instance.

        """
        multiplexer.FileInputHandler.__init__(self, owner, file_, name)
        self._commands = self.owner.root.commands
        self._quit_commands = self._commands['quit'].aliases

    def handle_line(self, line):
        """
        Handles arriving text line - interpreted as a command.

        :param line: String containing the command.

        """
        quit_ = self.check_quit(line)
        ps = line.split(None, 1)
        if len(ps) == 0:
            return
        name = ps[0]
        if name in self._commands:
            cmd = self._commands[name]
            try:
                args = cmd.parse(ps[1]) if len(ps) > 1 else []
                if cmd.executed_by == self.owner.name:
                    call_res = self.owner.root.call(rpc.Call('commands.execute.' + cmd.target, args))
                else:
                    call_res = self.owner.connection._rpc.call('commands.execute.' + cmd.target, *args)
                    self.owner.interrupt()
                if call_res.is_exception:
                    log.error(rpc.format_traceback(call_res.result.as_exc_info()))
                else:
                    self.on_command_completed(call_res.result)
            except Exception:
                log.error(rpc.format_traceback())
        else:
            self.owner.output.writelines(['Unknown command: ' + name])
        if quit_:
            self.set_done()

    def on_command_completed(self, res):
        """
        Executed when a command execution has finished.
        Prints the result of the commands to output, as a series of lines.

        :param res: Result of the Command (string).

        """
        if res is not None:
            if not isinstance(res, types.ListType):  # a poor way to do this, but it works...
                res = [res]
            self.owner.output.writelines(res)

    def check_quit(self, command):
        """
        Checks if `command` is a quit command, for special handling (a technicality).

        """
        return command in self._quit_commands


class Param(object):
    """
    Holds info about a command parameter (argument), and parses it from a string.

    """
    def __init__(self, name, count=1):
        """
        Initializes the object.

        :param name: Name of the parameter.
        :param count: Arity (number of r parts).

        """
        self.name = name
        self.count = count

    def parse(self, rest):
        """
        Parses the parameter from the commmands string.

        :param rest: Part of the command line starting with the parameter. Also contains consecutive parameters!
        :returns: Tuple (param, line), where:
                  * param - Parsed parameter value.
                  * line - remaining part of the command line, i.e. containing any parameters after this one.

        """
        if self.count == 0:
            sl = rest.split()
            sl.append('')
        else:
            sl = rest.split(None, self.count)
            if len(sl) < self.count:
                raise Exception(self.name + '- not enough parameters.')
            if len(sl) == self.count:
                sl.append('')
        pl = [self.parse_one(s) for s in sl[:-1]]
        return (pl, sl[-1])

    def parse_one(self, s):
        """
        Subclasses override this method to define parsing behavior of
        a single part of the parameter.
        This method is abstract.

        :param s: Parse a single part of a (possibly multi-part) parameter.
        :returns: The parsed parameter value (sibgle part).

        """
        raise NotImplementedError('Abstract method call.')


class Int(Param):
    """
    An integer parameter (can have more than one part).

    """
    def parse_one(self, s):
        """
        Parses `s` as an integer and returns the result or raises a
        standard python parsing error.

        """
        return int(s)


class Float(Param):
    """
    A float parameter (can have more than one part).

    """
    def parse_one(self, s):
        """
        Parses `s` as a float and returns the result or raises a
        standard python parsing error.

        """
        return float(s)


class Bool(Param):
    """
    A boolean parameter (can have more than one part).

    """
    def parse_one(self, s):
        """
        Parses `s` as a boolean value and returns the result or throws a
        standard python parsing error.

        """
        return bool(s)


class String(Param):
    """
    A string parameter (can have more than one part).

    """
    def parse_one(self, s):
        """
        Returns `s` unchanged.

        """
        return s


class Command(object):
    """
    Represetns a command that a server or client exposes.

    """

    def __init__(self, name, executed_by, desc='', params=[], aliases=[], target=None):
        """
        Initializes the object.

        :param name: Name of the command.
        :param executed_by: Name of the entity (e.g. 'server') which is
        supposed to physically execute the commmand.
        :param desc: String describing the command.
        :param params: List of Param objects, representing the parameters of the command.
        :param aliases: List of alternate names of this command.
        :param target: Python-friendly name for this command (to avoid name clashes).

        """
        self.name = name
        self.desc = desc
        self.params = list(params)
        self.aliases = list(aliases)
        self.executed_by = executed_by
        if name not in aliases:
            self.aliases.append(name)
        self.target = name if target is None else target

    def parse(self, param_str):
        """
        Parses the string containing command parameters into a list of values,
        according to the Params specified for this Command.

        :param param_str: String containing command parameters separated with whitespaces.
                          Adjacent whitespaces are treated as a single separator.

        :returns: A list of parsed parameter values, in the order ini which they appear ini the string.

        """
        param_values = []
        s = param_str
        for param in self.params:
            val, s = param.parse(s)
            param_values.extend(val)
        return param_values


class Commands(object):
    """
    A synchronized collection of commands.

    """

    def __init__(self, target):
        """
        Initializes the object.

        :param target: Defaut target for the commands contained in this set.

        """
        self._items_lock = threading.RLock()
        self._commands_dict = {}
        self._commands_list = []
        self._target = target

    def __len__(self):
        """
        Gets the number of commands in this collection.

        """
        with self._items_lock:
            return len(self._commands_list)

    def __getitem__(self, key):
        """
        Gets a command with a name or alias equal to `key`.

        """
        with self._items_lock:
            return self._commands_dict[key]

    def add(self, cmd):
        """
        Adds a command to the collection.

        :param cmd: Command object ot add.

        """
        with self._items_lock:
            self._commands_list.append(cmd)
            for als in cmd.aliases:
                self._commands_dict[als] = cmd

    def remove(self, name):
        """
        Remove a command from the collection.

        :param name: Name (not alias!) of the command to remove.

        """
        with self._items_lock:
            cmd = self._commands_dict.pop(name)
            self._commands_list.remove(cmd)
            for al in cmd.aliases:
                self._commands_dict.pop(al, None)

    def __iter__(self):
        """
        Returns a read-only list iterator over the collection of commands.

        """
        with self._items_lock:
            cmd_list_cpy = list(self._commands_list)
        return cmd_list_cpy.__iter__()

    def __contains__(self, key):
        """
        Checks, if the collection contains a command with name or alias equal to `key`.

        """
        with self._items_lock:
            return key in self._commands_dict

    def format_help(self):
        """
        Returns a formatted help message for the commands in this collection.

        """
        lines = []
        for cmd in self:
            s = cmd.name + ' '
            for p in cmd.params:
                s += p.name + ' '
            s += '- ' + cmd.desc
            if len(cmd.aliases) > 1:
                s += ' (alias:'
                for a in cmd.aliases:
                    if a != cmd.name:
                        s += ' \'%s\'' % a
                s += ')'
            lines.append(s)
        return lines

    def fetch(self):
        """
        Obtains a copy of the commands in this collection, as a list.
        This method is thread-safe.

        """
        with self._items_lock:
            return list(self._commands_list)

    @property
    def execute(self):
        """
        Gets the target of the command.

        """
        return self._target
