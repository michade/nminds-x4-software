"""
.. Copyright (c) 2014 Institute of Psychology of the Polish Academy of Sciences.
   All rights reserved. For full license text (in Polish) please see LICENSE.polish
  
   Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)

   This file is a part of the nminds-x4 program, used to conduct psychological
   experiments on group decision-making. For more information please see README file.

Test for `Trials` module.

"""

import unittest
from trials import Trials
from collections import OrderedDict


class Test_Trials(unittest.TestCase):
    
    def test_create_empty(self):
        t = Trials()
        self.assertEqual(len(t), 0)
        self.assertEqual(t.nrow, 0)
        self.assertEqual(len(t.colnames()), 0)
        rows = t.rows()
        self.assertEqual(len(rows), 0)
        self.assertEqual(t.format(sep=',', rowsep=';'), '')
        self.assertListEqual(t.data().items(), [])

    def test_add_one_col(self):
        t = Trials()
        t['foo'] = 123
        self.assertEqual(len(t), 0)
        self.assertEqual(t.nrow, 1)
        self.assertTrue('foo' in t)
        self.assertEqual(t['foo'], 123)
        self.assertListEqual(t.colnames(), ['foo'])
        rows = t.rows()
        self.assertEqual(len(rows), 1)
        self.assertListEqual(rows[0], [123])
        self.assertEqual(t.format(sep=',', rowsep=';'), 'foo;123')
        self.assertListEqual(t.data().items(), [('foo', 123)])

    def test_add_two_cols(self):
        t = Trials()
        t['foo'] = 123
        self.assertEqual(len(t), 0)
        self.assertEqual(t.nrow, 1)
        self.assertTrue('foo' in t)
        self.assertEqual(t['foo'], 123)
        self.assertListEqual(t.colnames(), ['foo'])
        t['bar'] = 567
        self.assertEqual(len(t), 0)
        self.assertEqual(t.nrow, 1)
        self.assertTrue('foo' in t)
        self.assertEqual(t['foo'], 123)
        self.assertTrue('bar' in t)
        self.assertEqual(t['bar'], 567)
        self.assertListEqual(t.colnames(), ['foo', 'bar'])
        rows = t.rows()
        self.assertEqual(len(rows), 1)
        self.assertListEqual(rows[0], [123, 567])
        self.assertEqual(t.format(sep=',', rowsep=';'), 'foo,bar;123,567')
        self.assertListEqual(t.data().items(), [('foo', 123), ('bar', 567)])

    def test_children(self):
        t = Trials()
        t['p1'] = 1
        t['p2'] = 2
        t1 = t.add_child(OrderedDict([('c1', 3), ('c2', 4)]))
        t2 = t.add_child(OrderedDict([('c1', 5), ('c2', 6)]))
        self.assertEqual(len(t), 2)
        self.assertEqual(t.nrow, 2)
        self.assertTrue('p1' in t)
        self.assertEqual(t['p1'], 1)
        self.assertTrue('p2' in t)
        self.assertEqual(t['p2'], 2)
        self.assertFalse('c1' in t)
        self.assertFalse('c2' in t)

        self.assertEqual(len(t1), 0)
        self.assertEqual(t1.nrow, 1)
        self.assertTrue('p1' in t1)
        self.assertEqual(t1['p1'], 1)
        self.assertTrue('p2' in t1)
        self.assertEqual(t1['p2'], 2)
        self.assertTrue('c1' in t1)
        self.assertEqual(t1['c1'], 3)
        self.assertTrue('c2' in t1)
        self.assertEqual(t1['c2'], 4)

        self.assertEqual(len(t2), 0)
        self.assertEqual(t2.nrow, 1)
        self.assertTrue('p1' in t2)
        self.assertEqual(t2['p1'], 1)
        self.assertTrue('p2' in t2)
        self.assertEqual(t2['p2'], 2)
        self.assertTrue('c1' in t2)
        self.assertEqual(t2['c1'], 5)
        self.assertTrue('c2' in t2)
        self.assertEqual(t2['c2'], 6)

        self.assertListEqual(t.colnames(), ['p1', 'p2', 'c1', 'c2'])
        self.assertListEqual(t1.colnames(), ['p1', 'p2', 'c1', 'c2'])
        self.assertListEqual(t2.colnames(), ['p1', 'p2', 'c1', 'c2'])
        self.assertListEqual(t.colnames(False), ['p1', 'p2', 'c1', 'c2'])
        self.assertListEqual(t1.colnames(False), ['c1', 'c2'])
        self.assertListEqual(t2.colnames(False), ['c1', 'c2'])

        rows = t.rows()
        self.assertEqual(len(rows), 2)
        self.assertListEqual(rows[0], [1, 2, 3, 4])
        self.assertListEqual(rows[1], [1, 2, 5, 6])
        self.assertEqual(t.format(sep=',', rowsep=';'), 'p1,p2,c1,c2;1,2,3,4;1,2,5,6')

        self.assertListEqual(t.data().items(), [('p1', 1), ('p2', 2)])
        self.assertListEqual(t1.data().items(), [('p1', 1), ('p2', 2), ('c1', 3), ('c2', 4)])
        self.assertListEqual(t2.data().items(), [('p1', 1), ('p2', 2), ('c1', 5), ('c2', 6)])

    def test_children_missing(self):
        t = Trials()
        t['p1'] = 1
        t['p2'] = 2
        t1 = t.add_child(OrderedDict([('c1', 3), ('c2', 4)]))
        t2 = t.add_child(OrderedDict([('c2', 6)]))
        self.assertEqual(len(t), 2)
        self.assertEqual(t.nrow, 2)
        self.assertTrue('p1' in t)
        self.assertEqual(t['p1'], 1)
        self.assertTrue('p2' in t)
        self.assertEqual(t['p2'], 2)
        self.assertFalse('c1' in t)
        self.assertFalse('c2' in t)

        self.assertEqual(len(t1), 0)
        self.assertEqual(t1.nrow, 1)
        self.assertTrue('p1' in t1)
        self.assertEqual(t['p1'], 1)
        self.assertTrue('p2' in t1)
        self.assertEqual(t1['p2'], 2)
        self.assertTrue('c1' in t1)
        self.assertEqual(t1['c1'], 3)
        self.assertTrue('c2' in t1)
        self.assertEqual(t1['c2'], 4)

        self.assertEqual(len(t2), 0)
        self.assertEqual(t2.nrow, 1)
        self.assertTrue('p1' in t2)
        self.assertEqual(t2['p1'], 1)
        self.assertTrue('p2' in t2)
        self.assertEqual(t2['p2'], 2)
        self.assertFalse('c1' in t2)
        self.assertTrue('c2' in t2)
        self.assertEqual(t2['c2'], 6)

        self.assertListEqual(t.colnames(), ['p1', 'p2', 'c1', 'c2'])
        self.assertListEqual(t1.colnames(), ['p1', 'p2', 'c1', 'c2'])
        self.assertListEqual(t2.colnames(), ['p1', 'p2', 'c1', 'c2'])
        self.assertListEqual(t.colnames(False), ['p1', 'p2', 'c1', 'c2'])
        self.assertListEqual(t1.colnames(False), ['c1', 'c2'])
        self.assertListEqual(t2.colnames(False), ['c2'])

        rows = t.rows()
        self.assertEqual(len(rows), 2)
        self.assertListEqual(rows[0], [1, 2, 3, 4])
        self.assertListEqual(rows[1], [1, 2, None, 6])
        self.assertEqual(t.format(sep=',', rowsep=';'), 'p1,p2,c1,c2;1,2,3,4;1,2,NA,6')

        self.assertListEqual(t.data().items(), [('p1', 1), ('p2', 2)])
        self.assertListEqual(t1.data().items(), [('p1', 1), ('p2', 2), ('c1', 3), ('c2', 4)])
        self.assertListEqual(t2.data().items(), [('p1', 1), ('p2', 2), ('c2', 6)])

    def test_children_lvl2(self):
        t = Trials()
        t['p1'] = 1
        t['p2'] = 2
        t1 = t.add_child(OrderedDict([('c1', 3), ('c2', 4)]))
        t2 = t.add_child(OrderedDict([('c1', 5), ('c2', 6)]))
        t11 = t1.add_child(OrderedDict([('d1', 7), ('d2', 8)]))
        t12 = t1.add_child(OrderedDict([('d1', 9), ('d2', 0)]))

        self.assertEqual(len(t), 2)
        self.assertEqual(t.nrow, 3)
        self.assertTrue('p1' in t)
        self.assertEqual(t['p1'], 1)
        self.assertTrue('p2' in t)
        self.assertEqual(t['p2'], 2)
        self.assertFalse('c1' in t)
        self.assertFalse('c2' in t)
        self.assertFalse('d1' in t)
        self.assertFalse('d2' in t)

        self.assertEqual(len(t1), 2)
        self.assertEqual(t1.nrow, 2)
        self.assertTrue('p1' in t1)
        self.assertEqual(t1['p1'], 1)
        self.assertTrue('p2' in t1)
        self.assertEqual(t1['p2'], 2)
        self.assertTrue('c1' in t1)
        self.assertEqual(t1['c1'], 3)
        self.assertTrue('c2' in t1)
        self.assertEqual(t1['c2'], 4)
        self.assertFalse('d1' in t)
        self.assertFalse('d2' in t)

        self.assertEqual(len(t11), 0)
        self.assertEqual(t11.nrow, 1)
        self.assertTrue('p1' in t11)
        self.assertEqual(t11['p1'], 1)
        self.assertTrue('p2' in t11)
        self.assertEqual(t11['p2'], 2)
        self.assertTrue('c1' in t11)
        self.assertEqual(t11['c1'], 3)
        self.assertTrue('c2' in t11)
        self.assertEqual(t11['c2'], 4)
        self.assertTrue('d1' in t11)
        self.assertEqual(t11['d1'], 7)
        self.assertTrue('d2' in t11)
        self.assertEqual(t11['d2'], 8)

        self.assertEqual(len(t12), 0)
        self.assertEqual(t12.nrow, 1)
        self.assertTrue('p1' in t12)
        self.assertEqual(t12['p1'], 1)
        self.assertTrue('p2' in t12)
        self.assertEqual(t12['p2'], 2)
        self.assertTrue('c1' in t12)
        self.assertEqual(t12['c1'], 3)
        self.assertTrue('c2' in t12)
        self.assertEqual(t12['c2'], 4)
        self.assertTrue('d1' in t12)
        self.assertEqual(t12['d1'], 9)
        self.assertTrue('d2' in t12)
        self.assertEqual(t12['d2'], 0)

        self.assertEqual(len(t2), 0)
        self.assertEqual(t2.nrow, 1)
        self.assertTrue('p1' in t2)
        self.assertEqual(t2['p1'], 1)
        self.assertTrue('p2' in t2)
        self.assertEqual(t2['p2'], 2)
        self.assertTrue('c1' in t2)
        self.assertEqual(t2['c1'], 5)
        self.assertTrue('c2' in t2)
        self.assertEqual(t2['c2'], 6)
        self.assertFalse('d1' in t)
        self.assertFalse('d2' in t)

        self.assertListEqual(t.colnames(), ['p1', 'p2', 'c1', 'c2', 'd1', 'd2'])
        self.assertListEqual(t1.colnames(), ['p1', 'p2', 'c1', 'c2', 'd1', 'd2'])
        self.assertListEqual(t11.colnames(), ['p1', 'p2', 'c1', 'c2', 'd1', 'd2'])
        self.assertListEqual(t12.colnames(), ['p1', 'p2', 'c1', 'c2', 'd1', 'd2'])
        self.assertListEqual(t2.colnames(), ['p1', 'p2', 'c1', 'c2', 'd1', 'd2'])
        self.assertListEqual(t.colnames(False), ['p1', 'p2', 'c1', 'c2', 'd1', 'd2'])
        self.assertListEqual(t1.colnames(False), ['c1', 'c2', 'd1', 'd2'])
        self.assertListEqual(t11.colnames(False), ['d1', 'd2'])
        self.assertListEqual(t12.colnames(False), ['d1', 'd2'])
        self.assertListEqual(t2.colnames(False), ['c1', 'c2'])

        rows = t.rows()
        self.assertEqual(len(rows), 3)
        self.assertListEqual(rows[0], [1, 2, 3, 4, 7, 8])
        self.assertListEqual(rows[1], [1, 2, 3, 4, 9, 0])
        self.assertListEqual(rows[2], [1, 2, 5, 6, None, None])
        self.assertEqual(t.format(sep=',', rowsep=';'), 'p1,p2,c1,c2,d1,d2;1,2,3,4,7,8;1,2,3,4,9,0;1,2,5,6,NA,NA')
        self.assertListEqual(t.data().items(), [('p1', 1), ('p2', 2)])
        self.assertListEqual(t1.data().items(), [('p1', 1), ('p2', 2), ('c1', 3), ('c2', 4)])
        self.assertListEqual(t11.data().items(), [('p1', 1), ('p2', 2), ('c1', 3), ('c2', 4), ('d1', 7), ('d2', 8)])
        self.assertListEqual(t12.data().items(), [('p1', 1), ('p2', 2), ('c1', 3), ('c2', 4), ('d1', 9), ('d2', 0)])
        self.assertListEqual(t2.data().items(), [('p1', 1), ('p2', 2), ('c1', 5), ('c2', 6)])

    def test_indexes(self):
        t = Trials(indexer=None, index_name='idx')
        t['p1'] = 1
        t['p2'] = 2
        t1 = t.add_child(OrderedDict([('c1', 3), ('c2', 4)]), index='i1')
        t2 = t.add_child(OrderedDict([('c1', 5), ('c2', 6)]), index='i2')
        self.assertEqual(len(t), 2)
        self.assertEqual(t.nrow, 2)
        self.assertTrue('p1' in t)
        self.assertEqual(t['p1'], 1)
        self.assertTrue('p2' in t)
        self.assertEqual(t['p2'], 2)
        self.assertFalse('c1' in t)
        self.assertFalse('c2' in t)
        self.assertFalse('idx' in t)

        self.assertEqual(len(t1), 0)
        self.assertEqual(t1.nrow, 1)
        self.assertTrue('p1' in t1)
        self.assertEqual(t1['p1'], 1)
        self.assertTrue('p2' in t1)
        self.assertEqual(t1['p2'], 2)
        self.assertTrue('c1' in t1)
        self.assertEqual(t1['c1'], 3)
        self.assertTrue('c2' in t1)
        self.assertEqual(t1['c2'], 4)
        self.assertTrue('idx' in t1)
        self.assertEqual(t1['idx'], 'i1')

        self.assertEqual(len(t2), 0)
        self.assertEqual(t2.nrow, 1)
        self.assertTrue('p1' in t2)
        self.assertEqual(t2['p1'], 1)
        self.assertTrue('p2' in t2)
        self.assertEqual(t2['p2'], 2)
        self.assertTrue('c1' in t2)
        self.assertEqual(t2['c1'], 5)
        self.assertTrue('c2' in t2)
        self.assertEqual(t2['c2'], 6)
        self.assertTrue('idx' in t2)
        self.assertEqual(t2['idx'], 'i2')

        self.assertListEqual(t.colnames(), ['p1', 'p2', 'idx', 'c1', 'c2'])
        self.assertListEqual(t1.colnames(), ['p1', 'p2', 'idx', 'c1', 'c2'])
        self.assertListEqual(t2.colnames(), ['p1', 'p2', 'idx', 'c1', 'c2'])
        self.assertListEqual(t.colnames(False), ['p1', 'p2', 'idx', 'c1', 'c2'])
        self.assertListEqual(t1.colnames(False), ['idx', 'c1', 'c2'])
        self.assertListEqual(t2.colnames(False), ['idx', 'c1', 'c2'])

        rows = t.rows()
        self.assertEqual(len(rows), 2)
        self.assertListEqual(rows[0], [1, 2, 'i1', 3, 4])
        self.assertListEqual(rows[1], [1, 2, 'i2', 5, 6])
        self.assertEqual(t.format(sep=',', rowsep=';'), 'p1,p2,idx,c1,c2;1,2,i1,3,4;1,2,i2,5,6')

        self.assertListEqual(t.data().items(), [('p1', 1), ('p2', 2)])
        self.assertListEqual(t1.data().items(), [('p1', 1), ('p2', 2), ('idx', 'i1'), ('c1', 3), ('c2', 4)])
        self.assertListEqual(t2.data().items(), [('p1', 1), ('p2', 2), ('idx', 'i2'), ('c1', 5), ('c2', 6)])


if __name__ == '__main__':
    unittest.main()
