"""
.. Copyright (c) 2014 Institute of Psychology of the Polish Academy of Sciences.
   All rights reserved. For full license text (in Polish) please see LICENSE.polish
  
   Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)

   This file is a part of the nminds-x4 program, used to conduct psychological
   experiments on group decision-making. For more information please see README file.

This module contains implementation of the rpc (remote procedure call) system
used by the PyInteract framework.

"""

import threading
import traceback
import sys
import weakref

#: Maximum length of human-readable message representations.
_MAX_REPR_LENGTH = 32

#: Global counter for message ids, guarded by _last_id_lock. A solution far from perfect, but sufficient.
_LAST_ID = 0

#: Lock guarding the _LAST_ID variable.
_last_id_lock = threading.Lock()


def _make_call_id():
    """
    Returns the next free ID for a new Call instance.

    Uses a global counter for IDs - a solution far from perfect, but sufficient
    for current purposes of the framework.

    """
    global _LAST_ID
    global _last_id_lock
    with _last_id_lock:
        _LAST_ID += 1
        return _LAST_ID


class Directory(object):
    """
    An rpc directory - a dictionaty-liek object, that facilitates
    remote querries and additions. Allows blocking until an entry appears.

    """

    def __init__(self, parent):
        """
        Initializes the object.

        :param parent: A Directory containing this instance. `None` indicates rpc root.

        """
        self._parent = weakref.ref(parent) if parent else None
        self._items = {}
        self._items_lock = threading.Lock()

    @property
    def parent(self):
        """
        Returns the parent Directory of this instance. `None` indicates rpc root.

        """
        return self._parent()

    def __getattr__(self, name):
        """
        Returns an object stored under `name`.

        """
        with self._items_lock:
            return self._items[name]

    def resolve(self, path):
        """
        Resolves an rpc path and returns the object at its end.

        :param path: An rpc path (dot-separated names, e.g. `foo.bar.baz`)

        :returns: The object referreed to which the path refers.

        """
        target = self
        prevs = []
        while len(path) > 1:
            ps = path.split('.', 1)
            prevs.append(ps[0])
            try:
                target = getattr(target, ps[0])
            except (AttributeError, KeyError, TypeError):
                raise PathException('.'.join(prevs))
            if len(ps) == 1:
                return target
            if isinstance(target, Directory):
                try:
                    return target.resolve(ps[1])
                except PathException as ex:
                    ex.path = '.'.join(prevs) + '.' + ex.path
                    raise ex
            path = ps[1]

    def call(self, call):
        """
        Executes a remote call.

        :param call: A Call object representing the remote call.

        :returns: A Result object represeting the result of the call.

        """
        try:
            target = self.resolve(call.path)
            rv = target(*call.args, **call.kwargs)
            return Result(call.call_id, rv, False)
        except Exception:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            tblist = traceback.extract_tb(exc_traceback)
            rexc = RemoteException(exc_type, exc_value, tblist)
            return Result(call.call_id, rexc, True)

    def put(self, name, item):
        """
        Adds the item to the contents of the Directory.

        :param name: The name of under which the object will appear.
        :param item: The instance to be added.

        """
        with self._items_lock:
            prev = self._items.get(name, None)
            self._items[name] = item
        if isinstance(prev, Trigger):
            prev.fire()

    def get(self, name, blocking=True):
        """
        Returns the object stored under `name`. If one exists, it is returned immidiately. Otherwise:
        * If `blocking` is True, the call blocks, until some object is inserted under `name` (or the call is aborted).
        * If `blocking` is False, a RemoteObjectNotFound exception is raised.

        :param name: The name of the object to retreive - i.e. key under which it is stored in the Directory.
        :param blocking: Block the calling thread if the object is not present?

        :returns: The instance stored under `name`.

        """
        with self._items_lock:
            if name in self._items:
                res = self._items[name]
            else:
                if not blocking:
                    raise RemoteObjectNotFound(name)
                res = Trigger()
                self._items[name] = res
        if isinstance(res, Trigger):
            res.wait()
        return self._items.get(name)

    def exists(self, name):
        """
        Checks if there is an object stored under `name` in the Directory.

        Note: due to a race condition, the following may still fail:
            if dir.exists('foo'):
                foo = dir.get('foo')

        :param name: The name of the object to check for - i.e. key under which it is stored in the Directory.

        :returns: True if the Directory contains an object under `name`, False otherwise.

        """
        with self._items_lock:
            return name in self._items

    def delete(self, name):
        """
        Removes an object stored under `name` from the Directory.
        If it does not exist, the call silently fails.

        :param name: The name of the object to remove.

        """
        with self._items_lock:
            res = self._items.pop(name, None)
        if isinstance(res, Trigger):
            res.fire()

    def ls(self):
        """
        Returns a list of names of all object stored in the Directory.

        """
        with self._items_lock:
            return self._items.keys()

    def create(self, name, klass, *args, **kwargs):
        """
        Create a new instance of a given class and put it in the Directory under a given name.

        :param name: Name under which to store the new object.
        :param klass: Class of the new object (actual class object, not a name).
        :param args: A list of positional arguments to be passed to the new object's __init__ method.
        :param kwargs: A dict of arguments to be passed to the new object's __init__ method.

        """
        self.put(name, klass(*args, **kwargs))

    def mkdir(self, name):
        """
        Create a subdirectory - a new Directory object.

        :param name: The name under which the new Directory will be placed.

        """
        self.create(name, Directory, self)

    def acquire(self, blocking):
        """
        Acquire the lock guarding the Directory contents. If the lock has already
        been acqired, `blocking` specifies, whether the call should return immidiately
        or wait for the lock's holder to release it.

        :param blocking: Wait for the lock to become available?

        :returns: False if `blocking` is False and the lock has already been acquired.
                  True otherwise.

        """
        return self._items_lock.acquire(blocking)

    def release(self):
        """
        Release the lock guarding the Directory contents.

        """
        self._items_lock.release()

    def itertems(self):
        """
        Returns an iterator over the (name, object) pairs contained in the Directory.

        """
        with self._items_lock:
            items_copy = dict(self._items)
        return items_copy.iteritems()

    def iterkeys(self):
        """
        Returns an iterator over the names of the objects contained in the Directory.

        """
        with self._items_lock:
            items_copy = dict(self._items)
        return items_copy.iterkeys()

    def itervalues(self):
        """
        Returns an iterator over the objects contained in the Directory.

        """
        with self._items_lock:
            items_copy = dict(self._items)
        return items_copy.itervalues()


class Trigger(object):
    """
    A helper object, on which threads can wait, until it is fired,
    much like a threading.Event.
    If a Trigger instance is placed in a Directory, it's fire method will be called
    if an object is inserted under the same name.

    """
    def __init__(self):
        """
        Initializes the object.

        """
        self._event = threading.Event()

    def wait(self):
        """
        Blocks current thread, until `fire` is called.
        If a Trigger instance is placed in a Directory, it's fire method will be called
        if an object is inserted under the same name.

        """
        self._event.wait()

    def fire(self):
        """
        Wake up all threads blocked in this object's `wait` call.

        """
        self._event.set()


class Dispatcher(object):
    """
    Manages and dispatches remote calls made and notifies the CallFinished objects
    about finished calls.

    """

    def __init__(self, send_fun):
        """
        Initializes the Dispatcher object.

        :param send_fun: A function sending the calls thourgh transport layer.
        Takes one argument - the Call object to be sent.

        """
        self._calls = {}
        self._send_fun = send_fun
        self._calls_lock = threading.RLock()

    def call(self, path, *args, **kwargs):
        """
        Initiates a remote call and blocks until it is completed.
        If the call has raised an exception on the remote process,
        this method will raise a RemoteException, containing the original
        exception.

        :param path: A path to a remote callable object, which will be called.
        :param args: Positional arguments to the call.
        :param kwargs: Keyword arguments to the call.

        :returns: The result (return value) of the remote call.

        """
        cf = self.init_call(path, *args, **kwargs)
        return cf.result()

    def init_call(self, path, *args, **kwargs):
        """
        Initiates a remote call. The caller can querry the
        progress and result trough the returned CallFinished object.

        :param path: A path to a remote callable object, which will be called.
        :param args: Positional arguments to the call.
        :param kwargs: Keyword arguments to the call.

        :returns: A CallFinished object, that can be used to check for completion
                  of the call and obtain its result (return value).

        """
        call = Call(path, args, kwargs)
        cf = CallFinished(call.call_id)
        with self._calls_lock:
            self._calls[call.call_id] = cf
        try:
            self._send_fun(call)
        except:
            with self._calls_lock:
                self._calls.pop(call.call_id)
            raise
        return cf

    def result_received(self, result):
        """
        The transport layer calls this method to notify about
        an incomming call result.

        :param result: A Result object.

        """
        with self._calls_lock:
            if result.call_id not in self._calls:
                raise Exception("Call id [%d] not found.", result.call_id)
            cf = self._calls.pop(result.call_id)
        cf.finish(result)

    def finish_all(self):
        """
        Aborts all calls in progress. They caller
        will receive an AbortedException.

        Should be renamed: abort_all

        """
        with self._calls_lock:
            while len(self._calls) > 0:
                call_id, cf = self._calls.popitem()
                cf.finish(Result(call_id, AbortedException(), True))


class CallFinished(object):
    """
    An object used to querry the state of an initiated remote call,
    and obtain its return value (or any exception that has been raised on
    the callee's side), after it has finished.

    """

    def __init__(self, call_id):
        """
        Initializes the object.

        :param call_id: The identifier (an integer) of the call, used to connect this object to the call's result.

        """
        self._event = threading.Event()
        self._call_id = call_id
        self._result = None
        self._has_succeeded = False

    def result(self, timeout=None, auto_raise=True):
        """
        Blocks until the call is finished, and returns the call's return value.
        If the call has raised an exception on the remote process,
        this method will raise a RemoteException containing the original exception
        (unless auto_raise is set to False).
        A timeout can be specified.

        :param timeout: Time in seconds to wait for the call to complete.
        :param auto_raise: Re-raise remote exceptions locally?

        :returns: The return value of the call. If the call raised an exeption on the
                  remote process and auto_raise is False, the exception is returned
                  instead of beig raised locally.
                  If timeout is reached before call completes, None is returned. To distinguish
                  between this situation and the call actually returning None,
                  use the `is_finished` method.

        """
        if not self._event.wait(timeout):
            return None
        if not self._has_succeeded and auto_raise:
            raise self._result
        return self._result

    def has_succeeded(self):
        """
        True if the call is completed and has not raised an exception.
        False if the call has rasied an exception or failed otherwise (e.g. due to broken connection).
        None, if the call is still in progress.

        """
        if not self._event.is_set():
            return None
        return self._has_succeeded

    def wait(self, timeout=None):
        """
        Blocks the current thread, until the call completes, aborted,
        or (optionally) until a timeout is reached.

        :param timeout: Time in seconds to wait for the call to complete.

        :returns: True if the call is completed, False if the timeout has been reached.

        """
        return self._event.wait(timeout)

    def is_finished(self):
        """
        Returns True if the call is finished (failed or not),False otherwise.
        Does not block.

        """
        return self._event.is_set()

    def finish(self, result):
        """
        Notifies the object, that the call has finished, and sets its result.
        Any threads waiting for the result will be woken up.

        :param result: The return value of the call. If an exception is passed, it is treated,
                       as if it was raised by the call target. Note: this means that a function
                       actually returning an exception will be handled incorrectly.

        """
        self._result = result.result
        self._has_succeeded = not result.is_exception
        self._event.set()


class Call(object):
    """
    An object containing info about a remote call request.

    """

    def __init__(self, path, args=[], kwargs={}, call_id=None):
        """
        Initializes the object.

        :param path: A path to a remote callable object, which is to be called.
        :param args: Positional arguments to the call.
        :param kwargs: Keyword arguments to the call.
        :param call_id: ID of the call. If not specified,
        the next available ID will be assigned.

        """
        self.call_id = call_id if call_id is not None else _make_call_id()
        self.path = path
        self.args = list(args)
        self.kwargs = dict(kwargs)

    def __repr__(self):
        """
        Returns a human-readable string representation of the content of this object.

        """
        try:
            arglst = []
            arglst.extend([str(a) for a in self.args])
            arglst.extend(['%s=%s' % kv for kv in self.kwargs])
            argsstr = ', '.join(arglst)
            if len(argsstr) > _MAX_REPR_LENGTH:
                argsstr = argsstr[:(_MAX_REPR_LENGTH - 3)] + '...'
        except:
            argsstr = '!!!'
        return '%d->%s(%s)' % (self.call_id, self.path, argsstr)


class Result(object):
    """
    Represents a completed remote call. Contians the call's
    result (return value) or any raised exception.

    """

    def __init__(self, call_id, result, is_exception):
        """
        Initializes the object.

        :param call_id: The ID of the respective Call.
        :param result: Return value of the call, or an exception that has been raised.
        :param is_exception: Is the `result` an exception, that has been raised?

        """
        self.call_id = call_id
        self.result = result
        self.is_exception = is_exception

    def __repr__(self):
        """
        Return a human-readable string representation of the contents of this object.

        """
        try:
            resstr = str(self.result)
            if len(resstr) > _MAX_REPR_LENGTH:
                resstr = resstr[:(_MAX_REPR_LENGTH - 3)] + '...'
        except:
            resstr = '!!!'
        return '%d<-%s:%s' % (self.call_id, 'res' if not self.is_exception else 'exc', resstr)


class RemoteException(Exception):
    """
    A wrapper class for exceptions raised remotely.

    """

    def __init__(self, cause_type=None, cause_value=None, remote_traceback=None):
        """
        Initializes the object.

        :param cause_type: Type of the original exception.
        :param cause_value: Value of the original exception (the instance actually raised).
        :param remote_traceback: Tracebak from the remote process.

        """
        Exception.__init__(self)
        self.cause_type = cause_type
        self.cause_value = cause_value
        self.remote_traceback = remote_traceback

    def as_exc_info(self):
        """
        Returns contents of this object in the same form, as in the
        call to sys.exc_info().

        :returns: Tuple (cause_type, cause_value, remote_traceback), where:
                  * cause_type - Type of the original exception.
                  * cause_value - Value of the original exception (the instance actually raised).
                  * remote_traceback - Tracebak from the remote process.

        """
        return (self.cause_type, self.cause_value, self.remote_traceback)

    def __str__(self):
        """
        Returns a human-readable string representation of the content of this object.

        """
        return 'Remote:<' + Exception.__str__(self.cause_value) + '>'


class AbortedException(Exception):
    """
    Raised it the caller thread when a remote call is aborted.

    """
    def __init__(self):
        """
        Initializes the object.

        """
        super(AbortedException, self).__init__()


class PathException(Exception):
    """
    Raised if an invalid rpc path is specified.

    """

    def __init__(self, path=None):
        """
        Initializes the object.

        :param path: The invalid path.

        """
        super(PathException, self).__init__()
        self.path = path

    def __str__(self):
        """
        Returns a human-readable string representation of the content of this object.

        """
        return 'Invalid path "%s".' % self.path


def format_traceback(exc_info=None, brief=True):
    """
    Returns a formatted traceback similar to what traceback.print_tb()
    would produce, but integrates remote exception information.

    :param exc_info: Info usually provided by sys.exc_info() - see its doc for details.
    :param brief: Make a shorter version?

    :returns: A multiline string with formatted traceback.

    """
    if exc_info is None:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        exc_traceback = traceback.extract_tb(exc_traceback)
    else:
        exc_type, exc_value, exc_traceback = exc_info
    lines = ['\n--------Local---------\n']
    tr_entries = exc_traceback
    if brief:
        tr_entries = [e for e in tr_entries if not e[0].endswith('\\' + __name__ + '.py')]
    lines.extend(traceback.format_list(tr_entries))
    if isinstance(exc_value, RemoteException):
        lines.append('--------Remote--------\n')
        tr_entries = exc_value.remote_traceback
        if brief:
            tr_entries = [e for e in tr_entries if not e[0].endswith('\\' + __name__ + '.py')]
        lines.extend(traceback.format_list(tr_entries))
        exc_type = exc_value.cause_type
        exc_value = exc_value.cause_value
    lines.extend(traceback.format_exception_only(exc_type, exc_value))
    text = ''.join(lines)
    return text


class RemoteObjectNotFound(Exception):
    """
    Indicates that the target of a remote call has not been found.

    """
    def __init__(self, name=None):
        """
        Initializes the object.

        :param name: Name of the missing object.

        """
        super(RemoteObjectNotFound, self).__init__('Remote object not found ' + str(name))
        self.name = name
