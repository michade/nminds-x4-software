"""
.. Copyright (c) 2014 Institute of Psychology of the Polish Academy of Sciences.
   All rights reserved. For full license text (in Polish) please see LICENSE.polish
  
   Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)

   This file is a part of the nminds-x4 program, used to conduct psychological
   experiments on group decision-making. For more information please see README file.

Functions and classes for dynamically importing modules containing experiment code.

"""

import os.path
import sys
import logging

#: Logger for this module
log = logging.getLogger(__name__)


def import_experiment_module(name, path):
    """
    Imports a module.

    :param name: Name of the module to import.
    :param path: Path to the module.

    :returns: A module object representing the loaded module.

    """
    module_path = os.path.join(path, name)
    if module_path not in sys.path:
        sys.path.append(module_path)
    if name not in sys.modules:
        log.debug('Importing module {} (in {}) ...'.format(name, module_path))
        __import__(name)
        log.debug('Module {} (in {}) imported.'.format(name, module_path))
    return sys.modules[name]


class DirInfo(object):
    """
    A listing of a directory's contents.
    Recursively includes subdirectories.

    """

    def __init__(self, root):
        """
        Initializes the object.

        :param root: The path to the directory.

        """
        self.root = root
        self._entries = set()
        for dirpath, dirnames, filenames in os.walk(root):
            relpath = os.path.relpath(dirpath, root)
            for f in filenames:
                self._entries.add(os.path.join(relpath, f))
            for d in dirnames:
                self._entries.add(os.path.join(relpath, d))

    def compare(self, other):
        """
        Not implemented yet. Always returns an empty list.

        """
        diff = []
        for f in self._entries:
            if f in self._entries:
                pass  # TODO: md5 check?
            else:
                diff.append('Missing: ' + f)
        return diff
