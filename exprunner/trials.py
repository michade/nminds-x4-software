''"""
.. Copyright (c) 2014 Institute of Psychology of the Polish Academy of Sciences.
   All rights reserved. For full license text (in Polish) please see LICENSE.polish
  
   Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)

   This file is a part of the nminds-x4 program, used to conduct psychological
   experiments on group decision-making. For more information please see README file.

Trials - utility for storing table-like data. Data can be placed on multiple levels.

"""

from collections import OrderedDict


class IntegerIndexer(object):
    """
    Assigns integer IDs to Trials' rows.
    Not thread safe.

    """

    def __init__(self, start=0):
        """
        Initializes a new indexer.

        :param start: First index to assign. Following assignments will return consecutive integers.
        
        """
        self._n = start

    def next(self):
        """
        Assigns index for another row. Following assignments will return consecutive integers.

        :returns: A new index.

        """
        res = self._n
        self._n = self._n + 1
        return res


class Trials(object):
    """
    Stores data in a tree-like fashion - each node has a set of attribute-value pairs.
    Descendant nodes inherit ancestor's attributes. Ancestor attributes cannot be overriden in descendant nodes.
    Nodes on the same level may have different sets of attributes. Each child of a node has a
    special index attribute, which is set on creation and cannot be altered. Index is unique within a node's parent.
    Children are ordered according to the index attribute. Attributes are also ordered (according to the
    order in which they are added).

    """

    def __init__(self, parent=None, children=None, index=None, data=None, indexer='ints', index_name=None):
        """
        Initializes the Trials obejct.

        :param parent: A Trials object, which is a parent to this node. `None` for root.
        :param children: Children of this node. Either a (possibly ordered) dictionary, or a list of (index, child) pairs.
        :param index: The index of this node (under which it is stored in the parent).
        :param data: A (possibly ordered) dictionary containing attribute-value pairs.
        :param indexer: An object used to assign new indexes to the children of this node.
                        Pass 'ints' to use a new instance of IntegerIndexer.
                        Can be `None` for manual index assingment.
        :param index_name: The name of a column used as the index attribute, in this node's children.
        
        """
        self._parent = parent
        self._children = OrderedDict(children) if children else OrderedDict()
        self._data = OrderedDict()
        if parent is not None and parent._index_name:
            self._data[parent._index_name] = index
        if data:
            self._data.update(data)
        if indexer == 'ints':
            indexer = IntegerIndexer()
        self._indexer = indexer
        self._index = index
        self._index_name = index_name

    def __getitem__(self, key):
        """
        Returns the value of the attribute named `key` of this node,
        or it's ancestors (ancestor and descendadnt attribute names cannot repeat,
        so this is unambigious). Raises KeyError in case attribute is not found.
        A descendant node's attribute cannot be accessed from its ancestor nodes.

        :param key: The name of the attriute to retreive.

        :returns: The value of the attribute.
        
        """
        if key in self._data:
            return self._data[key]
        else:
            if self._parent:
                return self._parent[key]
            else:
                raise KeyError('Key not found: "' + str(key) + '"')

    def __contains__(self, key):
        """
        Checks if the node or one of its ancestors contain an attribute of a name
        specified by the `key` argument (ancestor and descendadnt attribute names cannot repeat,
        so this is unambigious).
        A descendant node's attribute is not considered to be contained in its ancestor nodes.

        :param key: The name of the attriute to look for.

        :returns: True if the node, or one of it's ancestors contains the attribute.
        
        """
        if key in self._data:
            return True
        else:
            return self._parent and key in self._parent

    def __setitem__(self, key, value):
        """
        Sets the value of an attribute if it is present in this node.
        Adds an attribute to a node if it is not present.
        Ancestor or descendant attributes cannot be assigned to - KeyError is raised on such an attempt.

        BUG: you can still override a descendadnt attribute. Don't do that.

        :param key: Attribute to set.
        :param value: New attribute value.
        
        """
        if self._parent is not None:
            if key in self._parent:
                raise KeyError('Invalid level')
            if key == self._parent._index_name:
                raise KeyError('Invalid key: ' + str(key))
        self._data[key] = value

    def child(self, index):
        """
        Returns a child of a given index.
        
        """
        return self._children[index]

    def add_child(self, data, index=None, indexer='ints', index_name=None):
        """
        Creates a new child for this node.

        :param data: An ordered dictionary of attributes to add the child.
        :param index: Index of the child. (If the parent doesn't specify an indexer.)
        :param indexer: An object used to assign new indexes to the children of the child node.
                        Pass 'ints' to use a new instance of IntegerIndexer.
                        Can be `None` for manual index assingment.
        :param index_name: The name of a column used as the index attribute, in the child node's children.

        :returns: The newly created child node.
        
        """
        if index in self._children:
            raise KeyError('Index "' + str(index) + '" already present.')
        for k in data.keys():
            if k in self:
                raise KeyError('Key "' + str(k) + '" already present at higher level')
            if k == self._index_name:
                raise KeyError('Key "' + str(k) + '" already used as index')
        if self._indexer and index:
            raise Exception('Attempt to specify child index manually, while parent indexer is present.')
        if not index:
            index = self._indexer.next()
        child = Trials(self, None, index, data, indexer, index_name)
        self._children[index] = child
        return child

    @property
    def nrow(self):
        """
        Calculates the number of rows in a tabular representation of this object.
        Contains rows from this node and its's children.

        """
        if len(self._children) > 0:
            return sum([c.nrow for c in self.children()])
        else:
            return 1 if self._parent or len(self._data) > 0 else 0

    def children(self):
        """
        Returns an iterator over the children of this node.

        """
        return self._children.values()

    @property
    def parent(self):
        """
        The parent of this node (`None` for root).

        """
        return self._parent

    @property
    def index(self):
        """
        The index of this node within its parent.

        """
        return self._index

    def __len__(self):
        """
        The number of children of this node.

        """
        return len(self._children)

    def top(self):
        """
        Returns the root of this node's ancestral hierarchy.

        """
        top = self
        while top.parent is not None:
            top = top.parent
        return top

    def colnames(self, parent=True):
        """
        Returns the names of attributes (columns) of this node and
        its descendants OR (if `parent` is True) the entire tree's attributes.
        The attributes are ordered first according to the level
        in the node hierarchy, then by the index order in their most
        common ancestor, and finally in order they were added to the node.
        (In other words the attributes are ordered breadth-first)

        :param parent: Include the ancestors' attributes?

        :returns: A list of the attributes.
        
        """
        if parent:
            return self.top().colnames(False)
        return self._colnames(set())

    def _colnames(self, used):
        """
        Heler to compute the colnames. Enumerates attributes
        recursively, breadth-first.

        :param used: The set of already discovered attribute names (Modified by the function).

        """
        names = [k for k in self._data.iterkeys() if k not in used]
        used.update(names)
        for child in self._children.itervalues():
            names += child._colnames(used)
        return names
    
    def __str__(self):
        """
        Tabular string representation of this and descendant nodes, but
        containing ancestor attributes.

        """
        return self.format(header=True, sep=',\t', rowsep='\n')

    def rows(self, colnames=None, na_value=None):
        """
        List representation of this and descendant nodes, but
        containing ancestor attributes.

        :param colnames: Extract these columns only. None means all columns shuld be included.
        :param na_value: Value used to indicate, the absence of an attribute value. Defaults to `None`.

        """
        colnames = colnames if colnames else self.colnames()
        if len(self._children) > 0:
            rows = []
            for c in self._children.itervalues():
                rows.extend(c.rows(colnames))
            return rows
        elif len(self._data) > 0:
            return ([(self[k] if k in self else na_value) for k in colnames],)
        elif self._parent:
            return ([],)
        else:
            return ()

    def data(self):
        """
        Return this node and ancestor's data, as an OrderedDict.

        """
        if self._parent:
            data = self._parent.data()
            data.update(self._data)
            return data
        else:
            return OrderedDict(self._data)

    def dump_str(self, indent=0):
        """
        Returns a print-frienfly string representation of the this node and descendant's structure.

        """
        tabs = ''.join(['\t' for _ in range(indent + 1)])
        return str(self._data) + ''.join(['\n' + tabs + str(k) + ':\t' + v.dumps(indent + 1) for k, v in self._children.iteritems()])

    def format(self, header=True, sep=',\t', rowsep='\n', na_string='NA'):
        """
        Returns a tabular representation of this and descendant nodes, but
        containing ancestor attributes.

        :param header: Include column names as first row?
        :param sep: Item separator.
        :param rowsep: Row separator.
        :param na_string: String used to indicate absent attribute value.
        
        """
        cn = self.colnames()
        hdr = [sep.join(cn)] if header else []
        return rowsep.join(hdr + [sep.join([str(v) if v is not None else na_string for v in r]) for r in self.top().rows(cn)])
