"""
.. Copyright (c) 2014 Institute of Psychology of the Polish Academy of Sciences.
   All rights reserved. For full license text (in Polish) please see LICENSE.polish
  
   Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)

   This file is a part of the nminds-x4 program, used to conduct psychological
   experiments on group decision-making. For more information please see README file.

This module contains the client program, represented by an instance of `Client`,
which connects to the server and awaits commands - such as launching an experiment.

"""

import sys
import logging
import exprunner.message as message
import exprunner.msgsocket as msgsocket
import exprunner.multiplexer as multiplexer
import exprunner.rpc as rpc
import exprunner.importer as importer
import exprunner.experiment as experiment
from exprunner.commands import Command
from common import make_log_message_id

#: Logger for use by Client instances (and related objects).
log = logging.getLogger('client')


class ExperimentRunner(multiplexer.ThreadedMessageHandler):
    """
    Handles the execution of experiment code.

    """

    def __init__(self, socket, addr, owner):
        """
        Initialises instance and send introduction to the server.

        :param socket: TCP socket, connected to the server.
        :param addr: Tuple (address, port) of the connected socket.
        :param owner: Client instance containing this object.

        """
        super(ExperimentRunner, self).__init__(socket, addr, owner, 'Connection', True)
        self._rpc = rpc.Dispatcher(self._send_rpc)
        self.send(message.Introduction(self.owner.client_type))

    def _send_rpc(self, call):
        """
        Dispatch an rpc call through the connection.

        :param call: a Call object to dispatch.

        """
        log.debug('call: %s', call)
        self.send(message.Rpc(call))

    def run_thread(self):
        """
        Overriden from ThreadedMessageHandler.
        Runs client code in a separate thread.

        """
        if self.owner.client_type == experiment.ClientType.MASTER:
            self._run_master()
        else:
            self._run_experiment_threaded()

    def _run_master(self):
        """
        Runs the remote monitoring client logic.

        """
        i = 0
        print '*** Relay started'
        while True:
            id = make_log_message_id(i)
            text = self.rpc.call('logs.get', id)
            if text is None:
                print '***  Relay disconnected'
                break
            else:
                i += 1
                print '*', text
        self.close()
        self.owner.shutdown()

    def _run_experiment_threaded(self):
        """
        Runs experiment code in a separate thread.

        """
        experiment_ = None
        log.debug('Entering loop.')
        while True:
            try:
                shutdown = not self.rpc.call('.'.join(['clients', self.owner.name, 'wait']))
            except Exception:
                log.error('Fatal error')
                log.error(rpc.format_traceback())
                shutdown = True
            if shutdown:
                break
            try:
                exp_info = self.rpc.call('experiment.get_info')
                local = importer.DirInfo(exp_info.files.root)
                mismatches = exp_info.files.compare(local)
                if len(mismatches) > 0:
                    for s in mismatches:
                        log.warning(s)
                    raise Exception('Found %d mismatches reading experiment directory.' % (len(mismatches),))
                if experiment_ is None or exp_info.name != experiment_.info.name:
                    experiment_ = self._prepare_experiment(exp_info)
                self.rpc.call('experiment.client_starting', self.owner.name)
                experiment_.run()
                self.rpc.call('experiment.client_finished', self.owner.name)
            except Exception:
                experiment_ = None
                log.error('Error while running experiment')
                log.error(rpc.format_traceback())
                try:
                    self._rpc.call('experiment.abort')
                except Exception:
                    log.error('Error leaving experiment')
                    log.error(rpc.format_traceback())
        log.debug('Exiting loop')
        self.close()
        self.owner.shutdown()

    def _prepare_experiment(self, exp_info):
        """
        Creates experiment instance and helper objects.
        Runs `prepare` method of the experiment object.

        :param exp_info: Relevant ExperimentInfo object.

        """
        exp_dir = self.owner.config.get('paths', 'experiments')
        exp_module = importer.import_experiment_module(exp_info.name, exp_dir)
        cfg = self.owner.config.clone()
        cfg.test_mode = exp_info.test_mode
        exp = exp_module.Main(self.owner.name, exp_info, self._rpc, cfg, self.owner.pipes)
        exp.prepare(*exp_info.args, **exp_info.kwargs)
        return exp

    def handle_message(self, msg):
        """
        Hook to handle rpc messages separately.
        Overriden from ThreaedMessageHandler.

        :param msg: Received message.

        """
        if isinstance(msg, message.RpcResult):
            log.debug('result: %s', msg.result)
            self._rpc.result_received(msg.result)
        else:
            super(ExperimentRunner, self).handle_message(msg)

    def handle_disconnect(self):
        """
        Handler for the disconnection event. Writes message to stdout and shuts down the reactor.

        """
        self.owner.output.writelines(['Server disconnected.'])
        self.close()

    def close(self):
        """
        Closes the connection, aborts all rpc calls in progress.

        """
        self._rpc.finish_all()
        super(ExperimentRunner, self).close()

    @property
    def rpc(self):
        """
        Object providing rpc interface to the experiment code.

        """
        return self._rpc


class ConnectionHandler(multiplexer.ThreadedMessageHandler):
    """
    Maintains the connection to the server.

    """

    def __init__(self, socket, addr, owner):
        """
        Initializes the object and sends introductory message.

        :param socket: Connected TCP socket.
        :param addr: Address tuple (addr, port) of the local endpoint.
        :param owner: owning Client object.

        """
        super(ConnectionHandler, self).__init__(socket, addr, owner, 'Connection')
        self._rpc = rpc.Dispatcher(self._send_rpc)
        self.send(message.Introduction(self.owner.client_type))
        self._fetch_server_commands()

    def _fetch_server_commands(self):
        """
        Querries the server for available commands and creates local interface for them.

        """
        log.debug('Fetching server commands.')
        try:
            server_commands = self.rpc.call('commands.fetch')
            local_commands = self.owner.root.commands
            inew = 0
            for cmd in server_commands:
                if cmd.name not in local_commands:
                    local_commands.add(cmd)
                    inew += 1
            log.debug('Fetched %d commands (%d new).', len(server_commands), inew)
        except Exception:
            log.error('Error getting server commands')
            log.error(rpc.format_traceback())

    def _send_rpc(self, call):
        """
        Sends and rpc call as a message trough the connection.

        """
        log.debug('%s', call)
        self.send(message.Rpc(call))

    def handle_message(self, msg):
        """
        Handles the rpc message `msg`.

        :param msg: Message to be handled (must be instance of message.RpcResult).

        """
        if isinstance(msg, message.RpcResult):
            self._rpc.result_received(msg.result)
        else:
            log.warning('Unexpected message: %s', str(msg))

    def handle_disconnect(self):
        """
        Handles disconnection event - writes message to stdout and shuts down the client.

        """
        self.owner.output.writelines(['Server disconnected.'])
        self.close()

    def close(self):
        """
        Closes the connection and aborts all rpc calls in progress.

        """
        super(ConnectionHandler, self).close()
        self._rpc.finish_all()

    @property
    def rpc(self):
        """
        Object providing interface to rpc capabilities.

        """
        return self._rpc


class ClientCommands(object):
    """
    Represents commands exposed by the client.

    """
    def __init__(self, owner):
        """
        Inits the object.

        """
        self.owner = owner

    def help_(self):
        """
        Help command - prints available commands and some other info.

        """
        lines = []
        lines.append('Commands:')
        lines.extend(self.owner.root.commands.format_help())
        return lines

    def quit_(self):
        """
        Shuts the client down.

        """
        self.owner.shutdown()

    def info(self):
        """
        Prints client name and connection info.

        """
        return str(self.owner.get_remote_addr())


class Client(multiplexer.Multiplexer):
    """
    Represents the client program, capable of connecting to the server,
    loading and runnning an experiment.
    Client can run multiple experiments in its lifecycle.

    """

    def __init__(self, name, interactive, config, pipes):
        """
        Initializes the client.

        :param name: Name of the client.
        :param interactive: Allow stdin input?
        :param config: parent config object, containing connection information.
        :param pipes: Pipes used for IPC between pclients on same physical machine.

        """
        multiplexer.Multiplexer.__init__(self)
        self._pipes = pipes
        self._name = name
        self.connection = None
        self._config = config
        if interactive:
            self._client_type = experiment.ClientType.MASTER
            self.output = multiplexer.FileOutputHandler(self, sys.stdout, 'stdout')
            self.add_handler(self.output)
            self.conn_handler_class = ExperimentRunner
#            self._client_type = experiment.ClientType.MASTER
#            cmds = commands.Commands(ClientCommands(self))
#            for cmd in self._make_commands():
#                cmds.add(cmd)
#            self.root = rpc.Directory(None)
#            self.root.put('commands', cmds)
#            self.output = multiplexer.FileOutputHandler(self, sys.stdout, 'stdout')
#            self.add_handler(self.output)
#            self.add_handler(commands.CommandsHandler(self, sys.stdin, 'stdin'))
#            self.conn_handler_class = ConnectionHandler
        else:
            #stdout tymczasowo?
            self._client_type = experiment.ClientType.NORMAL
            self.output = multiplexer.FileOutputHandler(self, sys.stdout, 'stdout')
            self.add_handler(self.output)
            self.conn_handler_class = ExperimentRunner

    def try_connect(self, addr, port, reuse_addr):
        """
        Attempts to connect to the specified (addr, port) endpoint.

        """
        sock = msgsocket.MsgSocket(reuse_addr=reuse_addr)
        addr = sock.connect(addr, port)
        if addr is None:
            return False
        self.connection = self.conn_handler_class(sock, addr, self)
        self.add_handler(self.connection)
        return True

    def is_connected(self):
        """
        Is the connection already established adn functionning?

        """
        return self.connection is not None

    def get_remote_addr(self):
        """
        Returns a tuple (addr, port) containing remote endpoint information.

        """
        return self.connection.get_remote_addr()

    def _make_commands(self):
        """
        Creates and returns a list of Command objects for the clients commands.

        """
        return [Command('help', self.name, target='help_', desc='Print help.', aliases=['h', '?']),
                Command('quit', self.name, target='quit_', desc='Close this program.', aliases=['q']),
                Command('info', self.name, desc='Info about this program.')]

    @property
    def name(self):
        """
        Name of this client.

        """
        return self._name

    @property
    def client_type(self):
        """
        Type of this client - one of the experiment.ClientType members.

        """
        return self._client_type

    @property
    def config(self):
        """
        Configuration object of this client.

        """
        return self._config

    @property
    def pipes(self):
        """
        EventHub, which experiments ran by this client use for IO monitoring.

        """
        return self._pipes
