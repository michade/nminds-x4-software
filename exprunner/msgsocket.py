"""
.. Copyright (c) 2014 Institute of Psychology of the Polish Academy of Sciences.
   All rights reserved. For full license text (in Polish) please see LICENSE.polish
  
   Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)

   This file is a part of the nminds-x4 program, used to conduct psychological
   experiments on group decision-making. For more information please see README file.

A wrapper around socket.socket, that sends and receives python objects.
Uses pickle for serialization.

"""

import struct
import socket
import cPickle as pickle
from collections import deque
import logging


def addr_to_readable_str(addr):
    """
    Converts an (address, port) tuple (`addr`) to a human-readable string.

    """
    return str(addr[0] + ':' + str(addr[1]))


class MessageHeader(object):
    """
    A struct-like message header.
    Contents:
    time_sent - (float) The time the message was sent, relative to msgsocket's clock.
    size - (int) Size of the message body (i.e. not including header) in bytes.

    """

    def __init__(self):
        """
        Initializes the object with deafult values.

        """
        self.size = None


class Message(object):
    """
    A struct-like message.
    Contents:
    sender - (string) Name of the sender.
    heder - (MessageHeader) Header of this message.
    """
    def __init__(self):
        """
        Initializes the object with deafult values.

        """
        self.sender = None
        self.header = None

#: Size of the read buffer, in bytes, used by the msgsocket.
READ_BUFF_SIZE = 4096


class _LoggerAdapter(logging.LoggerAdapter):
    """
    A helper formatting logs regarding messages.
    See logging.LoggerAdapter docs for more info.

    """
    def __init__(self, logger):
        """
        Initializes the object.

        :param logger: The parent logger object.

        """
        logging.LoggerAdapter.__init__(self, logger, {})
        self._addr = '-'

    def process(self, msg, kwargs):
        """
        Processes the message, adding the adress information.

        :param msg: The log message string.

        :returns: The processed message string.

        """
        msg = '[%s] %s' % (self._addr, msg)
        return logging.LoggerAdapter.process(self, msg, kwargs)

    @property
    def addr(self):
        """
        The address to indicate in the log messages.

        """
        return self._addr

    @addr.setter
    def addr(self, value):
        """
        The address to indicate in the log messages.

        """
        self._addr = value


class MsgSocket(object):
    """
    A wrapper around socket.socket, that sends and receives python objects.
    Uses pickle for serialization.

    """

    def __init__(self, sock_and_addr=None, reuse_addr=False):
        """
        Initializes the object. You can pass an already connected socket,
        or establish a connection later, using `connect`.

        :param sock_and_addr: Tuple containing an already connected socket and remote address of the other endpoint.
        :param timer: A timeutil.Timer instance to timestamp the messages.
        :param reuse_addr: Reuse old binding addresses? See socket.socket doc for more info.

        """
        self._logger = _LoggerAdapter(logging.getLogger(__name__))
        if sock_and_addr is None:
            self._socket, self._remote_addr = None, None
        else:
            self._socket, self._remote_addr = sock_and_addr
            self._logger.addr = addr_to_readable_str(self.remote_addr)
        self._read_buff = ''
        self._curr_header = None
        self._curr_bytes_to_read = MsgSocket._header_struct.size
        self._curr_read_pos = 0
        self._write_queue = deque()
        self._write_buff = None
        self._write_pos = 0
        self._reuse_addr = reuse_addr

    def __del__(self):
        """
        Cleans up the connection.

        """
        self.close()

    def connect(self, host, port):
        """
        Establish a connection with a remote MsgSocket.

        :param host: The remote host address.
        :param port: The remote port number.

        :returns: The remote address form, that actually made the connection.

        """
        infos = socket.getaddrinfo(host, port, socket.AF_UNSPEC, socket.SOCK_STREAM)
        for ifo in infos:
            addr = ifo[4]
            try:
                self.log.info('Attempting to connect to %s', addr_to_readable_str(addr))
                self.connect_to(ifo)
            except socket.error as err:
                self.log.info('Unable to connect to %s: %s', addr_to_readable_str(addr), err)
                continue
            break
        return self._remote_addr

    def connect_to(self, info):
        """
        Connects to a remote host using specific paramters, stored in `info`.
        See socket.socket documentation for details.

        """
        family, socktype, proto, _, sockaddr = info
        try:
            sock = socket.socket(family, socktype, proto)
            sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
            if self._reuse_addr:
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.connect(sockaddr)
            self._remote_addr = info[4]
            self.log.addr = addr_to_readable_str(self.remote_addr)
            self.log.debug('Socket created and connected.')
            self._socket = sock
        except socket.error as err:
            if sock:
                sock.close()
                self.log.debug('Socket closed after error: %s', err)
            raise err

    def close(self):
        """
        Closes the connection (including the underlying socket.socket).

        """
        if self._socket:
            sock = self._socket
            self._socket = None
            sock.close()
            self.log.debug('Socket closed - %s', self._remote_addr)

    def recv(self):
        """
        Try to read messages from the socket.
        Makes exactly one call to the underlying socket.recv.
        Blocks if a read call to the underlying socket would block.

        :returns: List of complete messages that have been received, or None if the connection has been closed.

        """
        if self._socket is None:  # not very elegant slient failure
            return None
        s = ''
        try:
            s = self._socket.recv(READ_BUFF_SIZE)
            self.log.debug('Read %db from socket.', len(s))
        except socket.error, msg:
            self.log.error('Socket read error: %s', msg)
        if len(s) == 0:
            return None
        self._read_buff += s
        lst = []
        while self._curr_bytes_to_read <= len(self._read_buff) - self._curr_read_pos:
            msg_end = self._curr_read_pos + self._curr_bytes_to_read
            msg_s = self._read_buff[self._curr_read_pos:msg_end]
            if self._curr_header:
                msg = pickle.loads(msg_s)
                lst.append(msg)
                self._curr_bytes_to_read = MsgSocket._header_struct.size
                self.log.debug('Socket completed reading message.')
                self._curr_header = None
            else:
                self._curr_header = self._read_header(msg_s)
                self._curr_bytes_to_read = self._curr_header.size
            self._curr_read_pos = msg_end
        self._read_buff = self._read_buff[self._curr_read_pos:]
        self._curr_read_pos = 0
        return lst

    def recvall(self):
        """
        Calls recv until all messages have been read.

        :returns: A list of all received messages, or None if the conection has been closed.

        """
        while True:
            lst = self.recv()
            if lst is None:
                return None
            if len(lst) > 0:
                return lst

    #: Structure of the message prefix.
    _header_struct = struct.Struct('!I')

    def _read_header(self, s):
        """
        Unpacks a header from a string of bytes.

        """
        tmp = MsgSocket._header_struct.unpack_from(s)
        header = MessageHeader()
        header.size = tmp[0]
        return header

    def _write_header(self, msg_len):
        """
        Packs a header as a tring of bytes.

        """
        return MsgSocket._header_struct.pack(msg_len)

    def push(self, msg):
        """
        Enqueues a message to be sent. It is serialized immidiately.

        """
        s = pickle.dumps(msg)
        self._write_queue.append(s)

    def send(self):
        """
        Tries to send some messages, previously added by `push`.

        :returns: True if there are remaining messages to bes sent after the call. False otherwise.

        """
        if self._write_buff is None:
            if len(self._write_queue) == 0:
                self.log.debug('\'send\' called, but no data to send.')
                return False
            msg_s = self._write_queue.popleft()
            header_s = self._write_header(len(msg_s))
            self._write_buff = header_s + msg_s
        n = 0
        try:
            n = self._socket.send(self._write_buff[self._write_pos:])
        except socket.error, err_msg:
            self.log.debug('Socket write error %s.', err_msg)
            return None
        self._write_pos += n
        self.log.debug('Written %db to socket.', n)
        if self._write_pos < len(self._write_buff):
            return True
        self.log.debug('Socket completed writing message.')
        self._write_pos = 0
        self._write_buff = None
        return len(self._write_queue) > 0

    def sendall(self):
        """
        Calls send, until all pending messages are sent.

        :returns: False (analogously to `send`).

        """
        cont = True
        while cont:
            cont = self.send()
        return cont

    def fileno(self):
        """
        Returns the fileno of the underlying socket.

        """
        return self._socket.fileno()

    @property
    def remote_addr(self):
        """
        The addres and port of the connected remote endpoint.

        """
        return self._remote_addr

    @property
    def log(self):
        """
        Logger used by this instance.

        """
        return self._logger
