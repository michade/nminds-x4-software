"""
.. Copyright (c) 2014 Institute of Psychology of the Polish Academy of Sciences.
   All rights reserved. For full license text (in Polish) please see LICENSE.polish
  
   Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)

   This file is a part of the nminds-x4 program, used to conduct psychological
   experiments on group decision-making. For more information please see README file.

This module contains the server program, represented by an instance of `Server`,
which is an intermediary and coordinator between clients. Also accepts
command line input regarding e.g. starting an experiment.

"""

import os
import sys
import threading
import logging
import exprunner.multiplexer as multiplexer
import exprunner.message as message
import exprunner.rpc as rpc
import exprunner.commands as commands
import exprunner.experiment as experiment
import exprunner.importer as importer
from exprunner.commands import String
from exprunner.commands import Command
from exprunner.msgsocket import addr_to_readable_str
from experiment import ClientType
from common import make_log_message_id

#: Logger for use by Client instances (and related objects).
log = logging.getLogger('server')


class RpcDirRelayHandler(logging.Handler):
    """
    A logging.Handler subclass, that puts log messages in a specified rpc directory,
    under consecutive ids.

    """
    
    def __init__(self, target, level=logging.INFO):
        """
        Initializes the object.

        :param target: The rpc.Directory instance to put messages in.
        :param level: The logging level of the handler.

        """
        super(RpcDirRelayHandler, self).__init__(level)
        self._target = target
        self._lock = threading.RLock()
        self._n_messages = 0

    def emit(self, record):
        """
        Puts the log message in the directory.

        """
        text = self.format(record)
        with self._lock:
            if self._n_messages is not None:
                self._target.put(make_log_message_id(self._n_messages), text)
                self._n_messages += 1

    def disconnect(self):
        """
        Puts `None` as a next message (signalling the end of logging) and stops logging further messages.

        """
        with self._lock:
            if self._n_messages is not None:
                self._target.put(make_log_message_id(self._n_messages), None)
                self._n_messages = None


class ClientInfo(object):
    """
    Utility structure contatingin information about a connected client.

    """

    def __init__(self, addr=None, name=None, type_=None, handler=None, connected=True):
        """
        Initializes the object with default values.

        :param addr: A tuple (addr, port) of the client's endpoint.
        :param name: Name of the client.
        :param type_: A ClientType value.
        :param handler: ClientHandler responsible for communication with this client.
        :param connected: Is this client connected?

        """
        self._name = name
        self._addr = addr
        self._type = type_
        self._handler = handler
        self._connected = connected
        self._wait_event = threading.Event()
        self._shutdown = False

    def get_addr(self):
        """
        Gets the tuple (addr, port) of the client's endpoint.

        """
        return self._addr

    def get_type(self):
        """
        Gets the ClientType value represenitg the client's type.

        """
        return self._type

    def set_type(self, value):
        """
        Sets the ClientType value represenitg the client's type.

        """
        self._type = value

    def get_handler(self):
        """
        Gets the ClientHandler responsible for communication with this client.

        """
        return self._handler

    def get_name(self):
        """
        Gets the name of the client.

        """
        return self._name

    def set_name(self, name):
        """
        Sets the name of the client.

        """
        self._name = name
        self._handler.set_name(self.get_thread_name())

    def get_thread_name(self):
        """
        Returns a siutable thread name that includes connection information,
        for readiblity and debugging purposes.

        """
        if self._name is not None:
            return '%s[%s]' % (self._name, addr_to_readable_str(self._addr))
        else:
            return addr_to_readable_str(self._addr)

    def set_connected(self, value):
        """
        Set the connection state information.

        """
        self._connected = value

    def is_connected(self):
        """
        Is the client connected?

        """
        return self._connected

    def wait(self):
        """
        Orders the client to wait until some commands appear
        (e.g. run an experiment) for it to execute.

        :returns: True, if new command is available; False, to shut the client down.

        """
        log.debug('Client \'%s\' about to wait...', self.get_name())
        self._wait_event.wait()
        self._wait_event.clear()
        log.debug('Client %s awakened', self.get_name())
        return self._shutdown

    def awaken(self, shutdown):
        """
        Awaken the client, possibly to start executing a command.

        :param shutdown: Shut the client down?

        """
        self._shutdown = shutdown
        log.debug('Waking up %s.', self.get_name())
        self._wait_event.set()


class ClientHandler(multiplexer.ThreadedMessageHandler):
    """
    Handles communication with the client in a reactive manner.
    Runs in its own thread.

    """

    def __init__(self, socket, owner, addr):
        """
        Initializes the object.

        :param socket: A socket.socket used for communication.
        :param owner: The Server instance owning this handler.
        :param addr: The (address, port) tuple of the remote endpoint.

        """
        super(ClientHandler, self).__init__(socket, addr, owner, is_introduced=False)
        self._info = None

    def handle_introduction(self, msg):
        """
        Sets up server structures when a new client is introduced.
        Can fire a hook method (if one is specified) if a given
        number of clients has connected.

        :param msg: The introduction message from the client.

        """
        self._info = ClientInfo(self.get_remote_addr(), msg.sender, msg.client_type, self)
        self.set_name(self._info.get_thread_name())
        self.clients.put(self._info.get_name(), self._info)
        if self._info.get_type() != ClientType.MASTER:
            if self.owner.new_client_hook is not None:
                n, cmd = self.owner.new_client_hook
                if len(self.get_exp_clients()) >= n + 1:  # n clients + server
                    log.info('Hook: %s clients connected, executing command: "%s"', n, cmd)
                    self.owner.commands_handler.handle_line(cmd)
        return True

    @property
    def clients(self):
        """
        An rpc.Directory object containing connected clients.

        """
        return self.owner.root.clients

    def get_exp_clients(self):
        """
        Returns the list of names of clients controlling the experiment (of ClientType CLIENT or SERVER).

        """
        return [name for name in self.clients.ls() if self.clients.get(name).get_type() != ClientType.MASTER]

    def thread_handle_message(self, msg):
        """
        Handles an rpc message (executes a remote call) in the handler's private thread.

        :param msg: An RpcMessage.

        """
        log.debug('%s:%s', msg.sender, msg.call)
        result = self.owner.root.call(msg.call)
        log.debug('%s:%s', msg.sender, result)
        self.send(message.RpcResult(result))

    def handle_disconnect(self):
        """
        Handles the disconnection event - aborts the experiment and closes the socket.

        """
        log.info('Client %s disconnected.', self.get_name())
        if self._info.get_type() != ClientType.MASTER:
            self.owner.root.experiment.abort()
        self.close()

    def __repr__(self):
        """
        Returns a human-readable representation of the object.

        """
        return 'ClientHandler({})'.format(self.get_name())


class ExperimentController(object):
    """
    A helper object running the experiment.

    """

    class State(object):
        """
        Enumeration of possible experiment states:
        INIT - Just created, in the initialization phase.
        READY - Ready to be run.
        STARTED - Experiment is running, or has finished.
        ABORTED - Experiment has been aborted.

        """
        INIT = 'init'
        READY = 'ready'
        STARTED = 'started'
        ABORTED = 'aborted'

    def __init__(self, owner, logger):
        """
        Initializes the object, starts the worker thread.

        :param owner: The owning Server instance.

        """
        self._logger = logger
        self._state_lock = threading.RLock()
        self._owner = owner
        self._rpc = rpc.Dispatcher(self._send_rpc)
        self._master = None
        self._exp_info = None
        self._exp_test_mode = False
        self._experiment = None
        self._start_checkpoint = None
        self._end_checkpoint = None
        self._is_shutting_down = threading.Event()
        self._state = ExperimentController.State.INIT
        self._worker = owner.get_main_worker(self.run_thread)
        self._start_setup = threading.Event()
        self._setup_ready = threading.Event()
        self._worker.start()
        self.dir = rpc.Directory(None)

    def _send_rpc(self, call):
        """
        A method to execute a local rpc call.

        """
        log.debug('local call: %s', call)
        res = self.owner.root.call(call)
        self._rpc.result_received(res)

    def run_thread(self):
        """
        Prepares and runs the experiment in a separate thread.

        """
        cont = True
        while cont:
            self._start_setup.wait()
            if self.is_shutting_down():
                self._setup_ready.set()
                break
            self._start_setup.clear()
            try:
                #other thread holds state lock, but waits for self._setup_ready
                log.debug('About to prepare experiment %s.', self._exp_info.name)
                self._experiment = self._prepare_experiment(self._exp_info)
                log.debug('Finished preparing experiment %s.', self._exp_info.name)
                self._state = ExperimentController.State.READY
            except:
                log.error('Error preparing experiment %s.', self._exp_info.name)
                log.error(rpc.format_traceback())
            finally:
                self._setup_ready.set()
            try:
                if self._rpc.call('.'.join(['clients', self.owner.name, 'wait'])):
                    self._rpc.call('experiment.client_starting', self.owner.name)
                    log.debug('About to run experiment %s.', self._exp_info.name)
                    self._experiment.run()
                    log.debug('Finished running experiment %s.', self._exp_info.name)
                    self._rpc.call('experiment.client_finished', self.owner.name)
                else:
                    cont = False
            except:
                log.error('Error running experiment %s.', self._exp_info.name)
                log.error(rpc.format_traceback())
                cont = False
            if self.owner.experiment_done_hook is not None:
                cmd = self.owner.experiment_done_hook
                log.info('Hook: experiment done, executing command: %s', cmd)
                self.owner.commands_handler.handle_line(cmd)

    def setup(self, name, clients, args, kwargs, test_mode):
        """
        Prepares the experiment state.

        :param name: Name of the experiment.
        :param clients: List of tuples (name, type) for clients currently connected. They will be added to the experiment.
        :param args: Positional arguments to the experiment's __init__ method.
        :param kwargs: Keyword arguments to the experiment's __init__ method.
        :param test_mode: Run the experiment in test mode (usin test config files)?

        :returns: True if the setup was successful.

        """
        with self._state_lock:
            self._assert_state(ExperimentController.State.INIT, ExperimentController.State.ABORTED)
            exp_root = self.owner.config.get('paths', 'experiments')
            path = os.path.join(exp_root, name)
            if not (os.path.exists(path) and os.path.isdir(path)):
                raise Exception('Experiment folder not found: ' + path)
            main_path = os.path.join(path, name + '.py')
            if not (os.path.exists(main_path) and os.path.isfile(main_path)):
                raise Exception('Experiment main file not found: ' + path)
            clients_and_types = [(c.get_name(), c.get_type()) for c in clients]
            self._exp_info = experiment.ExperimentInfo(name,
                                                       importer.DirInfo(name),
                                                       clients_and_types,
                                                       test_mode,
                                                       args, kwargs)
            self._start_setup.set()  # slippery...
            self._setup_ready.wait()
            if self.is_shutting_down():
                return False
            self._setup_ready.clear()
            return True

    def is_shutting_down(self):
        """
        Is this instance terminating its loop?

        """
        return self._is_shutting_down.is_set()

    def shutdown(self):
        """
        Shuts down the loop.

        """
        self._is_shutting_down.set()
        self._start_setup.set()
        self._setup_ready.set()
        for c in self.clients.itervalues():
            c.awaken(False)

    def _prepare_experiment(self, exp_info):
        """
        Creates the experiment instance and prepares it for running (runs its `prepare method).

        :param exp_info: An ExperimentInfo object with additional information.

        :returns: The experiment instance.

        """
        exp_dir = self.owner.config.get('paths', 'experiments')
        exp_module = importer.import_experiment_module(exp_info.name, exp_dir)
        cfg = self.owner.config.clone()
        cfg.test_mode = exp_info.test_mode
        exp = exp_module.Main(self.owner.name, exp_info, self._rpc, cfg, None, self._logger)
        exp.prepare(*exp_info.args, **exp_info.kwargs)
        return exp

    def start(self):
        """
        Starts the experiment.

        """
        with self._state_lock:
            self._assert_state(ExperimentController.State.READY)
            client_names = self.get_exp_clients()
            self._start_checkpoint = experiment.Meeting(client_names, on_started=self._on_experiment_started)
            self._end_checkpoint = experiment.Meeting(client_names, on_started=self._on_experiment_ended)

    def _on_experiment_started(self):
        """
        Called whe the experiment is started.

        """
        with self._state_lock:
            self._state = ExperimentController.State.STARTED

    def client_starting(self, client_name):
        """
        Notifies the server that a client starts executing the experiment.

        :param client_name: Name of the client.

        """
        with self._state_lock:
            self._assert_state(ExperimentController.State.READY)
        self._start_checkpoint.join(client_name)

    def client_finished(self, client_name):
        """
        Notifies the server, that a client has finished executing the experiment.

        :param client_name: Name of the client.

        """
        with self._state_lock:
            self._assert_state(ExperimentController.State.STARTED)
        self._end_checkpoint.join(client_name)

    def _on_experiment_ended(self):
        """
        Called after the experiment has ended.

        """
        with self._state_lock:
            self._state = ExperimentController.State.INIT

    def abort(self):
        """
        Aborts the experiment.

        """
        with self._state_lock:
            if self._state != ExperimentController.State.ABORTED:
                self._state = ExperimentController.State.ABORTED
                if self._experiment is not None:
                    self._experiment.abort()

    def get_state(self):
        """
        Gets the state of the experiment, one of ExperimentController.State values:
        INIT - Just created, in the initialization phase.
        READY - Ready to be run.
        STARTED - Experiment is running, or has finished.
        ABORTED - Experiment has been aborted.

        """
        with self._state_lock:
            return self._state

    def get_name(self):
        """
        Gets the name of the experiment.

        """
        with self._state_lock:
            return self._exp_info.name if self._exp_info is not None else 'None'

    def get_info(self):
        """
        Gets additiona experiment info (an ExperimentInfo instance).

        """
        with self._state_lock:
            return self._exp_info

    def _assert_state(self, *states):
        """
        Raises an exception if the current state is different from those specifeid in the argument.

        """
        st = self._state
        if st not in states:
            raise Exception('Invalid state: ' + st + ', experiment: ' + self.get_name())

    def _assert_not_state(self, *states):
        """
        Raises an exception if the current state is one of those specified in the argument.

        """
        st = self._state
        if st in states:
            raise Exception('Invalid state: ' + st + ', experiment: ' + self.get_name())

    @property
    def owner(self):
        """
        The Server instance owning this obejct.

        """
        return self._owner
    
    @property
    def clients(self):
        """
        An rpc.Directory object containing connected clients.

        """
        return self.owner.root.clients

    def get_exp_clients(self):
        return [name for name in self.clients.ls() if self.clients.get(name).get_type() != ClientType.MASTER]


class ServerCommands(object):
    """
    Contains the code that executes server commands.

    """
    def __init__(self, owner):
        """
        Initializes the object.

        :param owner: The relevant Server instance.

        """
        self.owner = owner

    def help_(self):
        """
        A command that prints help message and commands listing.

        :returns: A list of lines (strings) containing the output of the command.

        """
        lines = []
        lines.append('Commands:')
        lines.extend(self.owner.root.commands.format_help())
        return lines

    def quit_(self):
        """
        A command that shuts down the server.

        :returns: A list of lines (strings) containing the output of the command.

        """
        self.owner.root.experiment.shutdown()
        self.owner.shutdown()

    def info(self):
        """
        A command that prints connection information.

        :returns: A list of lines (strings) containing the output of the command.

        """
        return 'Server bound to address: {}:{}'.format(*self.owner.get_bind_info())

    def _start(self, name, args, test_mode=False):
        """
        Implementation of the start experiment logic.

        :param name: Name of the experiment to start.
        :param args: Positional arguments to the experiment's __init__ method.
        :param test_mode: Run the experiment in test mode (using test config files)?

        """
        clients = self.owner.root.clients
        exp = self.owner.root.experiment
        if exp.setup(name, clients.itervalues(), args, {}, test_mode):
            exp.start()
            for c in clients.itervalues():
                c.awaken(True)

    def start(self, name, *args):
        """
        A command that starts an experiment.

        :param name: Name of the experiment to start.
        :param args: Positional arguments to the experiment's __init__ method.

        """
        self._start(name, args)

    def test(self, name, *args):
        """
        A command that starts an experiment in test mode.

        :param name: Name of the experiment to start.
        :param args: Positional arguments to the experiment's __init__ method.

        """
        self._start(name, args, test_mode=True)

    def abort(self):
        """
        A command that aborts the currently running (or prepared) experiment.

        """
        self.owner.root.experiment.abort()

    def status(self):
        """
        A command that prints the state of the experiment (one of ExperimentController.State values).
        
        :returns: A list of lines (strings) containing the output of the command.

        """
        return self.owner.root.experiment.get_state()

    def echo(self, text):
        """
        A command that prints the text passed as its argument.

        :returns: A list of lines (strings) containing the output of the command.

        """
        txt = 'Echo: ' + text
        self.owner.output.writelines([txt])
        return txt

    def ls(self, path):
        """
        A command that lists the contents of the Server's rpc roots.

        :returns: A list of lines (strings) containing the output of the command.

        """
        if path == '.':
            res = self.owner.root.ls()
        else:
            dir_ = self.owner.root.get(path, False)
            res = dir_.ls()
        return res if len(res) > 0 else 'Empty.'


class Server(multiplexer.Multiplexer):
    """
    Represents the server program which acts an intermediary and coordinator between clients.
    Also accepts command line input regarding e.g. starting an experiment.

    """

    def __init__(self, config):
        """
        Initializes the object, using configuration provided in `config`.

        :param config: A common.Config object containing initial configuration.

        """
        super(Server, self).__init__()
        self._name = 'server'
        self._config = config
        self.new_client_hook = None
        self.experiment_done_hook = None
        self.root = rpc.Directory(None)
        self.root.mkdir('logs')
        self._log_relay_handler = RpcDirRelayHandler(self.root.logs)
        log.addHandler(self._log_relay_handler)
        self.root.mkdir('clients')
        self.root.clients.put(self.name, ClientInfo('', self.name, experiment.ClientType.SERVER, None, True))
        self.root.put('experiment', ExperimentController(self, log))
        self._init_commands()
        self._init_handlers()

    def _init_commands(self):
        """
        Creates objects represenitng and handling server commads.

        """
        cmds = commands.Commands(ServerCommands(self))
        for cmd in self._make_commands():
            cmds.add(cmd)
        self.root.put('commands', cmds)
        if self.config.getboolean('misc', 'console'):
            self.commands_handler = commands.CommandsHandler(self, sys.stdin, 'stdin')
        else:
            self.commands_handler = commands.CommandsHandler(self, None, 'cmd')
        self.add_handler(self.commands_handler)

    def _init_handlers(self):
        """
        Initializes handlers for various events, such as new connection
        and keyboard input.

        """
        self.output = multiplexer.FileOutputHandler(self, sys.stdout, 'stdout')
        self.add_handler(self.output)
        self.listener = multiplexer.TcpListener(self, self.on_client_connected)
        self.listener.set_accepting(True)
        self.add_handler(self.listener)

    def on_client_connected(self, sock, addr):
        """
        Called when a new connection has been established (socket-level).

        """
        handler = ClientHandler(sock, self, addr)
        log.info('Accepted new client at: %s', addr_to_readable_str(addr))
        return handler

    def bind(self):
        """
        Binds the server to listen to incoming connections on the
        endpoint specified in the configuration.

        """
        addr = self.config.get('connection', 'bind_ip')
        port = self.config.getint('connection', 'server_port')
        backlog = self.config.getint('connection', 'backlog')
        return self.listener.bind(addr, port, backlog)

    def get_bind_info(self):
        """
        Returns information about the binding address of the listening socket.

        """
        return self.listener.bind_info

    def shutdown(self):
        """
        Initiates shutdown sequence.

        """
        self._log_relay_handler.disconnect()
        super(Server, self).shutdown()

    def _make_commands(self):
        """
        Creates actual Command objects for server commands.

        :returns: A list of Command objects representing the server commands.

        """
        return [Command('help', self.name, target='help_',
                        desc='Print help.', aliases=['h', '?']),
                Command('quit', self.name, target='quit_',
                        desc='Close this program.', aliases=['q']),
                Command('info', self.name, desc='Info about this program.'),
                Command('start', self.name, desc='Starts experiment',
                        params=[String('name'), String('args', 0)]),
                Command('test', self.name, desc='Starts experiment in test mode',
                        params=[String('name'), String('args', 0)]),
                Command('abort', self.name, desc='Aborts experiment'),
                Command('status', self.name, desc='Prints experiment status'),
                Command('ls', self.name, desc='Lists contents of rpc directory',
                        params=[String('path')]),
                Command('echo', self.name, desc='Prints the text', params=[String('text')])]

    @property
    def name(self):
        """
        Name of the server. Usually 'server'.

        """
        return self._name

    @property
    def config(self):
        """
        A common.Config object containig configuration of the server.

        """
        return self._config
