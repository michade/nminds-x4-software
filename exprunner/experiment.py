"""
.. Copyright (c) 2014 Institute of Psychology of the Polish Academy of Sciences.
   All rights reserved. For full license text (in Polish) please see LICENSE.polish
  
   Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)

   This file is a part of the nminds-x4 program, used to conduct psychological
   experiments on group decision-making. For more information please see README file.

This module contains the base class for distributed experiments, and some utils
facilitating the implementation.

"""

import threading
import logging


def _split_name_from_path(path):
    """
    Splits name of the target rpc object from the rpc path.
    An rpc path has same syntax as a reference to multiple
    nested python objects,
    e.g. `foo.bar.baz` (here `baz` is the target name).

    :param path: The rpc path (string) to split.

    :returns: Tuple (base_path, name), where:
              * base_path -  Part of the path before the name component (i.e. `foo.bar`)
              * name - The last component of the path (i.e. `baz`)

    """
    i = path.rfind('.')
    if i == -1:
        return ('', path)
    return (path[:i], path[(i + 1):])


#: Default path containing the experiments on the server.
_EXPERIMENT_DIR = 'experiment.dir'


def _make_absolute_path(*elems):
    """
    Returns a path consisting of `elems` joined with rpc path separator ('.'),
    and prepended with default path containing the experiments.

    :returns: absolute rpc path consisting of `elems`

    """
    tmp = [_EXPERIMENT_DIR]
    tmp.extend(elems)
    return '.'.join([e for e in tmp if len(e) > 0])


class ClientType(object):
    """
    Enumeration of client types:

    """
    NORMAL = "normal"
    """Client executing experiment code."""

    MASTER = "master"
    """Client used to control and supervise the experiment execution."""

    SERVER = "server"
    """Server-side process coordinating the experiment."""


class Meeting(object):
    """
    A high-level thread synchronization device.

    A Meeting is created with a list of `guests`. A threads can then `join` a meeting
    (as a particual guest) - which means it will block, until all other guets on the list join.

    A meeting can also be started manually - before all guests have joined,
    or aborted - which causes blocking `join` calls raise an exception.

    A meeting is created in the `pending` state.
    After the last guest has joined (or `start` has been called), it changes
    state to `started`, and wakes up all the guests.
    Calling `join` on a started meeting returns immidiately.

    If, `abort` is called, the meeting becomes `aborted`.
    Joining an aborted meeting causes (immidiately) the same exception, as in the blocking case.

    """
    class State(object):
        """
        Enumeration describing the sate of the meeting:
        PENDING - The meeting has not yet been started.
        STARTED - The meeting has been started and all of its guests will be woken up shortly.
        ABORTED - The meeting has been aborted and all of its guests will be woken up shortly.

        """
        PENDING = "pending"
        """The meeting has not yet been started."""

        STARTED = "started"
        """The meeting has been started and all of its guests will be woken up shortly."""

        ABORTED = "aborted"
        """The meeting has been aborted and all of its guests will be woken up shortly."""

    def __init__(self, guest_list=None, autostart=True, on_started=None, master=None, auto_reset=True):
        """
        Initializes the meeting object. It is immidiately available for use.

        :param guest_list: List of guest names, for which the meeting will wait.
        Empty list causes the meeting to start immidiately.
        `None` casues the meeting to wait until start is explicitly called,
        regardless of what guests join.
        Note: The above referts to the defualt situation, when `autostart` is True.
        Specifying `autostart` as False will render this argument meaningless.
        :param autostart: Start the meeting if all guests on the list join? Default is True.
        :param on_started: A hook method to specify an additional action to be taken, after the meeting chages
        its state to started. The function provided will be called in one of the guest threads;
        It shouldn't block or take long to execute.
        :param master: Legacy parameter, leave it at its defult value.
        :param auto_reset: Reset meeting after it has been started? Note: reseting is obsolete and shouldn't be used.

        """
        if autostart and guest_list is None:
            raise Exception("Must specify guest list for autostart.")
        self._ready = threading.Condition()
        self._guests = set()
        self._guest_list = frozenset(guest_list) if guest_list else None
        self._autostart = autostart
        self._state = Meeting.State.PENDING
        self._on_started = on_started
        self._auto_reset = auto_reset

    def join(self, guest):
        """
        Join the meeting as a specified guest. Blocks until the meeting becomes `STARTED`
        (i.e. all guests call `join`, or `start` has been called), or aborted. In the latter
        case an exception is raised.

        Calling `join` on a started meeting returns immidiately.
        Joining an aborted meeting causes (immidiately) the same exception, as if
        the call blocked.

        :param guest: The name of the guest to join.

        """
        with self._ready:
            if self._guest_list and guest not in self._guest_list:
                raise Exception("Unexpected guest: " + guest)  # TODO: make exception classes
            if self._state == Meeting.State.ABORTED:
                raise Exception("Aborted")
            self._guests.add(guest)
            if self._autostart and len(self._guests) >= len(self._guest_list):
                self._do_start()
                self._ready.notify_all()
                self._guests.remove(guest)
                if len(self._guests) == 0 and self._auto_reset:
                    self._do_reset()
            else:
                while self._state == Meeting.State.PENDING:
                    self._ready.wait()
                    if guest not in self._guests:
                        raise Exception("Client left")
                    self._guests.remove(guest)
                    if len(self._guests) == 0 and self._auto_reset:
                        self._do_reset()
                        break
        if self._state == Meeting.State.ABORTED:
            raise Exception("Aborted")

    def abort(self):
        """
        Aborts the meeting. The meeting becomes `ABORTED`, then all clients are woken up,
        and their `join` calls raise an exception.
        Subsequent calls to `join` on this meeting also raise an exception.
        Subsequent calls to `start` or `abort` have no effect.

        """
        with self._ready:
            if self._state == Meeting.State.PENDING:
                self._do_abort()
                self._ready.notify_all()

    def start(self):
        """
        Manually starts the meeting. The meeting becomes `STARTED`, then all clients are woken up.
        Subsequent calls to `join` on this meeting return immidiately.
        Subsequent calls to `start` or `abort` have no effect.

        """
        with self._ready:
            if self._state == Meeting.State.PENDING:
                self._do_start()
                self._ready.notify_all()

    def leave(self, guest):
        """
        Obsolete, do not use.

        """
        with self._ready:
            self._guests.remove(guest)
            self._ready.notify_all()

    def reset(self):
        """
        Obsolete, do not use.

        """
        with self._ready:
            if self._state != Meeting.State.PENDING:
                self._do_reset()

    def _do_start(self):
        """
        Handles internal start logic.

        """
        self._state = Meeting.State.STARTED
        if self._on_started is not None:
            try:
                self._on_started()
            except Exception:
                self._do_abort()

    def _do_abort(self):
        """
        Handles internal abort logic.

        """
        self._state = Meeting.State.ABORTED

    def _do_reset(self):
        """
        Handles internal reset logic.

        """
        self._state = Meeting.State.PENDING
        self._guests.clear()

    @property
    def guests(self):
        """
        Returns the list of currently joined guests. Read-only.

        """
        with self._ready:
            return self._guests

    @property
    def guest_list(self):
        """
        Returns the list of guest names. Read-only.

        """
        return self._guest_list

    @property
    def state(self):
        """
        Returns the current state of the Meeting (value of class Meeting.State):
        PENDING - The meeting has not yet been started.
        STARTED - The meeting has been started and all of its guests will be woken up shortly.
        ABORTED - The meeting has been aborted and all of its guests will be woken up shortly.

        """
        with self._ready:
            return self._state


class Experiment(object):
    """
    An abstract class, whose subclasses represent experiment types.
    When running a distributed experiment, each client has its own local instance.

    """

    def __init__(self, client, info, rpc, config, pipes, logger=None):
        """
        Initializes the Experiment object.
        Warning: this does NOT call Experiment.prepare.

        :param client: Name of the client running this instance.
        :param info: ExperimentInfo object, containing additional experiment info.
        :param rpc: Interface for rpc calls to server.
        :param config: Local configuration information.
        :param pipes: Pipes used for ipc with other instances on same physical machine.
        :param logger: A logging.Logger instance used to log messages from the experiment.

        """
        self._control = ExperimentControl(info.name, rpc)
        self._info = info
        self._client = client
        self._config = config
        self._logger = logger if logger else logging.getLogger('experiment')
        self._pipes = pipes

    def prepare(self):
        """
        Override to provide custom preparation code, to be executed just
        as the experiment is about to be started (e.g. loading resources).

        Default behaviour: do nothing.

        """
        pass

    def run(self):
        """
        Abstract method.
        Override with actual experiment code.

        """
        raise Exception('Not implemented.')

    def abort(self):
        """
        Abort the experiment. The instance must be disposed afterwards.

        """
        pass

    @property
    def log(self):
        """
        A logging.logger instance for use in the experiment code.

        """
        return self._logger

    @property
    def control(self):
        """
        An interface to experiment progress control, to be accessed
        in the experiment code.

        """
        return self._control

    @property
    def info(self):
        """
        Additional info about the running experiment and its environment.

        """
        return self._info

    @property
    def client(self):
        """
        The name of the client running this instance.

        """
        return self._client

    @property
    def config(self):
        """
        Configuration used by this instance, and owning client.

        """
        return self._config

    @property
    def pipes(self):
        """
        EventHub instance used by this instance for controlling keyboard or mouse.
        Shared between instances started in the same command line call.

        """
        return self._pipes


class ExperimentControl(object):
    """
    An interface to experiment progress control.

    """

    def __init__(self, name, rpc):
        """
        Initializes the object.

        :param name: Name of the local client.
        :param rpc: An rpc interface for the experiment instance.

        """
        self._name = name
        self._rpc = rpc

    def put(self, path, obj, wait=True):
        """
        Sends a python object to be stored on the server.

        :param path: The target path, under which the object will be placed, relative to rpc root.
        :param obj: The object to transfer. Can be a primitive or a custom type, which can be imported, provided that
                    in can be pickled and recreated in other clients.
        :param wait: If True, the call blocks, until it is completed (or aborted) - successful or not.
                     If False, the call immidiately returns a CallFinished object, which can be querried for call status.

        :returns: If `wait` is True the result of the call is returned, which is always None in this case.
                  If `wait` is False, a CallFinished object is returned, which can be querried for call status,
                  waited on, and used to retreive the actual call result.

        """
        parentpath, name = _split_name_from_path(path)
        cf = self._rpc.init_call(_make_absolute_path(parentpath, 'put'), name, obj)
        return cf.result() if wait else cf

    def create(self, path, klass, wait=True, args=[], kwargs={}):
        """
        Create an object on the remote server process.

        :param path: The target path, under which the object will be placed, relative to rpc root.
        :param klass: The class object representing the type of the object to create. Must be imported by
                      the remote process.
        :param wait: If True, the call blocks, until it is completed (or aborted) - successful or not.
                     If False, the call immidiately returns a CallFinished object, which can be querried for call status.
        :param args: A list of positional arguments to be passed to the new object's __init__ method.
        :param kwargs: A dict of arguments to be passed to the new object's __init__ method.

        :returns: If `wait` is True the result of the call is returned, which is always None in this case.
                  If `wait` is False, a CallFinished object is returned, which can be querried for call status,
                  waited on, and used to retreive the actual call result.

        """
        parentpath, name = _split_name_from_path(path)
        cf = self._rpc.init_call(_make_absolute_path(parentpath, 'create'), name, klass, *args, **kwargs)
        return cf.result() if wait else cf

    def get(self, path, blocking=True, wait=True):
        """
        Create a local copy of a remote object.

        Note: blocking=False is not implemented.

        :param path: The path of the object to retreive, relative to rpc root.
        :param blocking: Wait for the object to show up, if target path is empty?
        :param wait: If True, the call blocks, until it is completed (or aborted) - successful or not.
                     If False, the call immidiately returns a CallFinished object, which can be querried for call status.
        :returns: If `wait` is True the result of the call is returned.
                  If `wait` is False, a CallFinished object is returned, which can be querried for call status,
                  waited on, and used to retreive the actual call result.

        """
        parentpath, name = _split_name_from_path(path)
        cf = self._rpc.init_call(_make_absolute_path(parentpath, 'get'), name)
        return cf.result() if wait else cf

    def exists(self, path):
        """
        Checks if a remote object exitsts. Blocks until result is available.

        Note: due to a race condition, the following may still fail:::

              if experiment_control.exists('foo'):
                  foo = experiment_control.get('foo')

        :param path: The path of the object to retreive, relative to rpc root.

        :returns: True if the object exists, False otherwise.

        """
        parentpath, name = _split_name_from_path(path)
        return self._rpc.call(_make_absolute_path(parentpath, 'exists'), name)

    def acquire(self, path, blocking=True, wait=True):
        """
        Acquire the lock guarding the Directory contents. If the lock has already
        been acqired, `blocking` specifies, whether the call should return immidiately
        or wait for the lock's holder to release it.

        :param path: Path to the Directory to lock.
        :param blocking: Wait for the lock to become available?
        :param wait: If True, the call blocks, until it is completed (or aborted) - successful or not.
               If False, the call immidiately returns a CallFinished object,
               which can be querried for call status.

        :returns: False if `blocking` is False and the lock has already been acquired. True otherwise.
                  If `wait` is True the result of the call is returned.
                  If `wait` is False, a CallFinished object is returned, which can be querried for call status,
                  waited on, and used to retreive the actual call result.

        """

        cf = self._rpc.init_call(_make_absolute_path(path, 'acquire'), blocking)
        return cf.result() if wait else cf

    def release(self, path):
        """
        Release the lock guarding the Directory contents.

        :param path: Path to the Directory to unlock.

        """
        return self._rpc.call(_make_absolute_path(path, 'release'))

    def ensure(self, path, klass, wait=True, args=[], kwargs={}):
        """
        Create an object on the remote server process, or silently fail if it already exits.
        Obsolete - has same effect as `create`.

        :param path: The target path, under which the object will be placed, relative to rpc root.
        :param klass: The class object representing the type of the object to create. Must be imported by
                      the remote process.
        :param wait: If True, the call blocks, until it is completed (or aborted) - successful or not.
                     If False, the call immidiately returns a CallFinished object, which can be querried for call status.
        :param args: A list of positional arguments to be passed to the new object's __init__ method.
        :param kwargs: A dict of arguments to be passed to the new object's __init__ method.

        :returns: If `wait` is True the result of the call is returned, which is always None in this case.
                  If `wait` is False, a CallFinished object is returned, which can be querried for call status,
                  waited on, and used to retreive the actual call result.

        """
        #parentpath, _ = _split_name_from_path(path)
        #if self.acquire(parentpath, False):
        self.create(path, klass, wait, args, kwargs)
        #    self.release(parentpath)

    def ensure_meeting(self, path, guests):
        """
        Create a Meeting object on the remote server process, or silently fail if it already exits.

        :param path: The target path, under which the Meeting object will be placed,
                     relative to rpc root.
        :param guests: List of guest names, for which the meeting will wait.
                       Empty list causes the meeting to start immidiately.
                       `None` casues the meeting to wait until start is explicitly called,
                       regardless of what guests join.

        Note: The above referts to the defualt situation, when `autostart` is True.
        Specifying `autostart` as False will render this argument meaningless.

        """
        self.ensure(path, Meeting, wait=True, args=[guests])

    def abort_meeting(self, path, wait=True):
        """
        Aborts the meeting. The meeting becomes `ABORTED`, then all clients are woken up,
        and their `join` calls raise an exception.
        Subsequent calls to `join` on this meeting also raise an exception.
        Subsequent calls to `start` or `abort` have no effect.
        
        :param path: The path to the meeting to be aborted.
        :param wait: Block until the operation is finished?

        """
        cf = self._rpc.init_call(_make_absolute_path(path, 'abort'))
        return cf.result() if wait else cf

    def reset_meeting(self, path, wait=True):
        """
        Obsolete, do not use.

        """
        cf = self._rpc.init_call(_make_absolute_path(path, 'reset'))
        return cf.result() if wait else cf

    def meet(self, path, name, wait=True):
        """
        Join the meeting as a specified guest. Blocks until the meeting becomes `STARTED`
        (i.e. all guests call `join`, or `start` has been called), or aborted. In the latter
        case an exception is raised.

        Calling `join` on a started meeting returns immidiately.
        Joining an aborted meeting causes (immidiately) the same exception, as if
        the call blocked.

        :param guest: The name of the guest to join.
        :param path: The path to the meeting to be joined.

        :returns: If `wait` is True the result of the call is returned, which is always None in this case.
                  If `wait` is False, a CallFinished object is returned, which can be querried for call status,
                  waited on, and used to retreive the actual call result.

        """
        cf = self._rpc.init_call(_make_absolute_path(path, 'join'), name)
        return cf.result() if wait else cf

    def get_particpant_info(self):
        """
        Returns a list of tuples (name, type), one for each client in the experiment, where:

        :param name: The name of the client.
        :param type: The type (see: ClientType) of the client.

        """
        return self._rpc.call('experiment.get_participant_info')

    def get_participants(self):
        """
        Returns the names of all clients of type ClientType.NORMAL in the experiment.

        """
        pis = self.get_particpant_info()
        return [pi[0] for pi in pis if pi[1] == ClientType.NORMAL]

    @property
    def rpc(self):
        """
        An rpc interface (Dispatcher) used by the experiment code.

        """
        return self._rpc

    @property
    def name(self):
        """
        The name of the local client.

        """
        return self._name


class ExperimentInfo(object):
    """
    Additional info about an experiment.

    """
    def __init__(self, name, files, clients, test_mode, args, kwargs):
        """
        Initializes the object.

        :param name: Name of the experiment.
        :param files: List of paths required by the experiment to run.
        :param clients: List of connected clients.
        :param test_mode: Run the experiment in test mode?
        :param args: Positional arguments passed at the createion of the experiment.
        :param kwargs: Keyword arguments passed at the createion of the experiment.

        """
        self.name = name
        self.files = files
        self.clients = clients
        self.test_mode = test_mode
        self.args = args
        self.kwargs = kwargs


class ExperimentAbortedException(Exception):
    """
    Raised to indicate, that the experiment has been aborted.

    """
    def __init__(self):
        """
        Initializes the object.

        """
        Exception.__init__(self)
