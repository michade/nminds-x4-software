"""
.. Copyright (c) 2014 Institute of Psychology of the Polish Academy of Sciences.
   All rights reserved. For full license text (in Polish) please see LICENSE.polish
  
   Author: Michal Denkiewicz (michal.denkiewicz@gmail.com)

   This file is a part of the nminds-x4 program, used to conduct psychological
   experiments on group decision-making. For more information please see README file.

Various utilities to be used in the development of experiments.
Contains a simple object-oriented presentation system, that psychopy lacks.

"""

import sys
import math
import os.path
import xml.dom.minidom
import pyglet.media
import pyglet.window.key
from datetime import datetime
import pygame
import numpy as np


# sets a platform-specific clock type
if sys.platform == 'win32':
    from ctypes import windll
    from ctypes import byref
    from ctypes import c_int64

    _qpc_counter = c_int64()
    _QueryPerformanceCounter = windll.Kernel32.QueryPerformanceCounter

    _qpc_frequency = c_int64()
    windll.Kernel32.QueryPerformanceFrequency(byref(_qpc_frequency))
    _qpc_frequency_float = float(_qpc_frequency.value)

    def _getTime_win32():
        _QueryPerformanceCounter(byref(_qpc_counter))
        return _qpc_counter.value / _qpc_frequency_float

    _getTime = _getTime_win32
else:
    import time

    def _getTime_default():
        return time.time()

    _getTime = _getTime_default
#end if


class Clock(object):
    """
    A platform-independent wrapper for time checking.

    """
    def __init__(self):
        """
        Creates a clock object. It is initially stopeed.

        """
        self._last_time = None
        self.reset()

    def reset(self):
        """
        Resets the clock time to 0.

        """
        self._last_time = _getTime()

    def getTime(self):
        """
        Gets the time from last reset (in seconds).

        """
        return _getTime() - self._last_time


def make_results_filename(name, label, session, extension='.txt'):
    """
    Compose a data file name.

    :param name: Base of the name, ussually experiument name.
    :param label: Additional label.
    :param session: Session number.
    :param extension: File extension.
    :returns: a new results file name (string).

    """
    now = datetime.now()
    datestr = now.strftime('%Y%m%d-%H%M')
    return '%s-%s-%s-%03d%s' % (name, label, datestr, session, extension)


class ResultsDirectory(object):
    """
    Keeps track of a directory containing data files.
    Allows automatic session numbering.

    """

    def __init__(self, name, path, create=False):
        """
        Initializes the object.

        :param name: Base name for data files.
        :param path: Directory path.
        :param create: Create the path directories, if it doesn't exist?

        """
        if not os.path.exists(path):
            if create:
                os.makedirs(path)
            else:
                Exception('Results path doesn\'t exist: ' + path)
        self._name = name
        self._path = path

    def get_new_session_id(self, label):
        """
        Automatically determines next sesion id (number).
        It is the largest session id in the folder + 1.

        :param label: An additional label differentiating experiments.
        :returns: a new, consecutive, session id (int).

        """
        isession = 0
        for fn in os.listdir(self._path):
            fp = os.path.join(self._path, fn)
            if not os.path.isfile(fp):
                continue
            try:
                noext = os.path.splitext(fn)[0]
                name, lbl, _, _, session = noext.split('-')
                k = int(session)
                if name == self._name and label == lbl and k >= isession:
                    isession = k + 1
            except:
                pass
        return isession

    def make_new_file(self, label, isession=None):
        """
        Creates a new data file.

        :param label: Additional label, to add to the file name.
        :param isession: Session ID, if `None`, it is determined automatically,
                         i.e. it becomes the largest session id in the folder + 1.

        :returns: A full path to the new file.

        """
        if isession is None:
            isession = self.get_new_session_id(label)
        new_name = make_results_filename(self._name, label, isession)
        try:
            path = os.path.join(self._path, new_name)
            f = open(path, 'w')
        finally:
            if f is not None:
                f.close()
        return path

    def __str__(self):
        return os.path.join(self._path, self._name)


class Presenter(object):
    """
    Encapsulates the presentation of stimuli.

    """

    def __init__(self, window, master_clock, on_flip=lambda: None):
        """
        Initializes the object.

        :param window: The psychopy.window providing the rendering context.
        :param master_clock: Clock providing time relative to the start of the experimental session.
        :param on_flip: A callable to call after each flip (start of each frame).

        """
        self._window = window
        self._data = []
        self._on_flip = on_flip
        self._master_clock = master_clock

    def show(self, *screens):
        """
        Show one or more screens. The transition between consecutive screens
        is seamless - there are no blank frames in between.
        Each screen will be shown at least for one frame.

        :param screens: the Screen objects to show.

        """
        duration = Clock()
        prev = None
        onset = None
        for screen in screens:
            self._do_draw_and_flip_sqeuence(screen, True)
            now = self._now()
            self._record_data(prev, onset, now, duration.getTime())
            onset = now
            duration.reset()
            screen.on_show_first()
            screen.check_reset()
            #first = True
            while not screen.check():
                self._do_draw_and_flip_sqeuence(screen, True)
                #first = False
            prev = screen
        self._record_data(prev, onset, self._now(), duration.getTime())

    def _now(self):
        """
        Returns current time, according to master clock.

        """
        return self._master_clock.getTime() if self._master_clock is not None else None

    def _do_draw_and_flip_sqeuence(self, screen, redraw):
        """
        Clears the back buffer, draws a screen to the back buffer and does a buffer flip.
        Also performs housekeeping.
        If redraw is false and the screen is static, skips clearing and drawing, just does the filp.
        Waits for vsync.

        :param screen: The screen to draw.
        :param redraw: Skip clearing and drawing (for static stimuli).

        """
        self._window.switch_to()
        if redraw or not screen.is_static:
            self._window.clear()
            screen.show()  # draws the contents of the screen to the back buffer
        self._do_housekeeping()  # pumping events here prevents "hang-like" behavior of window
        self._window.flip()  # swaps buffers and blocks until vsync - now the result of screen.draw() is visible
        self._on_flip()

    def _record_data(self, screen, onset, offset, duration):
        """
        Add stimulus timing data to the `trials` table.

        :param screen: The relevant screen.
        :param onset: Screen's onset time (the time it appears, relative to  master_clock).
        :param offset: Screen's offset time (the time it disappears, relative to  master_clock).
        :param duration: The duration for which the screen appeared.

        """
        if screen is None:
            return
        screen.onset = onset
        screen.offset = offset
        screen.duration = duration
        if screen.name is not None:
            if onset is not None:
                self._data.append((screen.name + '.onset', onset))
            if offset is not None:
                self._data.append((screen.name + '.offset', offset))
            self._data.append((screen.name + '.duration', duration))

    def data(self):
        """
        A dictionary into which stimulus timing data is recorded.

        """
        return self._data

    def reset_data(self):
        self._data = []

    def _do_housekeeping(self):
        """
        Performs maintenance, that allows the program to run smoothly, and prevents
        hang-like behaviour.

        """
        self._window.dispatch_events()
        #window.dispatch_event('on_draw')  #drawing is handled manually
        pyglet.media.dispatch_events()


# Key codes for mouse buttons, to streamline input handling.
MOUSE_BUTTONS = ['lmb', 'mmb', 'rmb']

# Format to create joystick button codes
_JOYSTICK_BUTTON_FORMAT = 'joy_{btn_id:d}_{joy_id:d}'


def _make_joystick_key_code(joy_id, btn_id):
    """
    Create a joystick key code.

    :param joy_id: Joystick id.
    :param btn_id: Button id.

    """
    return _JOYSTICK_BUTTON_FORMAT.format(btn_id=btn_id, joy_id=joy_id)


def is_joystick_key_code(code):
    """
    Is `code` a joystick key code?

    :param code: a string key code

    """
    return code.startswith('joy')


def add_joystick_id(s, joy_id):
    """
    Adds a joystick id to the button-only code.

    :param s: joystick button code (eg. 'joy_3')
    :param joy_id: an integer joystick id
    
    """
    return s + '_' + str(joy_id)


class EventHub(object):
    """
    Serves as a bridge between the experiment and the input functions.
    Multiple EventHubs in different python runtimes on one machine can communicate
    and share events. This allows for running multiple participants on
    the same machine.

    """

    def __init__(self, is_master, pipes_in=None, pipes_out=None):
        """
        Initializes the object.

        :param is_master: The master hub gathers the events and distributes them to others.
        :param pipes_in: Pipe ends for receiving events.
        :param pipes_out: Pipe ends for sending events.

        """
        self._pipes_in = pipes_in if pipes_in else []
        self._pipes_out = pipes_out if pipes_out else []
        self._unsent = [] if is_master else None
        self._received = []
        self._is_master = is_master
        self._clock = Clock()
        self._window = None
        self._joysticks = []

    def after_window_created(self, win):
        """
        Call after a window has been created.

        """
        self._window = win
        if self._is_master:
            for i in range(pygame.joystick.get_count()):
                joy = pygame.joystick.Joystick(i)
                self._joysticks.append(joy)
                joy.init()
            win.push_handlers(self.on_key_press, self.on_mouse_press)

    def on_key_press(self, symbol, modifiers):
        self.receive_event(symbol)

    def on_mouse_press(self, x, y, button, modifiers):
        self.receive_event(button)

    def _check_joysticks(self):
        """
        Check for joystick/gamepad input. If joystick is not enabled returns an empty list.

        :returns: A list of pairs (button_code, time_pressed).

        """
        for event in pygame.event.get():
            if event.type == pygame.JOYBUTTONDOWN:
                self.receive_event(_make_joystick_key_code(event.joy, event.button))

    def receive_event(self, key):
        """
        Notifies the hub about an input event ocurring.

        :param key: key code of the event.

        """
        self._unsent.append((key, self._clock.getTime()))

    def _check_remote(self):
        """
        Check for events occuring on other hubs.

        :returns: A list of pairs (event, time_ocurred).

        """
        events = []
        for p in self._pipes_in:
            while p.poll():
                events.extend(p.recv())
        return events

    def check(self):
        """
        Check for inputs. They are stored in the EventHub object.

        """
        self._window.dispatch_events()
        if self._is_master:
            self._check_joysticks()
            new = self._unsent
            self._send(new)
            self._unsent = []
        else:
            pygame.event.pump()
            new = self._check_remote()
        self._received.extend(new)

    def reset(self):
        """
        Clear events gathered so far.

        """
        if self._is_master:
            self._unsent = []
        self._received = []

    def _send(self, events):
        """
        Sends events to other hubs.

        """
        if len(events) > 0:
            for p in self._pipes_out:
                p.send(events)

    def get(self, keys, clock=None):
        """
        Gets event of given types. They will be consumed (removed from internal storage).

        :param keys: Types of events to check for.
        :param clock: If specified, the timing of the events will be relative to this clock.

        :returns: A list (event, time).

        """
        self.check()
        keep = []
        result = []
        ptkeys = [str_to_pyglet_key(k) for k in keys]
        for ev in self._received:
            k, t = ev
            if k in ptkeys:
                kk = keys[ptkeys.index(k)]
                tt = t - self._clock.getTime() + clock.getTime() if clock else t
                result.append((kk, tt))
            else:
                keep.append(ev)
        self._received = keep
        return result


class EventChecker(object):
    """
    Encapsulates checking for some condition to be true, or
    checking whether some event has occured.

    Abstract class. Subclasses should override `check` and `reset`.

    """

    def check(self):
        """
        Called to check whether a relevant event has occured.

        To be overriden in subclasses.

        :returns: True if the event has occured. False otherwise.

        """
        raise NotImplementedError()

    def reset(self):
        """
        Reset the checker - essentially forget about any events that happened.

        To be overriden in subclasses.

        """
        pass


class Never(EventChecker):
    """
    A checker that never fires.

    """
    def check(self):
        """
        Always returns False.

        """
        return False


class TimeChecker(EventChecker):
    """
    Check if a predetermined amount of time (duration) has passed.
    Choses best time to terminate, depending on frame timing.

    """

    def __init__(self, max_duration, frame_length):
        """
        Initializes the object.

        :param max_duration: The duration to check for.
        :param frame_length: The length of a frame in seconds.

        """
        self._frame_length = frame_length
        self._max_duration = max_duration
        self._elapsed = Clock()

    def check(self):
        """
        Returns True, if terminating now would cause a stimulus to be presented
        for a duration closed to specified, than if it was terminated in the following frames.

        """
        f = self._frame_length
        d = self._max_duration
        t = self._elapsed.getTime()
        return math.fabs(t + f - d) < math.fabs(t + 2 * f - d)

    def reset(self):
        """
        Resets the checker.

        """
        self._elapsed.reset()


class InputChecker(EventChecker):
    """
    Checks for particular keys or mouse buttons to be pressed.

    """

    def __init__(self, keys, hub):
        """
        Initializes the object.

        :param keys: The keys or mouse button codes to wait for.
        :param hub: The event hub to take input from.

        """
        self._clock = Clock()
        self._keys = list(keys)
        self._hub = hub
        self._pressed = []

    def check(self):
        """
        Returns true, if one of relevant keys or mouse buttons were pressed.
        The keys pressed (and times of pressing) can be obtained trough the `pressed` property.

        """
        new = self._hub.get(self._keys, self._clock)
        self._pressed.extend(new)
        return len(self._pressed) > 0

    def reset(self):
        """
        Resets the checker.

        """
        self._hub.reset()
        self._clock.reset()
        self._pressed = []

    @property
    def pressed(self):
        """
        List of pairs (key, time_pressed), which caused the checker to fire.

        """
        return self._pressed


class AbortKeyChecker(InputChecker):
    """
    Checks for particular keys or mouse buttons to be pressed.
    Raises an exception instead of returning True from `check`.

    """

    def __init__(self, keys, hub, exception, args=None, kwargs=None):
        """
        Initializes the object.

        :param keys: The keys or mouse button codes to wait for.
        :param hub: The event hub to take input from.
        :param exception: The exception to raise.
        :param args: Positional arguments to the exception's __init__.
        :param kwargs: Keyword arguments to the exception's __init__.

        """
        super(AbortKeyChecker, self).__init__(keys, hub)
        self._exception = exception
        self._args = args if args else []
        self._kwargs = kwargs if kwargs else {}

    def check(self):
        """
        Checks if the relevant keys have been pressed.
        If so - raise the specified exception.

        """
        if super(AbortKeyChecker, self).check():
            raise self._exception(*self._args, **self._kwargs)
        return False


class RpcChecker(EventChecker):
    """
    Checks if a remote call has finished.

    """

    def __init__(self, rpc_cf):
        """
        Initializes the object.

        :param rpc_cf: A rpc.CallFinished object, used to querry for the call status.

        """
        self._rpc_cf = rpc_cf
        self._result = None

    def check(self):
        """
        Returns True, if the rpc call has finished.
        The result of the completed call can be retreived trough `result` property.

        """
        if self._rpc_cf.is_finished():
            self._result = self._rpc_cf.result()
            return True
        else:
            return False

    @property
    def result(self):
        """
        The result of the completed remote call.

        """
        return self._result


class MeetingChecker(EventChecker):
    """
    Joins an experiment.Meeting and checks if it is started.

    """

    def __init__(self, control, client, path, auto_reset=True):
        """
        Initializes the object.

        :param control: An ExperimentControl object used to access remote objects.
        :param client: The name

        """
        self._control = control
        self._client = client
        self._path = path
        self._cf = None
        self._auto_reset = auto_reset

    def check(self):
        """
        Returns True if the meeting has started.

        """
        if self._cf.is_finished():
            self._cf.result()
            return True
        else:
            return False

    def _reset(self):
        """
        Joins the meeting.

        """
        self._cf = self._control.meet(self._path, self._client, False)

    def reset(self):
        """
        If auto_reset is True, resets the checker (joins the meeting).

        """
        if self._auto_reset:
            self._reset()

    def manual_reset(self):
        """
        Unconditionally resets the checker (joins the meeting).

        """
        self._reset()


class ComplexChecker(EventChecker):
    """
    A combination of other (`children`) checkers.

    """

    def __init__(self, op, children):
        """
        Initializes the object.

        :param op: The operation to apply to the result of the child checkers.
        'and' - All checkers must fire, before the ComplexChecker fires.
        'or' - If one of the checkers fire, the ComplexChecker fires.
        :param children: A list of children checkers.

        """
        self._children = children
        self._op = op

    def check(self):
        """
        Checks ALL of the children checkers (no `short-circut` behaviour).

        If `op` == 'and' - all checkers must fire, before the ComplexChecker fires.
        If `op` == 'or' - If one of the checkers fire, the ComplexChecker fires.

        """
        if self._op == 'all':
            res = True
            for c in self._children:
                res = res and c.check()
        else:  # 'any'
            res = False
            for c in self._children:
                res = res or c.check()
        return res

    def reset(self):
        """
        Resets all children checkers.

        """
        for c in self._children:
            c.reset()

    @property
    def children(self):
        """
        The children checkers.

        """
        return self._children


class Screen(object):
    """
    A (possibly named) set of stimuli presented until specified conditions are met.

    """

    def __init__(self, stim, name=None, checker=Never(), on_show_first=lambda: None, is_static=True):
        """
        Initializes the object.

        :param stim: A stimulus presented. Use StimGroup to present many stimuli.
        :param name: The name od the screen, for recording purposes.
        :param checker: A Checker, checking for conditions that should terminate the presentation of stimuli.
        :param on_show_first: A callable, to execute when a distinct presentaion begins.

        """
        self.name = name
        self.stim = stim
        self.checker = checker
        self.on_show_first = on_show_first
        self.onset = None
        self.offset = None
        self.duration = None
        self._is_static = is_static

    def show(self):
        """
        Draw the stimuli to the back buffer.

        """
        self.stim.draw()

    def check(self):
        """
        Check for the conditions, that should terminate presentation of this screen.

        """
        return self.checker.check()

    def check_reset(self):
        """
        Reset the checkers (between distinct presentations).

        """
        self.checker.reset()

    @property
    def is_static(self):
        return self._is_static


class BlankScreen(object):
    """
    A blank screen stimulus.

    """
    def draw(self):
        """
        Does nothing.

        """
        pass


class StimGroup(object):
    """
    A set od stimuli to be drawn together.

    """
    def __init__(self, *stims):
        """
        Initializes the object.

        :param stims: the stimuli comprising the set.

        """
        self._stims = stims

    def draw(self):
        """
        Draw all stimuli contained in the group.

        """
        for stim in self._stims:
            stim.draw()


class DomSerializer(object):
    """
    Allows deserialization of text stimuli from external xml files.

    Each `Text` node will be converted to a string member of this class,
    under a name specified by `name` attribute.

    Example:
    <Text name='foo'>bar</Text> yields self.foo == 'bar'

    """

    def __init__(self, xmlfile):
        """
        Initializes the object.

        :param xmlfile: the file

        """
        stims = xml.dom.minidom.parse(xmlfile)
        self.handle_strings(stims.getElementsByTagName('Text'))
        stims.unlink()

    def handle_strings(self, strings):
        """
        Processes `Text` nodes and create attributes.

        """
        for s in strings:
            self.__setattr__(s.getAttribute('name'), self.get_text(s.childNodes))

    def get_text(self, nodelist):
        """
        Extract text frm nodes.

        :param nodelist: A list of nodes to process.

        :returns: The text parsed.

        """
        rc = []
        for node in nodelist:
            if node.nodeType == node.TEXT_NODE:
                rc.append(node.data)
        return ''.join(rc)


class Monitor(object):
    """
    Placeholder for monitor info.

    """

    def __init__(self, width, height, distance, resolution, frame_length):
        self.width = width
        self.height = height
        self.distance = distance
        self.resolution = resolution
        self.frame_length = frame_length


def str_to_pyglet_key(key):
    """
    Converts a string key code to a pyglet key code.


    """
    if is_joystick_key_code(key):
        return key
    key = key.strip().upper()
    return pyglet.window.key.__dict__[key]


class Sound(object):
    """
    Encapsulates a playable sound (file or a note).

    """

    def __init__(self):
        """
        Initializes the sound object, but the media is not yet loaded.
        To create resources use `load`.

        """
        self._sound = None
        
    def load(self):
        """
        Creates the media resources required.
        
        Override this method in subclasses.

        """
        pass

    def play(self):
        """
        Start playing the sound.

        """
        self._sound.play()

    def stop(self):
        """
        Stop playing the sound.

        """
        self._sound.stop()


class Note(Sound):
    """
    A musical note sound.

    """

    STEPS_FROM_A = {
        'C': 9,
        'Csh': 8,
        'Dfl': 8,
        'D': 7,
        'Dsh': 6,
        'Efl': 6,
        'E': 5,
        'F': 4,
        'Fsh': 3,
        'Gfl': 3,
        'G': 2,
        'Gsh': 1,
        'Afl': 1,
        'A': 0,
        'Ash': 1,
        'Bfl': 1,
        'B': 2,
        'Bsh': 2,
    }

    def __init__(self, note, duration, octave=4):
        """
        Initializes the object.

        :param note: The note symbol.
        :param duration: Duration of the sound in seconds.
        :param octave: The octave of the note sound.

        """
        super(Note, self).__init__()
        self._note = note
        self._duration = duration
        self._octave = octave
    
    def load(self):
        """
        Creates the associated media resources.

        """
        sample_rate, fmt, stereo = pygame.mixer.get_init()
        A = 440.0
        freq = A * 2.0 ** (Note.STEPS_FROM_A[self._note] / 12.0) * 2.0 ** (self._octave - 4)
        n = int(self._duration * sample_rate)
        arr = np.arange(0.0, 1.0, 1.0 / n)
        arr *= 2 * np.pi * freq * self._duration
        arr = np.sin(arr)
        if stereo == 2 and (len(arr.shape) == 1 or arr.shape[1] < 2):
            sarr = np.ones((len(arr), 2))
            sarr[:, 0] = arr
            sarr[:, 1] = arr
            arr = sarr
        if fmt == -16:
            arr = (arr * 2 ** 15).astype(np.int16)
        elif fmt == 16:
            arr = ((arr + 1) * 2 ** 15).astype(np.uint16)
        elif fmt == -8:
            arr = (arr * 2 ** 7).astype(np.Int8)
        elif fmt == 8:
            arr = ((arr + 1) * 2 ** 7).astype(np.uint8)
        self._sound = pygame.sndarray.make_sound(arr)
